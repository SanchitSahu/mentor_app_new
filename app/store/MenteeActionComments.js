Ext.define('Mentor.store.MenteeActionComments', {
    extend: 'Ext.data.Store',
    config: {
        model: "Mentor.model.MenteeActionComments",
        autoLoad: true,
        autoSync: true,
        proxy: {
            type: 'localstorage',
            id: 'idMenteeActionComments'
        }
    }
});