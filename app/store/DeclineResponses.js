Ext.define('Mentor.store.DeclineResponses', {
    extend: 'Ext.data.Store',
    config: {
        model: "Mentor.model.DeclineResponses",
        autoLoad: true,
        autoSync: true,
        proxy: {
            type: 'localstorage',
            id: 'idDeclineResponses'
        }
    }
});