Ext.define('Mentor.store.Skills', {
    extend: 'Ext.data.Store',
    config: {
        model: "Mentor.model.Skills",
        autoLoad: false,
        autoSync: true,
        proxy: {
            type: 'localstorage',
            id: 'idSkills'
        }
    }
});