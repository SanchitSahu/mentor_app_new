Ext.define('Mentor.store.TeamDropdown', {
    extend: 'Ext.data.Store',
    config: {
        model: "Mentor.model.TeamDropdown",
        autoLoad: true,
        autoSync: true,
        proxy: {
            type: 'localstorage',
            id: 'idTeamDropdown'
        }
    }
});