Ext.define('Mentor.store.WaitScreenMentorRejected', {
    extend: 'Ext.data.Store',
    config: {
        model: "Mentor.model.WaitScreenMentorRejected",
        autoLoad: true,
        autoSync: true
    }
});