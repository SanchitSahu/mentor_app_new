Ext.define('Mentor.store.EnterpreneurAction', {
    extend: 'Ext.data.Store',
    config: {
        model: "Mentor.model.EnterpreneurAction",
        autoLoad: false,
        autoSync: true,
        proxy: {
            type: 'localstorage',
            id: 'idEnterpreneurActionItem'
        }
    }
});