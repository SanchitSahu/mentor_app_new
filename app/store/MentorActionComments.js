Ext.define('Mentor.store.MentorActionComments', {
    extend: 'Ext.data.Store',
    config: {
        model: "Mentor.model.MentorActionComments",
        autoLoad: true,
        autoSync: true,
        proxy: {
            type: 'localstorage',
            id: 'idMentorActionComments'
        }
    }
});