Ext.define('Mentor.store.InviteMentee', {
    extend: 'Ext.data.Store',
    config: {
        model: "Mentor.model.InviteMentee",
        autoLoad: true,
        autoSync: true,
        proxy: {
            type: 'localstorage',
            id: 'idInviteMentee'
        }
    }
});