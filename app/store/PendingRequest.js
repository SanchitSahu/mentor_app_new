Ext.define('Mentor.store.PendingRequest', {
    extend: 'Ext.data.Store',
    config: {
        model: "Mentor.model.PendingRequest",
        autoLoad: true,
        autoSync: true
    }
});