Ext.define('Mentor.store.MentorForTeam', {
    extend: 'Ext.data.Store',
    config: {
        model: "Mentor.model.MentorForTeam",
        autoLoad: true,
        autoSync: true,
        proxy: {
            type: 'localstorage',
            id: 'idMentorForTeam'
        }
    }
});