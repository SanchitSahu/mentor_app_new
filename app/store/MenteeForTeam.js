Ext.define('Mentor.store.MenteeForTeam', {
    extend: 'Ext.data.Store',
    config: {
        model: "Mentor.model.MenteeForTeam",
        autoLoad: true,
        autoSync: true,
        proxy: {
            type: 'localstorage',
            id: 'idMenteeForTeam'
        }
    }
});