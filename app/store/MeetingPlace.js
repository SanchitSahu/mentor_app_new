Ext.define('Mentor.store.MeetingPlace', {
    extend: 'Ext.data.Store',
    config: {
        model: "Mentor.model.MeetingPlace",
        autoLoad: true,
        autoSync: true,
        proxy: {
            type: 'localstorage',
            id: 'idMeetingPlace'
        }
    }
});