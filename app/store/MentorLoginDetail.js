Ext.define('Mentor.store.MentorLoginDetail', {
    extend: 'Ext.data.Store',
    config: {
        model: "Mentor.model.MentorLoginDetail",
        autoLoad: false,
        autoSync: true,
        proxy: {
            type: 'localstorage',
            id: 'idMentorLoginDetail'
        }
    }
});