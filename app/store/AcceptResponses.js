Ext.define('Mentor.store.AcceptResponses', {
    extend: 'Ext.data.Store',
    config: {
        model: "Mentor.model.AcceptResponses",
        autoLoad: true,
        autoSync: true,
        proxy: {
            type: 'localstorage',
            id: 'idAcceptResponses'
        }
    }
});