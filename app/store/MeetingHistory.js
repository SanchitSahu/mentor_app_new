Ext.define('Mentor.store.MeetingHistory', {
    extend: 'Ext.data.Store',
    config: {
        model: "Mentor.model.MeetingHistory",
        autoLoad: true,
        autoSync: true
    }
});