Ext.define('Mentor.model.DeclineResponses', {
    extend: 'Ext.data.Model',
    config: {
        fields: [
            {name: 'ResponseID', type: 'string'},
            {name: 'ResponseName', type: 'string'}
        ]
    }
});