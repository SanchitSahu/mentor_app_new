Ext.define('Mentor.model.MenteeActionComments', {
    extend: 'Ext.data.Model',
    config: {
        fields: [
            {name: 'MenteeActionItemCommentID', type: 'string'},
            {name: 'MeetingID', type: 'string'},
            {name: 'MenteeActionItemID', type: 'string'},
            {name: 'Comment', type: 'string'}
        ]
    }
});