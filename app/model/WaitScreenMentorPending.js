Ext.define('Mentor.model.WaitScreenMentorPending', {
    extend: 'Ext.data.Model',
    config: {
        fields: [
            {name: 'InvitationId', type: 'string'},
            {name: 'MentorComments', type: 'string'},
            {name: 'MentorID', type: 'string'},
            {name: 'MentorName', type: 'string'},
            {name: 'Status', type: 'string'},
            {name: 'MentorImage', type: 'string'},
            {name: 'TopicDescription', type: 'string'},
            {name: 'inviteTime', type: 'string'},
            {name: 'MenteeComment', type: 'string'},
            {name: 'MeetingDateTime', type: 'string'},
            {name: 'MeetingPlaceName', type: 'string'},
            {name: 'MeetingTypeName', type: 'string'},
            {name: 'MentorEmail', type: 'string'},
            {name: 'MentorPhone', type: 'string'},
            {name: 'MentorSmsCapable', type: 'string'},
            {name: 'cancellationReason', type: 'string'},
            {name: 'cancelledBy', type: 'string'},
            {name: 'RescheduleComments', type: 'string'},
            {name: 'rescheduledBy', type: 'string'},
            {name: 'AcceptResponsesName', type: 'string'},
            {name: 'DeclineResponsesName', type: 'string'},
            {name: 'InitiatedBy', type: 'string'}
        ]
    }
});
