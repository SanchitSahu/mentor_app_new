Ext.define('Mentor.model.MentorPendingMeeting', {
    extend: 'Ext.data.Model',
    config: {
        fields: [
            {name: 'InvitationId', type: 'string'},
            {name: 'ManteeComments', type: 'string'},
            {name: 'InviteeEmail', type: 'string'},
            {name: 'InviteePhone', type: 'string'},
            {name: 'MenteeID', type: 'string'},
            {name: 'MenteeImage', type: 'string'},
            {name: 'MenteeName', type: 'string'},
            {name: 'SmsCapable', type: 'string'},
            {name: 'TopicDescription', type: 'string'},
            {name: 'TopicID', type: 'string'},
            {name: 'inviteTime', type: 'string'},
            {name: 'MeetingPlaceName', type: 'string'},
            {name: 'MeetingPlaceID', type: 'string'},
            {name: 'MentorComments', type: 'string'},
            {name: 'MeetingDateTime', type: 'string'},
            {name: 'MeetingTypeID', type: 'string'},
            {name: 'MeetingTypeName', type: 'string'},
            {name: 'Status', type: 'string'},
            {name: 'InitiatedBy', type: 'String'},
            {name: 'FollowUpMeetingFor', type: 'String'}

        ]
    }
});
