Ext.define('Mentor.model.TeamDropdown', {
    extend: 'Ext.data.Model',
    config: {
        fields: [
            {name: 'TeamId', type: 'string'},
            {name: 'TeamName', type: 'string'}
        ]
    }
});
