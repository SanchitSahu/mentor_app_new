Ext.define('Mentor.model.Mentee', {
    extend: 'Ext.data.Model',
    config: {
        fields: [
            {name: 'MenteeName', type: 'string'},
            {name: 'MenteeID', type: 'string'},
            {name: 'TopicID', type: 'string'},
            {name: 'MenteeComments', type: 'string'}
        ]
    }
});
