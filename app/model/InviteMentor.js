Ext.define('Mentor.model.InviteMentor', {
    extend: 'Ext.data.Model',
    config: {
        fields: [
            {name: 'MentorID', type: 'string'},
            {name: 'MentorName', type: 'string'},
            {name: 'rStatus', type: 'string'}
        ]
    }
});
