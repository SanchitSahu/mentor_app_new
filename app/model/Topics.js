Ext.define('Mentor.model.Topics', {
    extend: 'Ext.data.Model',
    config: {
        fields: [
            {name: 'TopicDescription', type: 'string'},
            {name: 'TopicID', type: 'string'},
        ]
    }
});
