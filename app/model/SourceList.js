Ext.define('Mentor.model.SourceList', {
    extend: 'Ext.data.Model',
    config: {
        fields: [
            {name: 'SourceName', type: 'string'},
            {name: 'SourceID', type: 'string'},
        ]
    }
});
