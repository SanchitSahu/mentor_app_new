Ext.define('Mentor.model.MentorForTeam', {
    extend: 'Ext.data.Model',
    config: {
        fields: [
            {name: 'MentorID', type: 'string'},
            {name: 'MentorName', type: 'string'}
        ]
    }
});