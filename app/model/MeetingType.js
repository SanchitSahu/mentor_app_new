Ext.define('Mentor.model.MeetingType', {
    extend: 'Ext.data.Model',
    config: {
        fields: [
            {name: 'MeetingTypeName', type: 'string'},
            {name: 'MeetingTypeID', type: 'string'},
        ]
    }
});
