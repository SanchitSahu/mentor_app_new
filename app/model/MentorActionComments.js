Ext.define('Mentor.model.MentorActionComments', {
    extend: 'Ext.data.Model',
    config: {
        fields: [
            {name: 'MentorActionItemCommentID', type: 'string'},
            {name: 'MeetingID', type: 'string'},
            {name: 'MentorActionItemID', type: 'string'},
            {name: 'Comment', type: 'string'}
        ]
    }
});