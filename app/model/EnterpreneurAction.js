Ext.define('Mentor.model.EnterpreneurAction', {
    extend: 'Ext.data.Model',
    config: {
        fields: [
            {name: 'MenteeActionID', type: 'string'},
            {name: 'MenteeActionName', type: 'string'}
        ]
    }
});
