Ext.define('Mentor.controller.Main', {
    extend: 'Ext.app.Controller',
    config: {
        refs: {
        },
        control: {
            "login button[action=doLogin]": {
                tap: "doLogin"
            },
            "login button[action=btnRegister]": {
                tap: "doRegister"
            },
            "login button[action=doForgotPassword]": {
                tap: "doForgotPassword"
            },
            "login button[action=loginHelpIcon]": {
                tap: "dologinHelpIcon"
            },
            "forgotpassword button[action=btnBackForgotPassword]": {
                tap: "btnBackForgotPassword"
            },
            "forgotpassword button[action=btnResetPassword]": {
                tap: "doResetPassword"
            },
            "signup button[action=back_signup]": {
                tap: "backSignUp"
            },
            "signup button[action=doRegisterSignUp]": {
                tap: "doRegisterSignUp"
            },
            "entrepreneur button[action=btnBackEnterpreneur]": {
                tap: 'btnBackEnterpreneur'
            },
            "entrepreneur button[action=btnNextEnterpreneur]": {
                tap: 'btnNextEnterpreneur'
            },
            "meetingdetails button[action=btnBackMeetingDetails]": {
                tap: 'btnBackMeetingDetails'
            },
            "subtopicdiscusses button[action=btnBackSubtopicDiscussed]": {
                tap: 'btnBackSubtopicDiscussed'
            },
            "subtopicdiscusses button[action=btnNextSubtopicDiscussed]": {
                tap: 'btnNextSubtopicDiscussed'
            },
            "meetingdetails button[action = btnNextMeetingDetails]": {
                tap: 'btnNextMeetingDetails'
            },
            "enterpreneur_action_items button[action = btnNextEntrepreneurItems]": {
                tap: 'btnNextEntrepreneurItems'
            },
            "enterpreneur_action_items button[action = btnBackEnterpreneurActionItems]": {
                tap: 'btnBackEnterpreneurActionItems'
            },
            "timing button[action = btnNextTiming]": {
                tap: 'btnNextTiming'
            },
            "timing button[action = btnBackTimingScreen]": {
                tap: 'btnBackTimingScreen'
            },
            "entrepreneur button[action = doStartSession]": {
                tap: 'doStartSession'
            },
            "entrepreneur button[action = doEndSession]": {
                tap: 'doEndSession'
            },
            "mentor_action_items button[action = btnNextMentorActionItems]": {
                tap: 'btnNextMentorActionItems'
            },
            "mentor_action_items button[action = btnBackMentorActionItems]": {
                tap: 'btnBackMentorActionItems'
            },
            "meetingedit_mentor_action_items button[action = btnNextMeetingEditMentorActionItems]": {
                tap: 'btnNextMeetingEditMentorActionItems'
            },
            "meetingedit_mentor_action_items button[action = btnBackMeetingEditMentorActionItems]": {
                tap: 'btnBackMeetingEditMentorActionItems'
            },
            "meetingedit_mentee_action_items button[action = btnNextMeetingEditMenteeActionItems]": {
                tap: 'btnNextMeetingEditMenteeActionItems'
            },
            "meetingedit_mentee_action_items button[action = btnBackMeetingEditMenteeActionItems]": {
                tap: 'btnBackMeetingEditMenteeActionItems'
            },
            "meetinghistory button[action = btnSignOutMeetingHistory]": {
                tap: 'btnSignOutMeetingHistory'
            },
            "meetinghistorymentee button[action = btnSignOutMeetingHistoryMentee]": {
                tap: 'btnSignOutMeetingHistoryMentee'
            },
            "signout button[action = btnSignOut]": {
                tap: 'btnSignOut'
            },
            "signout button[action = btnbackSignOut]": {
                tap: 'btnbackSignOut'
            },
            "datepickerfield[itemId=startSessionDatePicker]": {
                change: 'startSessionDatePicker'
            },
            "introductionscreen button[action=btnSkipIntroductionScreen]": {
                tap: "btnSkipIntroductionScreen"
            }
        }
    },
    dologinHelpIcon: function () {
        var popup = Mentor.app.application.getController('AppDetail').addHelpPopup('signin');
        Ext.Viewport.add(popup);
        popup.show();
    },
    startSessionDatePicker: function () {

    },
    getSchoolDetail: function () {
        var me = this;
        var GLOBAL = Mentor.Global;
        Ext.Viewport.setMasked({
            xtype: "loadmask",
            message: "Please wait"
        });
        Ext.Ajax.request({
            url: GLOBAL.URL,
            method: "POST",
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json'
            },
            xhr2: true,
            disableCaching: false,
            jsonData: {
                method: "getSchoolList",
                body: ""
            },
            success: function (responce) {
                Ext.Viewport.setMasked(false);
                var data = Ext.decode(responce.responseText);
                console.log(data);
                if (data.getSchoolList.Error == 1 || data.getSchoolList.Error == 2) {
                    Ext.getStore('SchoolDetail').removeAll();
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.getSchoolList.Message);
                    }, 100);
                    return;
                } else {
                    Ext.getStore('SchoolDetail').removeAll();
                    Ext.getStore('SchoolDetail').insert(0, {"subdomain_id": "-1", "subdomain_name": "Select School", "school_name": "-- Select School --"});
                    Ext.getStore('SchoolDetail').add(data.getSchoolList.data);
                    var TopicsStore = Ext.getStore('SchoolDetail').load(); //idSchoolDetailSelectFieldLoginScreen
                }
            },
            failure: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                console.log(responce);
                var connectionExist = Mentor.Global.doesConnectionExist();
                var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                Ext.Function.defer(function () {
                    Ext.Msg.alert('MELS', error);
                }, 100);
            }
        });
    },
    doLogin: function (btn) {
        me = this;
        var Email = Ext.getCmp('emailLogin').getValue();
        var Password = Ext.getCmp('passwordLogin').getValue();
        var SchoolID = Ext.getCmp('idSchoolDetailSelectFieldLoginScreen').getValue();
        if (SchoolID == "-1") {
            Ext.Msg.alert('MELS', 'Please select school.', Ext.emptyFn);
        } else if (Email == "") {
            Ext.Msg.alert('MELS', 'Please enter user name.', Ext.emptyFn);
        } else if (Password == "") {
            Ext.Msg.alert('MELS', 'Please enter password.', Ext.emptyFn);
        } else {
            me.LoginUser(Email, Password);
        }
    },
    LoginUser: function (strEmail, strPassword) {
        var GLOBAL = Mentor.Global;
        Ext.Viewport.setMasked({
            xtype: "loadmask",
            message: "Please wait"
        });
        var data = {
            UserName: strEmail,
            Password: strPassword
        };
        Ext.Ajax.request({
            url: GLOBAL.SERVER_URL,
            method: "POST",
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json'
            },
            xhr2: true,
            disableCaching: false,
            jsonData: {
                method: "checkLogin",
                body: data
            },
            success: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                var data = Ext.decode(responce.responseText);
                console.log(data);
                d = data.checkLogin.data;
                if (data.checkLogin.Error == 1 || data.checkLogin.Error == 2) {
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.checkLogin.Message);
                    }, 100);
                    return;
                } else {
                    Ext.getCmp('emailLogin').setValue("");
                    Ext.getCmp('passwordLogin').setValue("");
                    //get the Application Configuation
                    me.getApplicationConfiguration();

                    if (data.checkLogin.data.UserType == "0") { //Mentor
                        localStorage.setItem("idMentorLoginDetail", Ext.encode(data.checkLogin.data));
                        var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
                        me.getHelpText();
                        me.getMentee(MentorLoginUser.Id, true);
                        Mentor.app.application.getController('UpdateProfilePicture').getUpdatedTeamData(); //get Team Data after login
                    } else if (data.checkLogin.data.UserType == "1") {//Mentee
                        localStorage.setItem("idMenteeLoginDetail", Ext.encode(data.checkLogin.data));
                        me.getHelpText();
                        me.getEnterprenuerAction();
                        Mentor.app.application.getController('UpdateProfilePicture').getUpdatedTeamData(); //get Team Data after login
                    }
                }
            },
            failure: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                console.log(responce);
                var connectionExist = Mentor.Global.doesConnectionExist();
                var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                Ext.Function.defer(function () {
                    Ext.Msg.alert('MELS', error);
                }, 100);
            }
        });
    },
    doRegisterSignUp: function (btn) {
        me = this;
        var Email = Ext.getCmp('emailSignUp').getValue();
        var Password = Ext.getCmp('passwordSignUp').getValue();
        var AccessToken = Ext.getCmp('accessTokenSignUp').getValue();
        var Mentee = Ext.getCmp('radioBtnMentee').isChecked();
        var Mentor = Ext.getCmp('radioBtnMentor').isChecked();
        var UserType;
        if (Email == "") {
            Ext.Msg.alert('MELS', 'Please enter email address.', Ext.emptyFn);
        } else if (Password == "") {
            Ext.Msg.alert('MELS', 'Please enter password.', Ext.emptyFn);
        } else if (AccessToken == "") {
            Ext.Msg.alert('MELS', 'Please enter accesstoken that you received in mail.', Ext.emptyFn);
        } else if (me.ValidateEmail(Email) == false) {
            Ext.Msg.alert('MELS', "You have entered an invalid email address.");
        } else if (Password.length < 6) {
            Ext.Msg.alert('MELS', "Password length must be minimum 6 and maximum 8 characters.");
        } else {
            if (Mentee == true) {
                UserType = "1" // Mentee
            } else if (Mentor == true) {
                UserType = "0"  // Mentor
            }
            me.RegisterUser(Email, Password, AccessToken, UserType);
        }
    },
    RegisterUser: function (strEmail, strPassword, strAccessToken, strUserType) {
        var GLOBAL = Mentor.Global;
        Ext.Viewport.setMasked({
            xtype: "loadmask",
            message: "Please wait"
        });
        var data = {
            Email: strEmail,
            Password: strPassword,
            AccessToken: strAccessToken,
            UserType: strUserType
        };
        Ext.Ajax.request({
            url: GLOBAL.SERVER_URL,
            method: "POST",
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json'
            },
            xhr2: true,
            disableCaching: false,
            jsonData: {
                method: "registerUser",
                body: data
            },
            success: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                var data = Ext.decode(responce.responseText);
                console.log(data);
                d = data.registerUser.data;
                if (data.registerUser.Error == 1 || data.registerUser.Error == 2) {
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.registerUser.Message);
                    }, 100);
                    return;
                } else {
                    Ext.getCmp('emailSignUp').setValue("");
                    Ext.getCmp('passwordSignUp').setValue("");
                    Ext.getCmp('accessTokenSignUp').setValue("");
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', 'Registered successfully.', function (text) {
                            var viewport = Ext.Viewport,
                                    mainPanel = viewport.down("#mainviewport");
                            var mainMenu = mainPanel.down("login");
                            if (!mainMenu) {
                                mainMenu = mainPanel.add({xtype: "login"});
                            }
                            mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'right'});
                        });
                    }, 100);
                }
            },
            failure: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                console.log(responce);
                var connectionExist = Mentor.Global.doesConnectionExist();
                var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                Ext.Function.defer(function () {
                    Ext.Msg.alert('MELS', error);
                }, 100);
            }
        });
    },
    doForgotPassword: function (btn) {
        var viewport = Ext.Viewport,
                mainPanel = viewport.down("#mainviewport");
        var mainMenu = mainPanel.down("forgotpassword");
        if (!mainMenu) {
            mainMenu = mainPanel.add({xtype: "forgotpassword"});
        }
        mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'left'});
        Ext.getCmp('idSchoolDetailSelectFieldForgotPasswordScreen').setValue(-1);
        Ext.getCmp('emailForgotPassword').setValue("");
    },
    doResetPassword: function (btn) {
        me = this;
        var GLOBAL = Mentor.Global;
        var Email = Ext.getCmp('emailForgotPassword').getValue();
        var School = Ext.getCmp('idSchoolDetailSelectFieldForgotPasswordScreen').getValue();
        if (School == '-1') {
            Ext.Msg.alert('MELS', "Please select school");
        } else if (Email == "") {
            Ext.Msg.alert('MELS', 'Please enter email address.', Ext.emptyFn);
        } else if (me.ValidateEmail(Email) == false) {
            Ext.Msg.alert('MELS', "You have entered an invalid email address.");
        } else {
            Ext.Viewport.setMasked({
                xtype: "loadmask",
                message: "Please wait"
            });
            var data = {
                Email: Email
            };
            Ext.Ajax.request({
                url: GLOBAL.SERVER_URL,
                method: "POST",
                dataType: 'json',
                headers: {
                    'Content-Type': 'application/json'
                },
                xhr2: true,
                disableCaching: false,
                jsonData: {
                    method: "forgotPassword",
                    body: data
                },
                success: function (responce) {
                    Ext.Viewport.setMasked(false); //Avinash
                    var data = Ext.decode(responce.responseText);
                    console.log(data);
                    d = data.forgotPassword.data;
                    if (data.forgotPassword.Error == 1 || data.forgotPassword.Error == 2) {
                        Ext.Function.defer(function () {
                            Ext.Msg.alert('MELS', data.forgotPassword.Message);
                        }, 100);
                        return;
                    } else {
                        Ext.getCmp('idSchoolDetailSelectFieldForgotPasswordScreen').setValue(-1);
                        Ext.getCmp('emailForgotPassword').setValue("");
                        Ext.Function.defer(function () {
                            Ext.Msg.alert('MELS', data.forgotPassword.Message, function () {
                                var viewport = Ext.Viewport,
                                        mainPanel = viewport.down("#mainviewport");
                                var mainMenu = mainPanel.down("login");
                                if (!mainMenu) {
                                    mainMenu = mainPanel.add({xtype: "login"});
                                }
                                Mentor.Global.NavigationStack = [];
                                mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'right'});
                                Ext.getCmp('idSchoolDetailSelectFieldLoginScreen').setValue(-1);
                                Ext.getCmp('emailLogin').setValue("");
                                Ext.getCmp('passwordLogin').setValue("");
                            });
                        }, 100);
                    }
                },
                failure: function (responce) {
                    Ext.Viewport.setMasked(false); //Avinash
                    console.log(responce);
                    var connectionExist = Mentor.Global.doesConnectionExist();
                    var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', error);
                    }, 100);
                }
            });
        }
    },
    btnBackForgotPassword: function (btn) {
        var viewport = Ext.Viewport,
                mainPanel = viewport.down("#mainviewport");
        var mainMenu = mainPanel.down("login");
        if (!mainMenu) {
            mainMenu = mainPanel.add({xtype: "login"});
        }
        mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'right'});
        Ext.getCmp('idSchoolDetailSelectFieldLoginScreen').setValue(-1);
        Ext.getCmp('emailLogin').setValue("");
        Ext.getCmp('passwordLogin').setValue("");
    },
    doRegister: function (btn) {
        var viewport = Ext.Viewport,
                mainPanel = viewport.down("#mainviewport");
        var mainMenu = mainPanel.down("signup");
        if (!mainMenu) {
            mainMenu = mainPanel.add({xtype: "signup"});
        }
        mainPanel.animateActiveItem(mainMenu, {type: "slide", duration: 450});
    },
    backSignUp: function () {
        var viewport = Ext.Viewport,
                mainPanel = viewport.down("#mainviewport");
        var mainMenu = mainPanel.down("login");
        if (!mainMenu) {
            mainMenu = mainPanel.add({xtype: "login"});
        }
        mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'right'});
    },
    btnSignOutMeetingHistory: function () {
        var viewport = Ext.Viewport,
                mainPanel = viewport.down("#mainviewport");
        var mainMenu = mainPanel.down("signout");
        if (!mainMenu) {
            mainMenu = mainPanel.add({xtype: "signout"});
        }
        Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
        mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'right'});
    },
    btnSignOutMeetingHistoryMentee: function () {
        var viewport = Ext.Viewport,
                mainPanel = viewport.down("#mainviewport");
        var mainMenu = mainPanel.down("signout");
        if (!mainMenu) {
            mainMenu = mainPanel.add({xtype: "signout"});
        }
        Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
        mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'right'});
    },
    btnSignOut: function () {
        clearInterval(Mentor.Global.SET_INTERVAL_ID);
        Mentor.Global.SET_INTERVAL_ID = 0;
        localStorage.clear();
        var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
        mainPanel.removeAll();
        localStorage.clear();
        Mentor.Global.NavigationStack = [];
        Mentor.Global.MEETING_DETAIL = null;
        var mainMenu = mainPanel.down("login");
        if (!mainMenu) {
            mainMenu = mainPanel.add({xtype: "login"});
        }
        mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'left'});
        this.getSchoolDetail();
        this.getHelpText();
    },
    btnbackSignOut: function () {
        var View = Mentor.Global.NavigationStack.pop();
        var viewport = Ext.Viewport,
                mainPanel = viewport.down("#mainviewport");
        mainPanel.animateActiveItem(View, {type: "slide", direction: "left"});
    },
    btnNextEnterpreneur: function (btn) {
        var me = this;
        /*var MeetingDetailSubTopicStore = Ext.getStore('SubTopic').load();
         var MeetingDetailPanel = Ext.getCmp('idMeetingDetailSubTopic');
         for(i = 0 ; i<MeetingDetailSubTopicStore.data.length;i++){
         var checkBoxField = Ext.create('Ext.field.Checkbox', {
         name: 'recorded_stream',
         value: MeetingDetailSubTopicStore.data.getAt(i).data.SubTopicID,
         label: MeetingDetailSubTopicStore.data.getAt(i).data.SubTopicDescription,
         labelCls : 'radio-btn-cls',
         labelWidth: 250,
         listeners: {
         'check': function(radio, e, eOpts) {
         //me.radioHandler(radio.getValue());
         }
         }
         });
         MeetingDetailPanel.add(checkBoxField);
         }*/

        //Store Metting Detail (Change for New UI)
        /*var MeetingTypePanel = Ext.getCmp('idMeetingType');
         for(i = 0 ; i <MeetingTypePanel.getItems().length;i++){
         if(MeetingTypePanel.getItems().getAt(i).isChecked())
         {
         var store = Ext.getStore("Mentee");
         var MenteeSelectField = Ext.getCmp('idEnterpreneurSelectField');
         var MeenteeDetail = {"MeetngTypeID" : MeetingTypePanel.getItems().getAt(i).getValue(),
         "MeetingTypeTitle" : MeetingTypePanel.getItems().getAt(i).getLabel(),
         "MenteeName" : store.findRecord(MenteeSelectField.getValue()).get("MenteeName"),
         "MenteeID" : MenteeSelectField.getValue()}
         
         localStorage.setItem("storeMeetingDetail",Ext.encode(MeenteeDetail));
         //localStorage.setItem("storeMenteedID",Ext.encode(MeetingTypePanel.getItems().getAt(i).getValue()));
         }
         }*/
        //Store Metting Detail (Change for New UI)
        if (Mentor.Global.Timer != "") {
            clearInterval(Mentor.Global.Timer);
        }
        var store = Ext.getStore("Mentee");
        var MenteeSelectField = Ext.getCmp('idEnterpreneurSelectField');
        var storeMettingType = Ext.getStore("MeetingType");
        var MettingTypeSelectField = Ext.getCmp('idMettingTypeSelectField');
        /*** Time Screen Move to Enterpreneur Screen***/
        /*var MeenteeDetail = {"MeetngTypeID" : MettingTypeSelectField.getValue(),
         "MeetingTypeTitle" :  storeMettingType.findRecord(MettingTypeSelectField.getValue()).get("MeetingTypeName"),
         "MenteeName" : store.findRecord(MenteeSelectField.getValue()).get("MenteeName"),
         "MenteeID" : MenteeSelectField.getValue()}
         
         localStorage.setItem("storeMeetingDetail",Ext.encode(MeenteeDetail));
         
         
         var viewport = Ext.Viewport,
         mainPanel = viewport.down("#mainviewport");
         var mainMenu = mainPanel.down("timing");
         if(!mainMenu){
         mainMenu = mainPanel.add({xtype: "timing"});
         }
         
         Ext.getCmp('startSession').setText('Start Session');
         Ext.getCmp('startSession').setDisabled(false);
         Ext.getCmp('endSession').setText('End Session');
         Ext.getCmp('endSession').setDisabled(true);
         Ext.getCmp('idBtnNextTiming').setDisabled(true);
         
         Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
         mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'left'});
         
         */
        /*** End Time Screen Move to Enterpreneur Screen***/

        me.btnNextTiming();
    },
    btnBackEnterpreneur: function () {
        var MeetingDetail = Mentor.Global.MEETING_DETAIL; // From Meeting History
        if (MeetingDetail == null && Ext.getCmp('startSession').getText() != "Start Session") {
            Ext.Msg.confirm("MELS", "Are you sure you want to terminate this meeting?", function (btn) {
                if (btn == 'yes') {
                    Mentor.app.application.getController('Main').doEndSession();
                    if (Mentor.Global.NavigationStack[Mentor.Global.NavigationStack.length - 1].config.xtype != "entrepreneur") {
                        clearInterval(Mentor.Global.Timer);
                    }
                    var View = Mentor.Global.NavigationStack.pop();
                    var View = Mentor.Global.NavigationStack.pop();
                    var viewport = Ext.Viewport,
                            mainPanel = viewport.down("#mainviewport");
                    mainPanel.animateActiveItem(View, {type: "slide", direction: "right"});
                    Ext.getCmp('idMentorBottomTabView').setActiveItem(1);
                    localStorage.setItem("storeStartEndSession", null); // Start and End Session

                    try {
                        clearInterval(Mentor.Global.Timer);
                    } catch (e) {

                    }
                } else {
                    return;
                }
            });
        } else {
            if (Mentor.Global.NavigationStack[Mentor.Global.NavigationStack.length - 1].config.xtype != "entrepreneur") {
                clearInterval(Mentor.Global.Timer);
            }
            var View = Mentor.Global.NavigationStack.pop();
            var View = Mentor.Global.NavigationStack.pop();
            var viewport = Ext.Viewport,
                    mainPanel = viewport.down("#mainviewport");
            mainPanel.animateActiveItem(View, {type: "slide", direction: "right"});
            Ext.getCmp('idMentorBottomTabView').setActiveItem(1);
            localStorage.setItem("storeStartEndSession", null); // Start and End Session

            try {
                clearInterval(Mentor.Global.Timer);
            } catch (e) {

            }
        }
    },
    // Timing Scrren(Topics Screen)
    btnNextTiming: function () {
        //Store Timing Detail (Topics)
        var store = Ext.getStore("Topics");
        var TopicsSelectFieldTimingScreen = Ext.getCmp('idTopicsTimingScreen');
        var ElapsedTime = Ext.getCmp('MeetingElapsedTime');
        var MeetingDetail = Mentor.Global.MEETING_DETAIL; // From Meeting History

        var store = Ext.getStore("Mentee");
        var MenteeSelectField = Ext.getCmp('idEnterpreneurSelectField');
        var storeMettingType = Ext.getStore("MeetingType");
        var MettingTypeSelectField = Ext.getCmp('idMettingTypeSelectField');
        var storeMeetingPlace = Ext.getStore("MeetingPlace");
        var MeetingPlaceSelectField = Ext.getCmp('idMeetingPlaceSelectField');

        if (MeetingDetail != null) {
            if (isNaN(Date.parse(Ext.getCmp('startSessionEditMettingDetail').getValue())))
                Ext.Msg.alert('MELS', "Start session date is not valid");
            else if (isNaN(Date.parse(Ext.getCmp('endSessionEditMettingDetail').getValue())))
                Ext.Msg.alert('MELS', "End session date is not valid");
            else {
                var MeenteeDetail = {
                    "MeetngTypeID": MettingTypeSelectField.getValue(),
                    "MeetingTypeTitle": storeMettingType.findRecord(MettingTypeSelectField.getValue()).get("MeetingTypeName"),
                    "MenteeName": store.findRecord(MenteeSelectField.getValue()).get("MenteeName"),
                    "MenteeID": MenteeSelectField.getValue(),
                    "StartDate": Ext.getCmp('startSessionEditMettingDetail').getValue(),
                    "MeetingEndDatetime": Ext.getCmp('endSessionEditMettingDetail').getValue(),
                    "TopicName": store.findRecord(TopicsSelectFieldTimingScreen.getValue()).get("TopicDescription"),
                    "TopicID": TopicsSelectFieldTimingScreen.getValue(),
                    "MeetingElapsedTime": ElapsedTime.getValue(),
                    "MeetingPlaceID": MeetingPlaceSelectField.getValue(),
                    "MeetingPlaceTitle": storeMeetingPlace.findRecord(MeetingPlaceSelectField.getValue()).get("MeetingPlaceName")
                };
                localStorage.setItem("storeMeetingDetail", Ext.encode(MeenteeDetail));


                /*var TopicsTimingDetail = {"StartDate" : Ext.getCmp('startSessionEditMettingDetail').getValue(),
                 "MeetingEndDatetime" : Ext.getCmp('endSessionEditMettingDetail').getValue(),
                 "TopicName" : store.findRecord(TopicsSelectFieldTimingScreen.getValue()).get("TopicDescription"),
                 "TopicID" : TopicsSelectFieldTimingScreen.getValue(),
                 "MeetingElapsedTime" : ElapsedTime.getValue()}
                 
                 localStorage.setItem("storeMeetingTiming",Ext.encode(TopicsTimingDetail));*/

                var SubTopicStore = Ext.getStore('SubTopic');
                SubTopicStore.clearFilter();
                SubTopicStore.filterBy(function (rec) {
                    return rec.get('TopicID') === TopicsSelectFieldTimingScreen.getValue();
                });
                SubTopicStore.load();
                var viewport = Ext.Viewport,
                        mainPanel = viewport.down("#mainviewport");
                var mainMenu = mainPanel.down("meetingdetails");
                if (!mainMenu) {
                    mainMenu = mainPanel.add({xtype: "meetingdetails"});
                }
                Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
                mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'left'});

                 //Add Metting Topic Name in Meeting Detail scree
                Ext.getCmp('idMeetingMainTopicsMeetingDetailScreen').setHtml(TopicsSelectFieldTimingScreen.getRecord().data.TopicDescription);
            }
        } else {
            if (Ext.getCmp('startSession').getText() == "" || Ext.getCmp('startSession').getText() == "Start Session") {
                Ext.Msg.alert('MELS', "Please click on start session to start meeting");
            } else if (Ext.getCmp('endSession').getText() == "" || Ext.getCmp('endSession').getText() == "End Session") {
                Ext.Msg.alert('MELS', "Please click on end session to enter meeting duration.");
            } else {
                var startDate = Date.parse(Ext.getCmp('startSession').getText());
                var endDate = Date.parse(Ext.getCmp('endSession').getText());
                console.log(startDate);
                console.log(endDate);
                if ((endDate - startDate) < 0) {
                    Ext.Msg.alert('MELS', "End Session datetime should be greater than Start Session datetime");
                } else {
                    var ElapsedTimeInMin = ElapsedTime.getValue().split(" ")[0];
                    if (ElapsedTimeInMin == "0") {
                        ElapsedTimeInMin = "1";
                    }

                    if (Mentor.Global.Timer != "") {
                        clearInterval(Mentor.Global.Timer);
                    }
                    var MeenteeDetail = {
                        "MeetngTypeID": MettingTypeSelectField.getValue(),
                        "MeetingTypeTitle": storeMettingType.findRecord(MettingTypeSelectField.getValue()).get("MeetingTypeName"),
                        "MenteeName": store.findRecord(MenteeSelectField.getValue()).get("MenteeName"),
                        "MenteeID": MenteeSelectField.getValue(),
                        "StartDate": Ext.getCmp('startSession').getText(),
                        "MeetingEndDatetime": Ext.getCmp('endSession').getText(),
                        "TopicName": store.findRecord(TopicsSelectFieldTimingScreen.getValue()).get("TopicDescription"),
                        "TopicID": TopicsSelectFieldTimingScreen.getValue(),
                        "MeetingElapsedTime": ElapsedTimeInMin,
                        "MeetingPlaceID": MeetingPlaceSelectField.getValue(),
                        "MeetingPlaceTitle": storeMeetingPlace.findRecord(MeetingPlaceSelectField.getValue()).get("MeetingPlaceName")
                    };

                    localStorage.setItem("storeMeetingDetail", Ext.encode(MeenteeDetail));

                  

                    /*var TopicsTimingDetail = {"StartDate" : Ext.getCmp('startSession').getText(),
                     "MeetingEndDatetime" : Ext.getCmp('endSession').getText(),
                     "TopicName" : store.findRecord(TopicsSelectFieldTimingScreen.getValue()).get("TopicDescription"),
                     "TopicID" : TopicsSelectFieldTimingScreen.getValue(),
                     "MeetingElapsedTime" : ElapsedTime.getValue()}
                     
                     localStorage.setItem("storeMeetingTiming",Ext.encode(TopicsTimingDetail));*/

                    var MeetingStartEndDetail = {
                        "StartDate": Ext.getCmp('startSession').getText(),
                        "MeetingEndDatetime": Ext.getCmp('endSession').getText(),
                        "MeetingElapsedTime": Ext.getCmp('MeetingElapsedTime').getValue()
                    };

                    localStorage.setItem("storeStartEndSession", Ext.encode(MeetingStartEndDetail));
                    var SubTopicStore = Ext.getStore('SubTopic');
                    SubTopicStore.clearFilter();
                    SubTopicStore.filterBy(function (rec) {
                        return rec.get('TopicID') === TopicsSelectFieldTimingScreen.getValue();
                    });
                    SubTopicStore.load();
                    var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
                    var mainMenu = mainPanel.down("meetingdetails");
                    if (!mainMenu) {
                        mainMenu = mainPanel.add({xtype: "meetingdetails"});
                    }
                    Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
                    mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'left'});

                    //Add Metting Topic Name in Meeting Detail scree
                    Ext.getCmp('idMeetingMainTopicsMeetingDetailScreen').setHtml(TopicsSelectFieldTimingScreen.getRecord().data.TopicDescription);
                }
            }
        }
    },
    calculateElapsedTime: function () {
        var MeetingDetail = Mentor.Global.MEETING_DETAIL; // From Meeting History
        if (MeetingDetail != null) {
            StartSessionDate = Date.parse(Ext.getCmp('startSessionEditMettingDetail').getValue());
            var EndSessionDate;
            //if(Mentor.Global.Timer == "")
            EndSessionDate = Date.parse(Ext.getCmp('endSessionEditMettingDetail').getValue());
            //else
            //	EndSessionDate = Date.parse();

            var DateDiff = EndSessionDate - StartSessionDate;
            var diffHrs = parseInt((DateDiff % 86400000) / 3600000); // hours
            var diffMins = Math.floor(((DateDiff % 86400000) % 3600000) / 60000);
            var EditedHours = Ext.getCmp('idHrsEdited');
            var EditedMinutes = Ext.getCmp('idMinEdited');
            EditedMinutes.setValue(00); //Ext.getCmp("min").setHtml("00");
            EditedHours.setValue(00); //Ext.getCmp("hrs").setHtml("00");
            Ext.getCmp('endSessionEditMettingDetail').removeCls("invalidDate");
            Ext.getCmp('idBtnNextEnterpreneur').setDisabled(false);
            Ext.getCmp('idBtnUpdateEnterpreneur').setDisabled(false);
            diffMins = (diffMins == 0) ? 1 : (diffMins + 1);
            if (diffMins < 10)
                EditedMinutes.setValue("0" + diffMins); //Ext.getCmp("min").setHtml("0"+diffMins);
            else
                EditedMinutes.setValue(diffMins); //Ext.getCmp("min").setHtml(diffMins);

            if (diffHrs < 10)
                EditedHours.setValue("0" + diffHrs); //Ext.getCmp("hrs").setHtml("0"+diffHrs);
            else
                EditedHours.setValue(diffHrs); //Ext.getCmp("hrs").setHtml(diffHrs);


            var ElapsedTime = Ext.getCmp('MeetingElapsedTime');
            if (diffHrs > 0)
                diffMins = diffMins + (diffHrs * 60);
            ElapsedTime.setValue(diffMins + " Minutes");
            //If ElapsedTime is 0 then consider 1 min
            if (diffHrs == 0 && diffMins == 0) {
                ElapsedTime.setValue(01 + " Minutes");
                EditedMinutes.setValue("01");
            }

            if (isNaN(diffHrs) || diffHrs < 0) {
                EditedHours.setValue("00"); //Ext.getCmp("hrs").setHtml("00");
                ElapsedTime.setValue(0 + " Minutes");
                Ext.getCmp('endSessionEditMettingDetail').addCls("invalidDate");
                Ext.getCmp('idBtnNextEnterpreneur').setDisabled(true);
                Ext.getCmp('idBtnUpdateEnterpreneur').setDisabled(true);
            }
            if (isNaN(diffMins) || diffMins < 0) {
                EditedMinutes.setValue("00"); //Ext.getCmp("min").setHtml("00");
                ElapsedTime.setValue(0 + " Minutes");
                Ext.getCmp('endSessionEditMettingDetail').addCls("invalidDate");
                Ext.getCmp('idBtnNextEnterpreneur').setDisabled(true);
                Ext.getCmp('idBtnUpdateEnterpreneur').setDisabled(true);
            }
        } else {
            StartSessionDate = Date.parse(Ext.getCmp('startSession').getText());
            var EndSessionDate;
            /*if (Mentor.Global.Timer == "")
             EndSessionDate = Date.parse(Ext.getCmp('endSession').getText());
             else*/
            EndSessionDate = new Date();
            var DateDiff = EndSessionDate - StartSessionDate;
            var diffHrs = parseInt((DateDiff % 86400000) / 3600000); // hours
            var diffMins = Math.floor(((DateDiff % 86400000) % 3600000) / 60000);

            diffMins = (diffMins == 0) ? 1 : (diffMins + 1);
            if (diffMins < 10)
                Ext.getCmp("min").setHtml("0" + diffMins);
            else
                Ext.getCmp("min").setHtml(diffMins);
            if (diffHrs < 10)
                Ext.getCmp("hrs").setHtml("0" + diffHrs);
            else
                Ext.getCmp("hrs").setHtml(diffHrs);
            var ElapsedTime = Ext.getCmp('MeetingElapsedTime');
            if (diffHrs > 0)
                diffMins = diffMins + (diffHrs * 60);
            ElapsedTime.setValue(diffMins + " Minutes");
        }
    },
    // Calcualted End Time on changing the Elapsed time
    calculateEditedElapsedTime: function (form, e) {
        var me = this;
        var Hours = parseInt(Ext.getCmp('idHrsEdited').getValue());
        var Minutes = parseInt(Ext.getCmp('idMinEdited').getValue());
        var StartSessionDate = Date.parse(Ext.getCmp('startSessionEditMettingDetail').getValue());
        var EndSessionDate = Ext.getCmp('endSessionEditMettingDetail');
        if (Hours < 0 || Hours > 23) {
            Ext.getCmp('idHrsEdited').setValue("00");
        } else if (Minutes < 0 || Minutes > 59) {
            Ext.getCmp('idMinEdited').setValue("00");
        } else {
            if (isNaN(Hours)) {
                Hours = 0;
            }
            if (isNaN(Minutes)) {
                Minutes = 0;
            }

            StartSessionDate.setHours(StartSessionDate.getHours() + Hours);
            StartSessionDate.setMinutes(StartSessionDate.getMinutes() + Minutes);
            var FullYear = StartSessionDate.getFullYear();
            var Month = StartSessionDate.getMonth() + 1;
            if (Month < 10) {
                Month = "0" + Month;
            }
            var Day = StartSessionDate.getDate();
            if (Day < 10) {
                Day = "0" + Day;
            }
            var Hrs = StartSessionDate.getHours();
            if (Hrs < 10) {
                Hrs = "0" + Hrs;
            }
            var Min = StartSessionDate.getMinutes();
            if (Min < 10) {
                Min = "0" + Min;
            }
            var EndSessionDateTime = FullYear + "-" + Month + "-" + Day + " " + Hrs + ":" + Min;
            EndSessionDate.setValue(EndSessionDateTime);
            me.calculateElapsedTime();
        }
    },
    calculateElapsedTimeEditForm: function (form, e) {
        var me = this;
        var StartSessionDate = Date.parse(Ext.getCmp('meetingStartTimeMentorReviewForm').getValue());
        var EndSessionDate = Date.parse(Ext.getCmp('meetingEndTimeReviewForm').getValue());
        var ElapsedTime = Ext.getCmp('meetingElapsedTimeMentorReviewForm');
        if (StartSessionDate == 'Invalid Date' || EndSessionDate == 'Invalid Date' || StartSessionDate == null || EndSessionDate == null) {
            Ext.getCmp('meetingEndTimeReviewForm').addCls("invalidDate");
        } else {
            var DateDiff = EndSessionDate - StartSessionDate;
            var diffMins = parseInt(DateDiff / 60000);
            console.log("diffMins: " + diffMins);
            if (diffMins == 0) {
                Ext.getCmp('meetingEndTimeReviewForm').removeCls("invalidDate");
                diffMins = (diffMins == 0) ? 1 : diffMins;
                ElapsedTime.setValue(diffMins);
            } else if (diffMins < 0) {
                Ext.getCmp('meetingEndTimeReviewForm').addCls("invalidDate");
                ElapsedTime.setValue(0);
            } else {
                diffMins = diffMins;
                Ext.getCmp('meetingEndTimeReviewForm').removeCls("invalidDate");
                ElapsedTime.setValue(diffMins);
            }
        }
    },
    calculateEndTimeEditForm: function (form, e) {
        var me = this;
        var StartSession = Ext.getCmp('meetingStartTimeMentorReviewForm').getValue();
        var StartSessionDate = Date.parse(Ext.getCmp('meetingStartTimeMentorReviewForm').getValue());
        var EndSessionDate = Ext.getCmp('meetingEndTimeReviewForm');
        var ElapsedTime = Ext.getCmp('meetingElapsedTimeMentorReviewForm').getValue();
        if (ElapsedTime == '' || ElapsedTime == 0) {
            EndSessionDate.setValue(StartSession);
            Ext.getCmp('meetingEndTimeReviewForm').removeCls("invalidDate");
//            Ext.getCmp('meetingElapsedTimeMentorReviewForm').setValue(1);
        } else if (ElapsedTime < 0 || isNaN(ElapsedTime)) {
            EndSessionDate.setValue(StartSession);
            Ext.getCmp('meetingEndTimeReviewForm').addCls("invalidDate");
            //Ext.getCmp('meetingElapsedTimeMentorReviewForm').setValue(StartSession);
        } else {
            Ext.getCmp('meetingEndTimeReviewForm').removeCls("invalidDate");
            //var newDate = Date.parse(StartSessionDate.setMinutes(StartSessionDate.getMinutes() + parseInt(ElapsedTime)));
            var newDate = StartSessionDate.addMinutes(ElapsedTime);
            var FullYear = newDate.getFullYear();
            var Month = newDate.getMonth() + 1;
            var date = newDate.getDate();
            var Hours = newDate.getHours();
            var Minutes = newDate.getMinutes();

            Month = (Month < 10) ? "0" + Month : Month;
            date = (date < 10) ? "0" + date : date;
            Hours = (Hours < 10) ? "0" + Hours : Hours;
            Minutes = (Minutes < 10) ? "0" + Minutes : Minutes;

            var full_date = FullYear + "-" + Month + "-" + date + " " + Hours + ":" + Minutes;
            EndSessionDate.setValue(full_date);
        }
    },
    doStartSession: function () {
        me = this;
        var MeetingDetail = Mentor.Global.MEETING_DETAIL; // From Meeting History
        if (MeetingDetail == null) {
            var currentDate = new Date();
            var FullYear = currentDate.getFullYear();
            var Month = currentDate.getMonth() + 1;
            var date = currentDate.getDate();
            var Hours = currentDate.getHours();
            if (Hours < 10)
                Hours = "0" + Hours;
            var Minutes = currentDate.getMinutes();
            if (Minutes < 10)
                Minutes = "0" + Minutes;
            var StartSessionDateTime = FullYear + "-" + Month + "-" + date + " " + Hours + ":" + Minutes;

            var MenteeSelectField = Ext.getCmp('idEnterpreneurSelectField');
            var MeenteeID = MenteeSelectField.getValue();
            me.startEndMeetingSession(1, StartSessionDateTime, MeenteeID);
        }
    },
    doEndSession: function () {
        me = this;
        var MeetingDetail = Mentor.Global.MEETING_DETAIL; // From Meeting History
        if (MeetingDetail == null) {
            clearInterval(Mentor.Global.Timer);

            if (Ext.getCmp('startSession').getText() == "") {
                Ext.Msg.alert('MELS', "Please start session first");
            } else {
                var currentDate = new Date();
                var FullYear = currentDate.getFullYear();
                var Month = currentDate.getMonth() + 1;
                var date = currentDate.getDate();
                var Hours = currentDate.getHours();
                if (Hours < 10) {
                    Hours = "0" + Hours;
                }
                var Minutes = currentDate.getMinutes();
                if (Minutes < 10) {
                    Minutes = "0" + Minutes;
                }
                var StartSessionDateTime = FullYear + "-" + Month + "-" + date + " " + Hours + ":" + Minutes;
                if (Ext.getCmp('MeetingElapsedTime').getValue().split(" ")[0] == "0") {
                    Ext.getCmp('min').setHtml('01');
                }

                /*Ext.getCmp('endSession').setText(StartSessionDateTime);
                 Ext.getCmp('endSession').setDisabled(true);
                 Ext.getCmp('idBtnNextTiming').setDisabled(false);
                 
                 if(Mentor.Global.Timer !="")
                 clearInterval(Mentor.Global.Timer);
                 */

                // 22/02/2016 Move Timing Screen to Enterpreneur Screen
                /*var MeetingDetail = Ext.decode(localStorage.getItem("storeMeetingDetail"));
                 var MeenteeID  = MeetingDetail.MenteeID;
                 */

                var MenteeSelectField = Ext.getCmp('idEnterpreneurSelectField');
                var MeenteeID = MenteeSelectField.getValue();
                me.startEndMeetingSession(2, StartSessionDateTime, MeenteeID);
            }
        }
    },
    getDate: function () {
        return Date();
    },
    btnBackTimingScreen: function (btn) {
        /*var viewport = Ext.Viewport,
         mainPanel = viewport.down("#mainviewport");
         var mainMenu = mainPanel.down("entrepreneur");
         if(!mainMenu){
         mainMenu = mainPanel.add({xtype: "entrepreneur"});
         }
         mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'right'});*/
        var MeetingDetail = Mentor.Global.MEETING_DETAIL; // From Meeting History
        if (MeetingDetail == null)
        {
            if ((Ext.getCmp('startSession').getText() != "" && Ext.getCmp('startSession').getText() != "Start Session") &&
                    (Ext.getCmp('endSession').getText() == "" || Ext.getCmp('endSession').getText() == "End Session"))
            {
                Ext.Msg.confirm("MELS", "This meeting is still in session. Do you wish to end this meeting?", function (btn) {
                    if (btn == 'yes') {
                        var currentDate = new Date();
                        var FullYear = currentDate.getFullYear();
                        var Month = currentDate.getMonth() + 1;
                        var date = currentDate.getDate();
                        var Hours = currentDate.getHours();
                        if (Hours < 10) {
                            Hours = "0" + Hours;
                        }
                        var Minutes = currentDate.getMinutes();
                        if (Minutes < 10) {
                            Minutes = "0" + Minutes;
                        }
                        var StartSessionDateTime = FullYear + "-" + Month + "-" + date + " " + Hours + ":" + Minutes;
                        /*Ext.getCmp('endSession').setText(StartSessionDateTime);
                         Ext.getCmp('endSession').setDisabled(true);
                         Ext.getCmp('idBtnNextTiming').setDisabled(false);
                         
                         if(Mentor.Global.Timer !="")
                         clearInterval(Mentor.Global.Timer);
                         */
                        var MeetingDetail = Ext.decode(localStorage.getItem("storeMeetingDetail"));
                        var MeenteeID = MeetingDetail.MenteeID;
                        me.startEndMeetingSession(2, StartSessionDateTime, MeenteeID);
                    }
                });
            } else {

                if ((Ext.getCmp('startSession').getText() != "" && Ext.getCmp('startSession').getText() != "Start Session") &&
                        (Ext.getCmp('endSession').getText() != "" && Ext.getCmp('endSession').getText() != "End Session")) {
                    Ext.Msg.confirm("MELS", "Do you want to save meeting start and end time details?", function (btn) {
                        if (btn == 'yes') {
                            var startDate = Date.parse(Ext.getCmp('startSession').getText());
                            var endDate = Date.parse(Ext.getCmp('endSession').getText());
                            if (endDate - startDate < 0) {
                                Ext.Msg.alert('MELS', "End Session datetime should be greater than Start Seesion datetime");
                            } else {
                                if (Mentor.Global.Timer != "") {
                                    clearInterval(Mentor.Global.Timer);
                                }
                                var TopicsTimingDetail = {"StartDate": Ext.getCmp('startSession').getText(),
                                    "MeetingEndDatetime": Ext.getCmp('endSession').getText(),
                                    "MeetingElapsedTime": Ext.getCmp('MeetingElapsedTime').getValue()}

                                localStorage.setItem("storeStartEndSession", Ext.encode(TopicsTimingDetail));
                                var View = Mentor.Global.NavigationStack.pop();
                                var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
                                mainPanel.animateActiveItem(View, {type: "slide", direction: "right"});
                            }
                        } else {
                            var View = Mentor.Global.NavigationStack.pop();
                            var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
                            mainPanel.animateActiveItem(View, {type: "slide", direction: "right"});
                            if (typeof Mentor.Global.Timer !== 'undefined') {
                                if (Mentor.Global.Timer != "") {
                                    clearInterval(Mentor.Global.Timer);
                                }
                            }
                            localStorage.setItem("storeStartEndSession", null);
                        }
                    });
                } else {
                    var View = Mentor.Global.NavigationStack.pop();
                    var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
                    mainPanel.animateActiveItem(View, {type: "slide", direction: "right"});
                    localStorage.setItem("storeStartEndSession", null);
                }
            }
        } else {
            var View = Mentor.Global.NavigationStack.pop();
            var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
            mainPanel.animateActiveItem(View, {type: "slide", direction: "right"});
            localStorage.setItem("storeStartEndSession", null);
        }
    },
    // Meeting Detail Screen
    btnNextMeetingDetails: function () {
        // Store Metting Detail Screen (Sub Topics)
        var MeetingDetailSubTopicsPanel = Ext.getCmp('idMeetingDetailSubTopic');
        var SubTopicsID = "";
        var SubTopicsName = ""
        var isChecked = 0;
        for (i = 0; i < MeetingDetailSubTopicsPanel.getItems().length; i++) {
            if (MeetingDetailSubTopicsPanel.getItems().getAt(i).isChecked()) {
                if (isChecked == 0) {
                    SubTopicsID = MeetingDetailSubTopicsPanel.getItems().getAt(i).getValue();
                    SubTopicsName = MeetingDetailSubTopicsPanel.getItems().getAt(i).getLabel();
                    isChecked = isChecked + 1;
                } else {
                    SubTopicsID = SubTopicsID + "," + MeetingDetailSubTopicsPanel.getItems().getAt(i).getValue();
                    SubTopicsName = SubTopicsName + "," + MeetingDetailSubTopicsPanel.getItems().getAt(i).getLabel();
                    isChecked = isChecked + 1;
                }
            }
        }
        //if(isChecked == 0){
        //Ext.Msg.alert('MELS','Please select at least one meeting detail.');
        //}
        //else{
        var MeetingDetailSubTopics = {
            "SubTopicsID": SubTopicsID,
            "SubTopicsTitle": SubTopicsName
        };

        localStorage.setItem("storeMeetingDetailSubTopics", Ext.encode(MeetingDetailSubTopics));
        var MeetingDetail = Mentor.Global.MEETING_DETAIL; // From Meeting History
        if (MeetingDetail != null) {
            var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
            var mainMenu = mainPanel.down("mentor_action_items");
            if (!mainMenu) {
                mainMenu = mainPanel.add({xtype: "mentor_action_items"});
            }
            Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
            mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'left'});
        } else {
            var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
            var mainMenu = mainPanel.down("enterpreneur_action_items");
            if (!mainMenu) {
                mainMenu = mainPanel.add({xtype: "enterpreneur_action_items"});
            }
            Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
            mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'left'});
        }
        //}
    },
    btnBackMeetingDetails: function (btn) {

        /*var viewport = Ext.Viewport,
         mainPanel = viewport.down("#mainviewport");
         var mainMenu = mainPanel.down("timing");
         if(!mainMenu){
         mainMenu = mainPanel.add({xtype: "timing"});
         }
         mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'right'});*/
        var View = Mentor.Global.NavigationStack.pop();
        var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");

        if (typeof Ext.getCmp('idBtnNextTiming') !== 'undefined') {
            Ext.getCmp('idBtnNextTiming').setDisabled(false);
        }
        mainPanel.animateActiveItem(View, {type: "slide", direction: "right"});
        var MeetingStartEndDateTime = Ext.decode(localStorage.getItem('storeStartEndSession'));
        if (Mentor.Global.MEETING_DETAIL == null) {
            if (MeetingStartEndDateTime != null) {
                Ext.getCmp('startSession').setText(MeetingStartEndDateTime.StartDate).setDisabled(true);
                Ext.getCmp('endSession').setText(MeetingStartEndDateTime.MeetingEndDatetime).setDisabled(true);
                Ext.getCmp('idBtnNextEnterpreneur').setDisabled(false);
                Ext.getCmp('MeetingElapsedTime').setValue(MeetingStartEndDateTime.MeetingElapsedTime);
                var StartEndDateTime = MeetingStartEndDateTime.MeetingElapsedTime.split(" ");
                var Hours = parseInt(StartEndDateTime[0] / 60);
                var Minutes = StartEndDateTime[0] % 60;
                Hours = (Hours < 10) ? "0" + Hours : Hours;
                Minutes = (Minutes < 10) ? "0" + Minutes : Minutes;
                Ext.getCmp("hrs").setHtml(Hours);
                Ext.getCmp("min").setHtml(Minutes);
            }
        }
    },
    btnBackSubtopicDiscussed: function (btn) {
        var View = Mentor.Global.NavigationStack.pop();
        var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");

        if (typeof Ext.getCmp('idBtnNextTiming') !== 'undefined') {
            Ext.getCmp('idBtnNextTiming').setDisabled(false);
        }
        mainPanel.animateActiveItem(View, {type: "slide", direction: "right"});

        var MeetingStartEndDateTime = Ext.decode(localStorage.getItem('storeStartEndSession'));
        if (Mentor.Global.MEETING_DETAIL == null) {
            if (MeetingStartEndDateTime != null) {
                Ext.getCmp('startSession').setText(MeetingStartEndDateTime.StartDate).setDisabled(true);
                Ext.getCmp('endSession').setText(MeetingStartEndDateTime.MeetingEndDatetime).setDisabled(true);
                Ext.getCmp('idBtnNextEnterpreneur').setDisabled(false);
                Ext.getCmp('MeetingElapsedTime').setValue(MeetingStartEndDateTime.MeetingElapsedTime);
                var StartEndDateTime = MeetingStartEndDateTime.MeetingElapsedTime.split(" ");
                var Hours = parseInt(StartEndDateTime[0] / 60);
                var Minutes = StartEndDateTime[0] % 60;
                Hours = (Hours < 10) ? "0" + Hours : Hours;
                Minutes = (Minutes < 10) ? "0" + Minutes : Minutes;
                Ext.getCmp("hrs").setHtml(Hours);
                Ext.getCmp("min").setHtml(Minutes);
            }
        }
    },
    btnBackMeetingEditMentorActionItems: function () {
        var View = Mentor.Global.NavigationStack.pop();
        var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
        mainPanel.animateActiveItem(View, {type: "slide", direction: "right"});
    },
    btnBackMeetingEditMenteeActionItems: function () {
        var View = Mentor.Global.NavigationStack.pop();
        var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
        mainPanel.animateActiveItem(View, {type: "slide", direction: "right"});
    },
    btnNextSubtopicDiscussed: function () {
        var SubtopicDiscussedPanel = Ext.getCmp('idSubtopicDiscussed');
        var SubTopicsID = "";
        var SubTopicsName = "";
        var isChecked = 0;
        for (i = 0; i < SubtopicDiscussedPanel.getItems().length; i++) {
            if (SubtopicDiscussedPanel.getItems().getAt(i).isChecked()) {
                if (isChecked == 0) {
                    SubTopicsID = SubtopicDiscussedPanel.getItems().getAt(i).getValue();
                    SubTopicsName = SubtopicDiscussedPanel.getItems().getAt(i).getLabel();
                    isChecked = isChecked + 1;
                } else {
                    SubTopicsID = SubTopicsID + "," + SubtopicDiscussedPanel.getItems().getAt(i).getValue();
                    SubTopicsName = SubTopicsName + "," + SubtopicDiscussedPanel.getItems().getAt(i).getLabel();
                    isChecked = isChecked + 1;
                }
            }
        }
        var MeetingDetailSubTopics = {
            "SubTopicsID": SubTopicsID,
            "SubTopicsTitle": SubTopicsName
        };

        localStorage.setItem("storeMeetingDetailSubTopics", Ext.encode(MeetingDetailSubTopics));
        if (MeetingDetailSubTopics.SubTopicsTitle != "") {
            var SubTopics = MeetingDetailSubTopics.SubTopicsTitle.split(',');
            var SubTopicsName;
            for (i = 0; i < SubTopics.length; i++) {
                if (i == 0) {
                    SubTopicsName = SubTopics[i];
                } else {
                    SubTopicsName = SubTopicsName + ",\n" + SubTopics[i];
                }
            }
            Ext.getCmp('mainIssueDiscussedMentorReviewForm').setValue(SubTopicsName);
            Ext.getCmp('mainIssueDiscussedMentorReviewForm').setMaxRows(SubTopics.length);
        } else {
            Ext.getCmp('mainIssueDiscussedMentorReviewForm').setValue("");
            Ext.getCmp('mainIssueDiscussedMentorReviewForm').setMaxRows(2);
        }


        var View = Mentor.Global.NavigationStack.pop();
        var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
        if (typeof Ext.getCmp('idBtnNextTiming') !== 'undefined') {
            Ext.getCmp('idBtnNextTiming').setDisabled(false);
        }
        mainPanel.animateActiveItem(View, {type: "slide", direction: "right"});
    },
    btnNextMeetingEditMentorActionItems: function () {
        var MentorActions = Ext.getCmp('idMentorActionItem');
        var MentorActionsDone = Ext.getCmp('idMentorActionItemDoneOnMeetingUpdate');
        var MentorActionID = "";
        var MentorActionName = "";
        var MentorActionIDDone = "";
        var MentorActionNameDone = "";
        var isChecked = 0;
        for (i = 0; i < MentorActions.getItems().length; i++) {
            if (MentorActions.getItems().getAt(i).isChecked()) {
                if (isChecked == 0) {
                    MentorActionID = MentorActions.getItems().getAt(i).getValue();
                    MentorActionName = MentorActions.getItems().getAt(i).getItemId();
                    isChecked = isChecked + 1;
                } else {
                    MentorActionID = MentorActionID + "," + MentorActions.getItems().getAt(i).getValue();
                    MentorActionName = MentorActionName + "," + MentorActions.getItems().getAt(i).getItemId();
                    isChecked = isChecked + 1;
                }
            }
        }
        var isChecked = 0;
        for (i = 0; i < MentorActionsDone.getItems().length; i++) {
            if (MentorActionsDone.getItems().getAt(i).isChecked()) {
                if (isChecked == 0) {
                    MentorActionIDDone = MentorActionsDone.getItems().getAt(i).getValue();
                    MentorActionNameDone = MentorActionsDone.getItems().getAt(i).getLabel();
                    isChecked = isChecked + 1;
                } else {
                    MentorActionIDDone = MentorActionIDDone + "," + MentorActionsDone.getItems().getAt(i).getValue();
                    MentorActionNameDone = MentorActionNameDone + "," + MentorActionsDone.getItems().getAt(i).getLabel();
                    isChecked = isChecked + 1;
                }
            }
        }
        var MeetingDetailMentorActionItems = {
            "MentorActionItemID": MentorActionID,
            "MentorActionItemName": MentorActionName,
            "MentorActionItemIDDone": MentorActionIDDone,
            "MentorActionItemNameDone": MentorActionNameDone
        };

        localStorage.setItem("storeMentorActionItems", Ext.encode(MeetingDetailMentorActionItems));
        if (MeetingDetailMentorActionItems.MentorActionItemName != "") {
            var ActionItems = MeetingDetailMentorActionItems.MentorActionItemName.split(',');
            var ActionItemsName;
            for (i = 0; i < ActionItems.length; i++) {
                if (i == 0) {
                    ActionItemsName = ActionItems[i];
                } else {
                    ActionItemsName = ActionItemsName + ",\n" + ActionItems[i];
                }
            }
            Ext.getCmp('actionItemsByMentorMentorReviewForm').setValue(ActionItemsName);
            Ext.getCmp('actionItemsByMentorMentorReviewForm').setMaxRows(ActionItems.length);
        } else {
            Ext.getCmp('actionItemsByMentorMentorReviewForm').setValue("");
            Ext.getCmp('actionItemsByMentorMentorReviewForm').setMaxRows(2);
        }

        var View = Mentor.Global.NavigationStack.pop();
        var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
        mainPanel.animateActiveItem(View, {type: "slide", direction: "right"});
    },
    btnNextMeetingEditMenteeActionItems: function () {
        var MenteeActions = Ext.getCmp('idEnterpreneurActionItem');
        var MenteeActionsDone = Ext.getCmp('idMenteeActionItemDoneOnMeetingUpdate');
        var MenteeActionID = "";
        var MenteeActionName = "";
        var MenteeActionIDDone = "";
        var MenteeActionNameDone = "";
        var isChecked = 0;
        for (i = 0; i < MenteeActions.getItems().length; i++) {
            if (MenteeActions.getItems().getAt(i).isChecked()) {
                if (isChecked == 0) {
                    MenteeActionID = MenteeActions.getItems().getAt(i).getValue();
                    MenteeActionName = MenteeActions.getItems().getAt(i).getItemId();
                    isChecked = isChecked + 1;
                } else {
                    MenteeActionID = MenteeActionID + "," + MenteeActions.getItems().getAt(i).getValue();
                    MenteeActionName = MenteeActionName + "," + MenteeActions.getItems().getAt(i).getItemId();
                    isChecked = isChecked + 1;
                }
            }
        }
        var isChecked = 0;
        for (i = 0; i < MenteeActionsDone.getItems().length; i++) {
            if (MenteeActionsDone.getItems().getAt(i).isChecked()) {
                if (isChecked == 0) {
                    MenteeActionIDDone = MenteeActionsDone.getItems().getAt(i).getValue();
                    MenteeActionNameDone = MenteeActionsDone.getItems().getAt(i).getLabel();
                    isChecked = isChecked + 1;
                } else {
                    MenteeActionIDDone = MenteeActionIDDone + "," + MenteeActionsDone.getItems().getAt(i).getValue();
                    MenteeActionNameDone = MenteeActionNameDone + "," + MenteeActionsDone.getItems().getAt(i).getLabel();
                    isChecked = isChecked + 1;
                }
            }
        }
        var MeetingDetailMenteeActionItems = {
            "MenteeActionItemID": MenteeActionID,
            "MenteeActionItemName": MenteeActionName,
            "MenteeActionItemIDDone": MenteeActionIDDone,
            "MenteeActionItemNameDone": MenteeActionNameDone
        };

        localStorage.setItem("storeEntrepreneurActionItems", Ext.encode(MeetingDetailMenteeActionItems));
        if (MeetingDetailMenteeActionItems.MenteeActionItemName != "") {
            var ActionItems = MeetingDetailMenteeActionItems.MenteeActionItemName.split(',');
            var ActionItemsName;
            for (i = 0; i < ActionItems.length; i++) {
                if (i == 0) {
                    ActionItemsName = ActionItems[i];
                } else {
                    ActionItemsName = ActionItemsName + ",\n" + ActionItems[i];
                }
            }
            Ext.getCmp('actionItemsByMenteeMenteeReviewForm').setValue(ActionItemsName);
            Ext.getCmp('actionItemsByMenteeMenteeReviewForm').setMaxRows(ActionItems.length);
        } else {
            Ext.getCmp('actionItemsByMenteeMenteeReviewForm').setValue("");
            Ext.getCmp('actionItemsByMenteeMenteeReviewForm').setMaxRows(2);
        }

        var View = Mentor.Global.NavigationStack.pop();
        var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
        mainPanel.animateActiveItem(View, {type: "slide", direction: "right"});
    },
    btnNextEntrepreneurItems: function () {
        me = this;
        // Store Mentee Action item (Mentee Action Item)
        var EntrepreneurItemsPanel = Ext.getCmp('idEnterpreneurActionItem');
        var MenteeActionItemID;
        var MenteeActionItemName
        var isChecked = 0;
        for (i = 0; i < EntrepreneurItemsPanel.getItems().length; i++) {
            if (EntrepreneurItemsPanel.getItems().getAt(i).isChecked()) {
                if (isChecked == 0) {
                    MenteeActionItemID = EntrepreneurItemsPanel.getItems().getAt(i).getValue();
                    //MenteeActionItemName = EntrepreneurItemsPanel.getItems().getAt(i).getLabel();
                    MenteeActionItemName = EntrepreneurItemsPanel.getItems().getAt(i).getItemId();
                    isChecked = isChecked + 1;
                } else {
                    MenteeActionItemID = MenteeActionItemID + "," + EntrepreneurItemsPanel.getItems().getAt(i).getValue();
                    MenteeActionItemName = MenteeActionItemName + "," + EntrepreneurItemsPanel.getItems().getAt(i).getItemId();
                    isChecked = isChecked + 1;
                }
            }
        }

        if (isChecked == 0) {
            Ext.Msg.alert('MELS', 'Please select at least one action.');
        } else {
            if (Mentor.Global.MEETING_DETAIL == null) { //New Meeting
                var EntrepreneurActionItemsDonePanel = Ext.getCmp('idMenteeActionItemDoneOnMeetingUpdate');
                var MenteeActionItemDoneID;
                var MenteeActionItemDoneName;
                var isCheckedDone = 0;
                for (i = 0; i < EntrepreneurActionItemsDonePanel.getItems().length; i++) {
                    if (EntrepreneurActionItemsDonePanel.getItems().getAt(i).isChecked()) {
                        if (isCheckedDone == 0) {
                            MenteeActionItemDoneID = EntrepreneurActionItemsDonePanel.getItems().getAt(i).getValue();
                            MenteeActionItemDoneName = EntrepreneurActionItemsDonePanel.getItems().getAt(i).getLabel();
                            isCheckedDone = isCheckedDone + 1;
                        } else {
                            MenteeActionItemDoneID = MenteeActionItemDoneID + "," + EntrepreneurActionItemsDonePanel.getItems().getAt(i).getValue();
                            MenteeActionItemDoneName = MenteeActionItemDoneName + "," + EntrepreneurActionItemsDonePanel.getItems().getAt(i).getLabel();
                            isCheckedDone = isCheckedDone + 1;
                        }
                    }
                }
                var EntrepreneurActionItems = {
                    "MenteeActionItemID": MenteeActionItemID,
                    "MenteeActionItemName": MenteeActionItemName,
                    "MenteeActionItemDoneID": MenteeActionItemDoneID,
                    "MenteeActionItemDoneName": MenteeActionItemDoneName
                };
            } else {
                var EntrepreneurActionItems = {
                    "MenteeActionItemID": MenteeActionItemID,
                    "MenteeActionItemName": MenteeActionItemName
                };
            }

            localStorage.setItem("storeEntrepreneurActionItems", Ext.encode(EntrepreneurActionItems));
            if (Ext.getCmp('idBtnNextEntrepreneurItems').getText() == "Update") {
                me.updateMenteeActionItemsWithDone();
            } else {
                var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
                var mainMenu = mainPanel.down("mentor_action_items");
                if (!mainMenu) {
                    mainMenu = mainPanel.add({xtype: "mentor_action_items"});
                }
                Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
                mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'left'});
            }
        }
    },
    btnBackEnterpreneurActionItems: function (btn) {
        /*var viewport = Ext.Viewport,
         mainPanel = viewport.down("#mainviewport");
         var mainMenu = mainPanel.down("meetingdetails");
         if(!mainMenu){
         mainMenu = mainPanel.add({xtype: "meetingdetails"});
         }
         mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'right'});*/
        var View = Mentor.Global.NavigationStack.pop();
        var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
        mainPanel.animateActiveItem(View, {type: "slide", direction: "right"});
    },
    // Mentor Action items
    btnNextMentorActionItems: function (btn) {
        me = this;
        // Store Mentee Action item (Mentee Action Item)
        var MentorActionItemsPanel = Ext.getCmp('idMentorActionItem');
        var MentorActionItemID;
        var MentorActionItemName
        var isChecked = 0;
        var MentorFeedback = Ext.getCmp('idFeedbackSelectFieldMentorActionTaken');
        for (i = 0; i < MentorActionItemsPanel.getItems().length; i++) {
            if (MentorActionItemsPanel.getItems().getAt(i).isChecked()) {
                if (isChecked == 0) {
                    MentorActionItemID = MentorActionItemsPanel.getItems().getAt(i).getValue();
                    MentorActionItemName = MentorActionItemsPanel.getItems().getAt(i).getItemId();
                    isChecked = isChecked + 1;
                } else {
                    MentorActionItemID = MentorActionItemID + "," + MentorActionItemsPanel.getItems().getAt(i).getValue();
                    MentorActionItemName = MentorActionItemName + "," + MentorActionItemsPanel.getItems().getAt(i).getItemId();
                    isChecked = isChecked + 1;
                }
            }
        }

        // validation
        if (isChecked == 0) {
            Ext.Msg.alert('MELS', 'Please select at least one action.');
        } else {
            var MentorActionItems = {
                "MentorActionItemID": MentorActionItemID,
                "MentorActionItemName": MentorActionItemName,
                "Feedback": MentorFeedback.getValue()
            };
            localStorage.setItem("storeMentorActionItems", Ext.encode(MentorActionItems));
            me.SubmitDetail();
        }
    },
    btnBackMentorActionItems: function (btn) {
        /*var viewport = Ext.Viewport,
         mainPanel = viewport.down("#mainviewport");
         var mainMenu = mainPanel.down("enterpreneur_action_items");
         if(!mainMenu){
         mainMenu = mainPanel.add({xtype: "enterpreneur_action_items"});
         }
         mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'right'});*/
        var View = Mentor.Global.NavigationStack.pop();
        var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
        mainPanel.animateActiveItem(View, {type: "slide", direction: "right"});
    },
    //Select Enterpreneur screen (It will be call at the of login also and at the time of New Meeting and Edit Metting-When
    //	Enterpreneur screen calls if isLogin true it means its call from login else it call at the time of Enterprenur screen)
    getMentee: function (MentorID, isLogin) {
        var GLOBAL = Mentor.Global;
        Ext.Viewport.setMasked({
            xtype: "loadmask",
            message: "Please wait"
        });
        var data = {
            MentorID: MentorID
        };

        Ext.Ajax.request({
            url: GLOBAL.SERVER_URL,
            method: "POST",
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json'
            },
            xhr2: true,
            disableCaching: false,
            jsonData: {
                method: "getMentee",
                body: data
            },
            success: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                var data = Ext.decode(responce.responseText);
                console.log(data);
                d = data.getMentee.data;
                if (data.getMentee.Error == 1) {
                    Ext.Function.defer(function () {
                        Ext.getStore('Mentee').removeAll();
                        Ext.Msg.alert('MELS', data.getMentee.Message);
                    }, 100);
                    return;
                } else if (data.getMentee.Error == 2) {
                    Ext.getStore('Mentee').removeAll();
                    if (isLogin) { // If Service at a time Of Login
                        me.getTopic();
                    }
                } else {
                    //localStorage.setItem("idMentor",Ext.encode(data.getMentee.data));
                    Ext.getStore('Mentee').removeAll();
                    Ext.getStore('Mentee').add(data.getMentee.data);
                    if (isLogin) // If Service at a time Of Login
                        me.getTopic();
                    else { //Check if user comes from meeting detail
                        var MeetingDetail = Mentor.Global.MEETING_DETAIL;
                        if (MeetingDetail != null) {
                            Ext.getCmp('idEnterpreneurSelectField').setValue(MeetingDetail.MenteeID);
                        }
                    }
                }
            },
            failure: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                console.log(responce);
                var connectionExist = Mentor.Global.doesConnectionExist();
                var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                Ext.Function.defer(function () {
                    Ext.Msg.alert('MELS', error);
                }, 100);
            }
        });
    },
    getTopic: function () {
        var GLOBAL = Mentor.Global;
        Ext.Viewport.setMasked({
            xtype: "loadmask",
            message: "Please wait"
        });
        Ext.Ajax.request({
            url: GLOBAL.SERVER_URL,
            method: "POST",
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json'
            },
            xhr2: true,
            disableCaching: false,
            jsonData: {
                method: "getTopic",
                body: "{}"
            },
            success: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                var data = Ext.decode(responce.responseText);
                console.log(data);
                d = data.getTopic.data;
                if (data.getTopic.Error == 1 || data.getTopic.Error == 2) {
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.getTopic.Message);
                    }, 100);
                    return;
                } else {
                    //localStorage.setItem("idMentor",Ext.encode(data.getTopic.data));
                    var defaultVal = {TopicID: "", TopicDescription: "--Select Main Topic--"};
                    Ext.getStore('Topics').add(defaultVal);
                    Ext.getStore('Topics').add(data.getTopic.data);
                    var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
                    var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
                    if (MentorLoginUser != null) {
                        me.getMeetingType();
                    } else if (MenteeLoginUser != null) {
                        me.getSkills();
                    }
                    //me.getMeetingType();
                }
            },
            failure: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                console.log(responce);
                var connectionExist = Mentor.Global.doesConnectionExist();
                var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                Ext.Function.defer(function () {
                    Ext.Msg.alert('MELS', error);
                }, 100);
            }
        });
    },
    getActionItemComments: function () {
        var GLOBAL = Mentor.Global;
        Ext.Viewport.setMasked({
            xtype: "loadmask",
            message: "Please wait"
        });
        Ext.Ajax.request({
            url: GLOBAL.SERVER_URL,
            method: "POST",
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json'
            },
            xhr2: true,
            disableCaching: false,
            jsonData: {
                method: "getActionItemComments",
                body: "{}"
            },
            success: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                var data = Ext.decode(responce.responseText);
                console.log(data);

                Ext.getStore('MenteeActionComments').clearData();
                Ext.getStore('MentorActionComments').clearData();
                if (data.menteeActionComments != "" || data.menteeActionComments.length > 0) {
                    Ext.getStore('MenteeActionComments').add(data.menteeActionComments);
                }

                if (data.mentorActionComments != "" || data.mentorActionComments.length > 0) {
                    Ext.getStore('MentorActionComments').add(data.mentorActionComments);
                }
            },
            failure: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                console.log(responce);
                var connectionExist = Mentor.Global.doesConnectionExist();
                var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                Ext.Function.defer(function () {
                    Ext.Msg.alert('MELS', error);
                }, 100);
            }
        });
    },
    getMeetingType: function () {
        var GLOBAL = Mentor.Global;
        Ext.Viewport.setMasked({
            xtype: "loadmask",
            message: "Please wait"
        });
        Ext.Ajax.request({
            url: GLOBAL.SERVER_URL,
            method: "POST",
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json'
            },
            xhr2: true,
            disableCaching: false,
            jsonData: {
                method: "getMeetingType",
                body: "{}"
            },
            success: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                var data = Ext.decode(responce.responseText);
                console.log(data);
                d = data.getMeetingType.data;
                if (data.getMeetingType.Error == 1 || data.getMeetingType.Error == 2) {
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.getMeetingType.Message);
                    }, 100);
                    return;
                } else {
                    //localStorage.setItem("idMentor",Ext.encode(data.getTopic.data));
                    var defaultVal = {MeetingTypeID: "", MeetingTypeName: "-- Select Meeting Type --", Weight: "100"};
                    Ext.getStore('MeetingType').add(defaultVal);
                    Ext.getStore('MeetingType').add(data.getMeetingType.data);
                    me.getMeetingDetailSubTopic();
                }
            },
            failure: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                console.log(responce);
                var connectionExist = Mentor.Global.doesConnectionExist();
                var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                Ext.Function.defer(function () {
                    Ext.Msg.alert('MELS', error);
                }, 100);
            }
        });
    },
    getMeetingDetailSubTopic: function () {
        var GLOBAL = Mentor.Global;
        Ext.Viewport.setMasked({
            xtype: "loadmask",
            message: "Please wait"
        });
        Ext.Ajax.request({
            url: GLOBAL.SERVER_URL,
            method: "POST",
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json'
            },
            xhr2: true,
            disableCaching: false,
            jsonData: {
                method: "getSubTopic",
                body: "{}"
            },
            success: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                var data = Ext.decode(responce.responseText);
                console.log(data);
                d = data.getSubTopic.data;
                if (data.getSubTopic.Error == 1 || data.getSubTopic.Error == 2) {
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.getSubTopic.Message);
                    }, 100);
                    return;
                } else {
                    //localStorage.setItem("idMentor",Ext.encode(data.getTopic.data));
                    Ext.getStore('SubTopic').add(data.getSubTopic.data);
                    //var MeetingTypeStore = Ext.getStore('SubTopic');

                    me.getEnterprenuerAction();
                }
            },
            failure: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                console.log(responce);
                var connectionExist = Mentor.Global.doesConnectionExist();
                var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                Ext.Function.defer(function () {
                    Ext.Msg.alert('MELS', error);
                }, 100);
            }
        });
    },
    getEnterprenuerAction: function () {
        var GLOBAL = Mentor.Global;
        Ext.Viewport.setMasked({
            xtype: "loadmask",
            message: "Please wait"
        });
        Ext.Ajax.request({
            url: GLOBAL.SERVER_URL,
            method: "POST",
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json'
            },
            xhr2: true,
            disableCaching: false,
            jsonData: {
                method: "getMenteeActionTaken",
                body: "{}"
            },
            success: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                var data = Ext.decode(responce.responseText);
                console.log(data);
                d = data.getMenteeActionTaken.data;
                if (data.getMenteeActionTaken.Error == 1 || data.getMenteeActionTaken.Error == 2) {
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.getMenteeActionTaken.Message);
                    }, 100);
                    return;
                } else {
                    Ext.getStore('EnterpreneurAction').add(data.getMenteeActionTaken.data);
                    var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
                    var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
                    if (MentorLoginUser != null) {
                        me.getMentorAction();
                    } else if (MenteeLoginUser != null) {
                        //me.getSkills();
                        me.getTopic();
                    }
                }
            },
            failure: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                console.log(responce);
                var connectionExist = Mentor.Global.doesConnectionExist();
                var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                Ext.Function.defer(function () {
                    Ext.Msg.alert('MELS', error);
                }, 100);
            }
        });
    },
    getMentorAction: function () {
        var GLOBAL = Mentor.Global;
        Ext.Viewport.setMasked({
            xtype: "loadmask",
            message: "Please wait"
        });
        Ext.Ajax.request({
            url: GLOBAL.SERVER_URL,
            method: "POST",
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json'
            },
            xhr2: true,
            disableCaching: false,
            jsonData: {
                method: "getMentorActionTaken",
                body: "{}"
            },
            success: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                var data = Ext.decode(responce.responseText);
                console.log(data);
                d = data.getMentorActionTaken.data;
                if (data.getMentorActionTaken.Error == 1 || data.getMentorActionTaken.Error == 2) {
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.getMentorActionTaken.Message);
                    }, 100);
                    return;
                } else {
                    //localStorage.setItem("idMentor",Ext.encode(data.getTopic.data));
                    Ext.getStore('MentorAction').add(data.getMentorActionTaken.data);
                    //var MeetingTypeStore = Ext.getStore('SubTopic');
                    me.getMettingPlace();
                }
            },
            failure: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                console.log(responce);
                var connectionExist = Mentor.Global.doesConnectionExist();
                var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                Ext.Function.defer(function () {
                    Ext.Msg.alert('MELS', error);
                }, 100);
            }
        });
    },
    //Get Metting Place
    getMettingPlace: function () {
        var GLOBAL = Mentor.Global;
        Ext.Viewport.setMasked({
            xtype: "loadmask",
            message: "Please wait"
        });
        Ext.Ajax.request({
            url: GLOBAL.SERVER_URL,
            method: "POST",
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json'
            },
            xhr2: true,
            disableCaching: false,
            jsonData: {
                method: "getMeetingPlace",
                body: "{}"
            },
            success: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                var data = Ext.decode(responce.responseText);
                console.log(data);
                d = data.getMeetingPlace.data;
                if (data.getMeetingPlace.Error == 1 || data.getMeetingPlace.Error == 2) {
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.getMeetingPlace.Message);
                    }, 100);
                    return;
                } else {
                    //localStorage.setItem("idMentor",Ext.encode(data.getTopic.data));
                    var defaultVal = {MeetingPlaceID: "", MeetingPlaceName: "-- Select Meeting Place --", Weight: "100"};
                    Ext.getStore('MeetingPlace').add(defaultVal);
                    Ext.getStore('MeetingPlace').add(data.getMeetingPlace.data);
                    //var MeetingTypeStore = Ext.getStore('SubTopic');
                    me.getSkills();
                }
            },
            failure: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                console.log(responce);
                var connectionExist = Mentor.Global.doesConnectionExist();
                var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                Ext.Function.defer(function () {
                    Ext.Msg.alert('MELS', error);
                }, 100);
            }
        });
    },
    getSkills: function () {
        var GLOBAL = Mentor.Global;
        Ext.Viewport.setMasked({
            xtype: "loadmask",
            message: "Please wait"
        });
        Ext.Ajax.request({
            url: GLOBAL.SERVER_URL,
            method: "POST",
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json'
            },
            xhr2: true,
            disableCaching: false,
            jsonData: {
                method: "getSkills",
                body: "{}"
            },
            success: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                var data = Ext.decode(responce.responseText);
                console.log(data);
                d = data.getSkills.data;
                if (data.getSkills.Error == 1 || data.getSkills.Error == 2) {
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.getSkills.Message);
                    }, 100);
                    return;
                } else {
                    //localStorage.setItem("idMentor",Ext.encode(data.getTopic.data));
                    var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
                    var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
                    Ext.getStore('Skills').add(data.getSkills.data);
                    if (MentorLoginUser != null) {
                        var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
                        /*var mainMenu = mainPanel.down("meetinghistory");
                         if(!mainMenu){
                         mainMenu = mainPanel.add({xtype: "meetinghistory"});
                         }
                         mainPanel.animateActiveItem(mainMenu, {type: "slide", duration: 450});*/
                        var mainMenu = mainPanel.down("mentorLeftMenu");
                        if (!mainMenu) {
                            mainMenu = mainPanel.add({xtype: "mentorLeftMenu"});
                        }
                        mainPanel.animateActiveItem(mainMenu, {type: "slide", duration: 450});

                        if (MentorLoginUser.MentorEmail == "") {
                            Ext.getCmp('idMentorBottomTabView').setActiveItem(3);
                            Ext.getCmp('idMentorBottomTabView').getTabBar().getComponent(0).setDisabled(true);
                            Ext.getCmp('idMentorBottomTabView').getTabBar().getComponent(1).setDisabled(true);
                            Ext.getCmp('idMentorBottomTabView').getTabBar().getComponent(2).setDisabled(true);
                            Ext.getCmp('profileBackBtn').setDisabled(true);
                        } else {
                            //Avinash (Tab)
                           /* Ext.getCmp('idMentorBottomTabView').getTabBar().getComponent(0).setDisabled(false);
                            Ext.getCmp('idMentorBottomTabView').getTabBar().getComponent(1).setDisabled(false);
                            Ext.getCmp('idMentorBottomTabView').getTabBar().getComponent(2).setDisabled(false);
                            Ext.getCmp('profileBackBtn').setDisabled(false);*/
                        }
//                        Mentor.app.application.getController('AppDetail').getMeetingHistory();
                    } else if (MenteeLoginUser != null) {
                        var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
                        /*var mainMenu = mainPanel.down("meetinghistorymentee");
                         if(!mainMenu){
                         mainMenu = mainPanel.add({xtype: "meetinghistorymentee"});
                         }
                         mainPanel.animateActiveItem(mainMenu, {type: "slide", duration: 450});*/
                        var mainMenu = mainPanel.down("menteeLeftMenu");
                        if (!mainMenu) {
                            mainMenu = mainPanel.add({xtype: "menteeLeftMenu"});
                        }
                        mainPanel.animateActiveItem(mainMenu, {type: "slide", duration: 450});

                        if (MenteeLoginUser.MenteeEmail == "") {
                            Ext.getCmp('idMenteeBottomTabView').setActiveItem(2);
                            Ext.getCmp('idMenteeBottomTabView').getTabBar().getComponent(0).setDisabled(true);
                            Ext.getCmp('idMenteeBottomTabView').getTabBar().getComponent(1).setDisabled(true);
                            Ext.getCmp('profileBackBtn').setDisabled(true);
                        } else {
                             //Avinash (Tab)
                          /*  Ext.getCmp('idMenteeBottomTabView').getTabBar().getComponent(0).setDisabled(false);
                            Ext.getCmp('idMenteeBottomTabView').getTabBar().getComponent(1).setDisabled(false);
                            Ext.getCmp('profileBackBtn').setDisabled(false);*/
                        }
//                        Mentor.app.application.getController('AppDetail').getMeetingHistory();
                    }
                }
            },
            failure: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                console.log(responce);
                var connectionExist = Mentor.Global.doesConnectionExist();
                var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                Ext.Function.defer(function () {
                    Ext.Msg.alert('MELS', error);
                }, 100);
            }
        });
    },
    getAcceptResponses: function (refreshScreen) {
        var GLOBAL = Mentor.Global;
        Ext.Viewport.setMasked({
            xtype: "loadmask",
            message: "Please wait"
        });
        Ext.Ajax.request({
            url: GLOBAL.SERVER_URL,
            method: "POST",
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json'
            },
            xhr2: true,
            disableCaching: false,
            jsonData: {
                method: "getAcceptResponses",
                body: "{}"
            },
            success: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                var data = Ext.decode(responce.responseText);
                console.log(data);
                d = data.getAcceptResponses.data;
                if (data.getAcceptResponses.Error == 1 || data.getAcceptResponses.Error == 2) {
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.getAcceptResponses.Message);
                    }, 100);
                    return;
                } else {
                    Ext.getStore('AcceptResponses').removeAll();
                    Ext.getStore('AcceptResponses').add(data.getAcceptResponses.data);
                    if (refreshScreen) {
                        var MentorAcceptResponsesStore = Ext.getStore('AcceptResponses');
                        var MentorAcceptResponses = Ext.getCmp('idMentorAcceptResponses');
                        MentorAcceptResponses.removeAll();
                        for (i = 0; i < MentorAcceptResponsesStore.data.length; i++) {
                            var checkBoxField = Ext.create('Ext.field.Checkbox', {
                                name: 'recorded_stream',
                                value: MentorAcceptResponsesStore.data.getAt(i).data.ResponseID,
                                itemId: MentorAcceptResponsesStore.data.getAt(i).data.ResponseName,
                                label: MentorAcceptResponsesStore.data.getAt(i).data.ResponseName,
                                labelCls: 'radio-btn-cls',
                                checked: false,
                                labelWidth: '90%',
                                labelAlign: 'right'
                            });
                            MentorAcceptResponses.add(checkBoxField);
                        }
                    }
                }
            },
            failure: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                console.log(responce);
                var connectionExist = Mentor.Global.doesConnectionExist();
                var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                Ext.Function.defer(function () {
                    Ext.Msg.alert('MELS', error);
                }, 100);
            }
        });
    },
    getUserNotes: function (refreshScreen) {
        var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
        var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
        var UserID = '';
        var UserType = '';
        if (MentorLoginUser != null) {
            UserID = MentorLoginUser.Id;
            UserType = "0"
        } else if (MenteeLoginUser != null) {
            UserID = MenteeLoginUser.Id;
            UserType = "1"
        }

        var data = {
            UserID: UserID,
            UserType: UserType
        };

        var GLOBAL = Mentor.Global;
        Ext.Viewport.setMasked({
            xtype: "loadmask",
            message: "Please wait"
        });
        Ext.Ajax.request({
            url: GLOBAL.SERVER_URL,
            method: "POST",
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json'
            },
            xhr2: true,
            disableCaching: false,
            jsonData: {
                method: "getUserNotes",
                body: data
            },
            success: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                var data = Ext.decode(responce.responseText);
                console.log(data);
                d = data.getUserNotes.data;
                if (data.getUserNotes.Error == 1 || data.getUserNotes.Error == 2) {
                    Ext.getStore('UserNotes').removeAll();
                    if (!refreshScreen) {
//                        Ext.Function.defer(function () {
//                            Ext.Msg.alert('MELS', data.getUserNotes.Message);
//                        }, 100);
                    }
                    return;
                } else {
                    Ext.getStore('UserNotes').removeAll();
                    Ext.getStore('UserNotes').add(data.getUserNotes.data);
                }
            },
            failure: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                console.log(responce);
                var connectionExist = Mentor.Global.doesConnectionExist();
                var error = (!connectionExist || responce.statusText == 'communication failure') ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                Ext.Function.defer(function () {
                    Ext.Msg.alert('MELS', error);
                }, 100);
            }
        });
    },
    getDeclineResponses: function (refreshScreen) {
        var GLOBAL = Mentor.Global;
        Ext.Viewport.setMasked({
            xtype: "loadmask",
            message: "Please wait"
        });
        Ext.Ajax.request({
            url: GLOBAL.SERVER_URL,
            method: "POST",
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json'
            },
            xhr2: true,
            disableCaching: false,
            jsonData: {
                method: "getDeclineResponses",
                body: "{}"
            },
            success: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                var data = Ext.decode(responce.responseText);
                console.log(data);
                d = data.getDeclineResponses.data;
                if (data.getDeclineResponses.Error == 1 || data.getDeclineResponses.Error == 2) {
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.getDeclineResponses.Message);
                    }, 100);
                    return;
                } else {
                    Ext.getStore('DeclineResponses').removeAll();
                    Ext.getStore('DeclineResponses').add(data.getDeclineResponses.data);
                    if (refreshScreen) {
                        var MentorDeclineResponsesStore = Ext.getStore('DeclineResponses');
                        var MentorDeclineResponses = Ext.getCmp('idMentorDeclineResponses');
                        MentorDeclineResponses.removeAll();
                        for (i = 0; i < MentorDeclineResponsesStore.data.length; i++) {
                            var checkBoxField = Ext.create('Ext.field.Checkbox', {
                                name: 'recorded_stream',
                                value: MentorDeclineResponsesStore.data.getAt(i).data.ResponseID,
                                itemId: MentorDeclineResponsesStore.data.getAt(i).data.ResponseName,
                                label: MentorDeclineResponsesStore.data.getAt(i).data.ResponseName,
                                labelCls: 'radio-btn-cls',
                                checked: false,
                                labelWidth: '90%',
                                labelAlign: 'right',
                                listeners: {
                                }
                            });
                            MentorDeclineResponses.add(checkBoxField);
                        }
                    }
                }
            },
            failure: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                console.log(responce);
                var connectionExist = Mentor.Global.doesConnectionExist();
                var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                Ext.Function.defer(function () {
                    Ext.Msg.alert('MELS', error);
                }, 100);
            }
        });
    },
    //get the configuration of the application
    getApplicationConfiguration: function () {
        var GLOBAL = Mentor.Global;
        var data = {};
        Ext.Viewport.setMasked({
            xtype: "loadmask",
            message: "Please wait"
        });
        Ext.Ajax.request({
            url: GLOBAL.SERVER_URL,
            method: "POST",
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json'
            },
            xhr2: true,
            disableCaching: false,
            jsonData: {
                method: "getConfig",
                body: data
            },
            success: function (responce) {
                //Ext.Viewport.setMasked(false);
                var data = Ext.decode(responce.responseText);
                console.log(data);
                if (data.getConfig.Error == 1 || data.getConfig.Error == 2) {
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.getConfig.Message);
                    }, 100);
                    return;
                } else {
                    console.log("Sucess");
                    appConfigData = data.getConfig.data[0];
                    var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
                    if (MentorLoginUser != null) {
                        appConfigData.LoggenInUserId = MentorLoginUser.Id;
                    }
                    var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
                    if (MenteeLoginUser != null) {
                        appConfigData.LoggenInUserId = MenteeLoginUser.Id;
                    }
                    GLOBAL.MENTOR_NAME = appConfigData.mentorName;
                    GLOBAL.MENTEE_NAME = appConfigData.menteeName;
                    GLOBAL.IMPROMPTU_NAME = appConfigData.impromptu;
                    localStorage.setItem(GLOBAL.APPLICATION_CONFIGURATION, Ext.encode(appConfigData));
                }
            },
            failure: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                console.log(responce);
                var connectionExist = Mentor.Global.doesConnectionExist();
                var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                Ext.Function.defer(function () {
                    Ext.Msg.alert('MELS', error);
                }, 100);
            }
        });
    },
    //get the help texts of the application
    getHelpText: function () {
        var GLOBAL = Mentor.Global;
        var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
        var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
        var UserID;
        var UserType;
        var CurrentUserName = "";
        var URL = '';
        if (MentorLoginUser != null) {
            UserID = MentorLoginUser.Id;
            UserType = "0";
            CurrentUserName: MentorLoginUser.MentorName
            Mentor.app.application.getController('Main').getAcceptResponses();
            Mentor.app.application.getController('Main').getDeclineResponses();
            Mentor.app.application.getController('Main').getUserNotes();
            URL = GLOBAL.SERVER_URL;
        } else if (MenteeLoginUser != null) {
            UserID = MenteeLoginUser.Id;
            UserType = "1";
            CurrentUserName: MenteeLoginUser.MenteeName
            Mentor.app.application.getController('Main').getAcceptResponses();
            Mentor.app.application.getController('Main').getDeclineResponses();
            Mentor.app.application.getController('Main').getUserNotes();
            URL = GLOBAL.SERVER_URL;
        } else {
            UserType = "2";
            URL = GLOBAL.HELP_SERVER_URL;
        }

        var data = {
            UserType: UserType,
            CurrentUserName: "CurrentUserName"
        };
        Ext.Viewport.setMasked({
            xtype: "loadmask",
            message: "Please wait"
        });
        Ext.Ajax.request({
            url: URL,
            method: "POST",
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json'
            },
            xhr2: true,
            disableCaching: false,
            jsonData: {
                method: "getHelpText",
                body: data
            },
            success: function (responce) {
                //Ext.Viewport.setMasked(false);
                var data = Ext.decode(responce.responseText);
                console.log(data);
                if (data.getHelpText.Error == 1 || data.getHelpText.Error == 2) {
//                    Ext.Function.defer(function () {
//                        Ext.Msg.alert('MELS', data.getHelpText.Message);
//                    }, 100);
                    return;
                } else {
                    helpConfigData = data.getHelpText.data;
                    localStorage.setItem('helpConfigData', Ext.encode(helpConfigData));
                }

            },
            failure: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                console.log(responce);
                var connectionExist = Mentor.Global.doesConnectionExist();
                var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                Ext.Function.defer(function () {
                    Ext.Msg.alert('MELS', error);
                }, 100);
            }
        });
    },
    SubmitDetail: function () {
        var GLOBAL = Mentor.Global;
        var MeetingDetail = Ext.decode(localStorage.getItem("storeMeetingDetail"));
        //var MeetingTimingDetail = Ext.decode(localStorage.getItem("storeMeetingTiming"));
        var MeetingDetailSubTopics = Ext.decode(localStorage.getItem("storeMeetingDetailSubTopics"));
        var EntrepreneurActionItems = Ext.decode(localStorage.getItem("storeEntrepreneurActionItems"));
        var MentorActionItems = Ext.decode(localStorage.getItem("storeMentorActionItems"));
        var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
        //localStorage.setItem("storeMeetingDetail",Ext.encode(MeenteeDetail));
        //localStorage.setItem("storeMeetingTiming",Ext.encode(TopicsTimingDetail));
        //localStorage.setItem("storeMeetingDetailSubTopics",Ext.encode(MeetingDetailSubTopics));
        //localStorage.setItem("storeEntrepreneurActionItems",Ext.encode(EntrepreneurActionItems));
        //localStorage.setItem("storeMentorActionItems",Ext.encode(MentorActionItems));

        var MeetingID = "0";
        var MentorActionItemDoneID = null;
        var MentorComment = Ext.getCmp('mentorCommentMentorActionItems').getValue();
        if (Ext.getCmp('btnSubmitMentorActionItems').getText() == "Update") {
            MeetingID = GLOBAL.MEETING_DETAIL.MeetingID;
            // Done Mentor Action Items
            /*var MentorActionPanelDoneOnMettingUpdatePanel = Ext.getCmp('idMentorActionItemDoneOnMeetingUpdate');
             
             
             var isChecked = 0;
             for(i = 0 ; i <MentorActionPanelDoneOnMettingUpdatePanel.getItems().length;i++){
             if(MentorActionPanelDoneOnMettingUpdatePanel.getItems().getAt(i).isChecked())
             {
             if(isChecked ==0){
             MentorActionItemDoneID = MentorActionPanelDoneOnMettingUpdatePanel.getItems().getAt(i).getValue();
             
             isChecked = isChecked + 1;
             }
             else{
             MentorActionItemDoneID = MentorActionItemDoneID + ","+MentorActionPanelDoneOnMettingUpdatePanel.getItems().getAt(i).getValue();
             
             isChecked = isChecked + 1;
             }
             }
             }*/
        }

        //New Meeting
        // Done Mentor Action Items
        var MentorActionPanelDoneOnMettingUpdatePanel = Ext.getCmp('idMentorActionItemDoneOnMeetingUpdate');
        var isChecked = 0;
        for (i = 0; i < MentorActionPanelDoneOnMettingUpdatePanel.getItems().length; i++) {
            if (MentorActionPanelDoneOnMettingUpdatePanel.getItems().getAt(i).isChecked()) {
                if (isChecked == 0) {
                    MentorActionItemDoneID = MentorActionPanelDoneOnMettingUpdatePanel.getItems().getAt(i).getValue();
                    isChecked = isChecked + 1;
                } else {
                    MentorActionItemDoneID = MentorActionItemDoneID + "," + MentorActionPanelDoneOnMettingUpdatePanel.getItems().getAt(i).getValue();
                    isChecked = isChecked + 1;
                }
            }
        }

        var data = {
            MeetingID: MeetingID,
            MentorID: MentorLoginUser.Id,
            MenteeID: MeetingDetail.MenteeID,
            MeetingStartDatetime: MeetingDetail.StartDate,
            MeetingEndDatetime: MeetingDetail.MeetingEndDatetime,
            MeetingTypeID: MeetingDetail.MeetngTypeID,
            MeetingTopicID: MeetingDetail.TopicID,
            MeetingSubTopicID: MeetingDetailSubTopics.SubTopicsID,
            MeetingPlaceID: MeetingDetail.MeetingPlaceID,
            MenteeActionIDs: EntrepreneurActionItems.MenteeActionItemID,
            MentorActionIDs: MentorActionItems.MentorActionItemID,
            MeetingFeedback: MentorActionItems.Feedback,
            MeetingElapsedTime: MeetingDetail.MeetingElapsedTime,
            MentorActionItemDone: MentorActionItemDoneID,
            MenteeActionItemDone: EntrepreneurActionItems.MenteeActionItemDoneID,
            MentorComment: MentorComment
        };

        Ext.Viewport.setMasked({
            xtype: "loadmask",
            message: "Please wait"
        });
        Ext.Ajax.request({
            url: GLOBAL.SERVER_URL,
            method: "POST",
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json'
            },
            xhr2: true,
            disableCaching: false,
            jsonData: {
                method: "submitMeetingInfo",
                body: data
            },
            success: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                var data = Ext.decode(responce.responseText);
                console.log(data);
                d = data.submitMeetingInfo.data;
                if (data.submitMeetingInfo.Error == 1 || data.submitMeetingInfo.Error == 2) {
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.submitMeetingInfo.Message);
                    }, 100);
                    return;
                } else {
                    console.log("Sucess");
                    localStorage.setItem("storeMeetingDetail", null);
                    localStorage.setItem("storeMeetingTiming", null);
                    localStorage.setItem("storeMeetingDetailSubTopics", null);
                    localStorage.setItem("storeEntrepreneurActionItems", null);
                    localStorage.setItem("storeMentorActionItems", null);
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', "Meeting details submitted successfully.", function () {
                            var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
                            var mainMenu = mainPanel.down("mentorBottomTabView");
                            if (!mainMenu) {
                                mainMenu = mainPanel.add({xtype: "mentorBottomTabView"});
                            }
                            Mentor.Global.NavigationStack = [];
                            Ext.getCmp('idMentorBottomTabView').setActiveItem(0);
                            mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'right'});
                            Mentor.app.application.getController('AppDetail').getMeetingHistory();
                        });
                    }, 100);
                }
            },
            failure: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                console.log(responce);
                var connectionExist = Mentor.Global.doesConnectionExist();
                var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                Ext.Function.defer(function () {
                    Ext.Msg.alert('MELS', error);
                }, 100);
            }
        });
    },
    updateMeetingDetail: function () {
        var GLOBAL = Mentor.Global;
        var MeetingDetail = Ext.decode(localStorage.getItem("storeMeetingDetail"));
        //var MeetingTimingDetail = Ext.decode(localStorage.getItem("storeMeetingTiming"));
        var MeetingDetailSubTopics = Ext.decode(localStorage.getItem("storeMeetingDetailSubTopics"));
        var EntrepreneurActionItems = Ext.decode(localStorage.getItem("storeEntrepreneurActionItems"));
        var MentorActionItems = Ext.decode(localStorage.getItem("storeMentorActionItems"));
        var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
        // Done Mentor Action Items
        var MentorActionPanelDoneOnMettingUpdatePanel = Ext.getCmp('idMentorActionItemDoneOnMeetingUpdate');
        var MentorActionItemDoneID;
        var isChecked = 0;
        if (MentorActionPanelDoneOnMettingUpdatePanel != null) {
            for (i = 0; i < MentorActionPanelDoneOnMettingUpdatePanel.getItems().length; i++) {
                if (MentorActionPanelDoneOnMettingUpdatePanel.getItems().getAt(i).isChecked()) {
                    if (isChecked == 0) {
                        MentorActionItemDoneID = MentorActionPanelDoneOnMettingUpdatePanel.getItems().getAt(i).getValue();
                        isChecked = isChecked + 1;
                    } else {
                        MentorActionItemDoneID = MentorActionItemDoneID + "," + MentorActionPanelDoneOnMettingUpdatePanel.getItems().getAt(i).getValue();
                        isChecked = isChecked + 1;
                    }
                }
            }
        }

        MeetingID = GLOBAL.MEETING_DETAIL.MeetingID;
        console.log(MentorActionItemDoneID);
        var data = {
            MeetingID: MeetingID,
            MentorID: MentorLoginUser.Id,
            MenteeID: MeetingDetail.MenteeID,
            MeetingStartDatetime: MeetingDetail.StartDate,
            MeetingEndDatetime: MeetingDetail.MeetingEndDatetime,
            MeetingTypeID: MeetingDetail.MeetngTypeID,
            MeetingTopicID: MeetingDetail.TopicID,
            MeetingSubTopicID: MeetingDetailSubTopics.SubTopicsID,
            MeetingPlaceID: MeetingDetail.MeetingPlaceID,
            MenteeActionIDs: EntrepreneurActionItems.MenteeActionItemID,
            MentorActionIDs: MentorActionItems.MentorActionItemID,
            MeetingFeedback: MentorActionItems.Feedback,
            MeetingElapsedTime: MeetingDetail.MeetingElapsedTime,
            MentorActionItemDone: MentorActionItemDoneID
        };

        Ext.Viewport.setMasked({
            xtype: "loadmask",
            message: "Please wait"
        });
        Ext.Ajax.request({
            url: GLOBAL.SERVER_URL,
            method: "POST",
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json'
            },
            xhr2: true,
            disableCaching: false,
            jsonData: {
                method: "submitMeetingInfo",
                body: data
            },
            success: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                var data = Ext.decode(responce.responseText);
                console.log(data);
                d = data.submitMeetingInfo.data;
                if (data.submitMeetingInfo.Error == 1 || data.submitMeetingInfo.Error == 2) {
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.submitMeetingInfo.Message);
                    }, 100);
                    return;
                } else {
                    console.log("Sucess");
                    localStorage.setItem("storeMeetingDetail", null);
                    localStorage.setItem("storeMeetingTiming", null);
                    localStorage.setItem("storeMeetingDetailSubTopics", null);
                    localStorage.setItem("storeEntrepreneurActionItems", null);
                    localStorage.setItem("storeMentorActionItems", null);
                    localStorage.setItem("storeStartEndSession", null);
                    Mentor.Global.MEETING_DETAIL = null;
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', "Meeting details submitted successfully.", function () {
                            var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
                            var mainMenu = mainPanel.down("mentorBottomTabView");
                            if (!mainMenu) {
                                mainMenu = mainPanel.add({xtype: "mentorBottomTabView"});
                            }
                            Mentor.Global.NavigationStack = [];
                            mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'right'});
                            Mentor.app.application.getController('AppDetail').getMeetingHistory();
                        });
                    }, 100);
                }
            },
            failure: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                console.log(responce);
                var connectionExist = Mentor.Global.doesConnectionExist();
                var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                Ext.Function.defer(function () {
                    Ext.Msg.alert('MELS', error);
                }, 100);
            }
        });
    },
    updateMenteeActionItemsWithDone: function () {
        var EntrepreneurItemsPanel = Ext.getCmp('idEnterpreneurActionItem');
        var EntrepreneurItemsDonePanel = Ext.getCmp('idMenteeActionItemDoneOnMeetingUpdate');
        var MenteeActionItemID;
        var MenteeActionItemDoneID;
        var MenteeActionItemName
        var isChecked = 0;
        var isCheckedDone = 0;
        for (i = 0; i < EntrepreneurItemsPanel.getItems().length; i++) {
            if (EntrepreneurItemsPanel.getItems().getAt(i).isChecked()) {
                if (isChecked == 0) {
                    MenteeActionItemID = EntrepreneurItemsPanel.getItems().getAt(i).getValue();
                    MenteeActionItemName = EntrepreneurItemsPanel.getItems().getAt(i).getLabel();
                    isChecked = isChecked + 1;
                } else {
                    MenteeActionItemID = MenteeActionItemID + "," + EntrepreneurItemsPanel.getItems().getAt(i).getValue();
                    MenteeActionItemName = MenteeActionItemName + "," + EntrepreneurItemsPanel.getItems().getAt(i).getLabel();
                    isChecked = isChecked + 1;
                }
            }

            if (EntrepreneurItemsDonePanel.getItems().getAt(i).isChecked()) {
                if (isCheckedDone == 0) {
                    MenteeActionItemDoneID = EntrepreneurItemsPanel.getItems().getAt(i).getValue();
                    isCheckedDone = isCheckedDone + 1;
                } else {
                    MenteeActionItemDoneID = MenteeActionItemDoneID + "," + EntrepreneurItemsDonePanel.getItems().getAt(i).getValue();
                    isCheckedDone = isCheckedDone + 1;
                }
            }
        }

        if (isChecked == 0) {
            Ext.Msg.alert('MELS', 'Please select at least one action.');
        } else {
            var GLOBAL = Mentor.Global;
            var MeetingID = GLOBAL.MEETING_DETAIL.MeetingID;
            var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
            var data = {
                MenteeID: MenteeLoginUser.Id,
                MeetingID: MeetingID,
                ActionId: MenteeActionItemID,
                MenteeActionItemDone: MenteeActionItemDoneID
            };

            Ext.Viewport.setMasked({
                xtype: "loadmask",
                message: "Please wait"
            });
            Ext.Ajax.request({
                url: GLOBAL.SERVER_URL,
                method: "POST",
                dataType: 'json',
                headers: {
                    'Content-Type': 'application/json'
                },
                xhr2: true,
                disableCaching: false,
                jsonData: {
                    method: "updateMenteeactionsItemsWithDone",
                    body: data
                },
                success: function (responce) {
                    Ext.Viewport.setMasked(false); //Avinash
                    var data = Ext.decode(responce.responseText);
                    console.log(data);
                    d = data.updateMenteeactionsItemsWithDone.data;
                    if (data.updateMenteeactionsItemsWithDone.Error == 1 || data.updateMenteeactionsItemsWithDone.Error == 2) {
                        Ext.Function.defer(function () {
                            Ext.Msg.alert('MELS', data.updateMenteeactionsItemsWithDone.Message);
                        }, 100);
                        return;
                    } else {
                        Ext.Function.defer(function () {
                            Ext.Msg.alert('MELS', "Meeting details submitted successfully.", function () {
                                var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
                                var mainMenu = mainPanel.down("menteeBottomTabView");
                                if (!mainMenu) {
                                    mainMenu = mainPanel.add({xtype: "menteeBottomTabView"});
                                }
                                Mentor.Global.NavigationStack = [];
                                mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'right'});
                                Mentor.app.application.getController('AppDetail').getMeetingHistory();
                            });
                        }, 100);
                    }
                },
                failure: function (responce) {
                    Ext.Viewport.setMasked(false); //Avinash
                    console.log(responce);
                    var connectionExist = Mentor.Global.doesConnectionExist();
                    var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', error);
                    }, 100);
                }
            });
        }
    },
    startEndMeetingSession: function (SesssionStatus, DateTime, MenteeID) {
        var GLOBAL = Mentor.Global;
        var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
        var ElapsedTime = Ext.getCmp('MeetingElapsedTime').getValue().split(" ")[0];
        //Start Metting Session send other detail like Meeting Type,Place and Maintopics
        var MettingTypeSelectField = Ext.getCmp('idMettingTypeSelectField');
        var MeetingPlaceSelectField = Ext.getCmp('idMeetingPlaceSelectField');
        var TopicsSelectFieldTimingScreen = Ext.getCmp('idTopicsTimingScreen');
        if (SesssionStatus == 2) {
            if (ElapsedTime == "0") {
                ElapsedTime = "1";
            }
        }

        Ext.Viewport.setMasked({
            xtype: "loadmask",
            message: "Please wait"
        });
        var data = {
            MenteeID: MenteeID,
            MentorID: MentorLoginUser.Id,
            DateTime: DateTime,
            SessionStatus: SesssionStatus,
            ElapsedTime: ElapsedTime,
            MeetingTypeID: MettingTypeSelectField.getValue(),
            MeetingPlaceID: MeetingPlaceSelectField.getValue(),
            MainTopicID: TopicsSelectFieldTimingScreen.getValue()
        };

        if (SesssionStatus == 1) {
            data.FollowupMeetingID = Ext.getCmp('FollowUpMeetingID').getValue();
        }

        Ext.Ajax.request({
            url: GLOBAL.SERVER_URL,
            method: "POST",
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json'
            },
            xhr2: true,
            disableCaching: false,
            jsonData: {
                method: "startEndMeetingSession",
                body: data
            },
            success: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                var data = Ext.decode(responce.responseText);
                console.log(data);
                d = data.startEndMeetingSession.data;
                if (data.startEndMeetingSession.Error == 1) {
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.startEndMeetingSession.Message);
                    }, 100);
                    return;
                } else if (data.startEndMeetingSession.Error == 2) {
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', "Not able to start session.");
                    }, 100);
                } else {
                    localStorage.setItem('SaveStartedMeetingID', Ext.encode({SaveStartedMeetingID: data.startEndMeetingSession.data[0].MeetingID}));
                    // Start Metting Session
                    if (SesssionStatus == 1) {
                        localStorage.setItem('SaveMeetingStartDateTime', Ext.encode({StartTime: DateTime}));
                        var currentDate = new Date();
                        var FullYear = currentDate.getFullYear();
                        var Month = currentDate.getMonth() + 1;
                        var date = currentDate.getDate();
                        var Hours = currentDate.getHours();
                        var Minutes = currentDate.getMinutes();

                        Hours = (Hours < 10) ? "0" + Hours : Hours;
                        Minutes = (Minutes < 10) ? "0" + Minutes : Minutes;
                        date = (date < 10) ? "0" + date : date;
                        Month = (Month < 10) ? "0" + Month : Month;

                        var StartSessionDateTime = FullYear + "-" + Month + "-" + date + " " + Hours + ":" + Minutes;
                        Ext.getCmp('startSession').setText(StartSessionDateTime);
                        Ext.getCmp('startSession').setDisabled(true);
                        Ext.getCmp('endSession').setDisabled(false);
                        var ElapsedTime = Ext.getCmp('MeetingElapsedTime');
                        ElapsedTime.setValue("0 Minutes");
                        Mentor.app.application.getController('Main').calculateElapsedTime();
                        Mentor.Global.Timer = setInterval(function () {
                            Mentor.app.application.getController('Main').calculateElapsedTime();
                            console.log("a")
                        }, 60000);
                        /*var MeetingDetail = Ext.decode(localStorage.getItem("storeMeetingDetail"));
                         var MeenteeID  = MeetingDetail.MenteeID;*/
                    } else { // End Meeting
                        localStorage.setItem('SaveMeetingStartDateTime', Ext.encode({StartTime: null}));
                        var currentDate = new Date();
                        var FullYear = currentDate.getFullYear();
                        var Month = currentDate.getMonth() + 1;
                        var date = currentDate.getDate();
                        var Hours = currentDate.getHours();
                        var Minutes = currentDate.getMinutes();

                        Hours = (Hours < 10) ? "0" + Hours : Hours;
                        Minutes = (Minutes < 10) ? "0" + Minutes : Minutes;
                        date = (date < 10) ? "0" + date : date;
                        Month = (Month < 10) ? "0" + Month : Month;

                        var StartSessionDateTime = FullYear + "-" + Month + "-" + date + " " + Hours + ":" + Minutes;
                        Ext.getCmp('endSession').setText(StartSessionDateTime);
                        Ext.getCmp('endSession').setDisabled(true);
                        Ext.getCmp('idBtnNextEnterpreneur').setDisabled(false);
                        if (Mentor.Global.Timer != "") {
                            clearInterval(Mentor.Global.Timer);
                        }
                        /*var MeetingDetail = Ext.decode(localStorage.getItem("storeMeetingDetail"));
                         var MeenteeID  = MeetingDetail.MenteeID;*/
                    }
                }

            },
            failure: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                console.log(responce);
                var connectionExist = Mentor.Global.doesConnectionExist();
                var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                Ext.Function.defer(function () {
                    Ext.Msg.alert('MELS', error);
                }, 100);
            }
        });
    },
    ValidateEmail: function (mail)
    {
        if (/^\w+([\+.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
            return (true);
        }
        return (false);
        /*if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))
         {
         return (true)
         }
         
         return (false)*/
        //return (true);
    },
    // Introduction Screen , On Skip Button move to Login Screen
    btnSkipIntroductionScreen: function () {
        var viewport = Ext.Viewport,
                mainPanel = viewport.down("#mainviewport");
        var mainMenu = mainPanel.down("login");
        if (!mainMenu) {
            mainMenu = mainPanel.add({xtype: "login"});
        }
        mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'left'});
    },
    checkPlayStoreVersion: function () {
        if ("android") {
            $.get("https://play.google.com/store/apps/details?id=com.melstm&hl=en", function (data) {
                var version = data.substr((data.indexOf('softwareVersion')), 30).split('>')[1].split('<')[0].trim(' ');
                console.log(version);
            });
        } else if ("ios") {
            $.get("https://itunes.apple.com/us/app/melstm/id1070132202?ls=1&mt=8", function (data) {
                var version = data.substr(data.indexOf('Version: '), 60).split('>')[2].split('<')[0];
                console.log(version);
            });
        }
        Mentor.Global.VERSION_NAME.split('v')[1].split(' ')[0];
    }
});
