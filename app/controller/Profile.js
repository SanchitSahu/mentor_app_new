Ext.define('Mentor.controller.UpdateMeetingDetail', {
    extend: 'Ext.app.Controller',
    config: {
        refs: {},
        control: {
            "entrepreneur button[action=btnUpdateEnterpreneur]": {
                tap: "btnUpdateEnterpreneur"
            },
            "timing button[action=btnUpdateTiming]": {
                tap: "btnUpdateTiming"
            },
            "meetingdetails button[action=btnUpdateMeetingDetails]": {
                tap: "btnUpdateMeetingDetails"
            }
        }
    },
    //called when the Application is launched, remove if not needed
    launch: function (app) {},
    btnUpdateEnterpreneur: function () {
        MeetingTypePanel = Ext.getCmp('idMeetingType');
        for (i = 0; i < MeetingTypePanel.getItems().length; i++) {
            if (MeetingTypePanel.getItems().getAt(i).isChecked())
            {
                var store = Ext.getStore("Mentee");
                var MenteeSelectField = Ext.getCmp('idEnterpreneurSelectField');
                var MeenteeDetail = {
                    "MeetngTypeID": MeetingTypePanel.getItems().getAt(i).getValue(),
                    "MeetingTypeTitle": MeetingTypePanel.getItems().getAt(i).getLabel(),
                    "MenteeName": store.findRecord(MenteeSelectField.getValue()).get("MenteeName"),
                    "MenteeID": MenteeSelectField.getValue()
                };
                localStorage.setItem("storeMeetingDetail", Ext.encode(MeenteeDetail));
                //localStorage.setItem("storeMenteedID",Ext.encode(MeetingTypePanel.getItems().getAt(i).getValue()));
            }
        }
        Mentor.app.application.getController('Main').updateMeetingDetail();
    },
    btnUpdateTiming: function () {
        var store = Ext.getStore("Topics");
        var TopicsSelectFieldTimingScreen = Ext.getCmp('idTopicsTimingScreen');
        var ElapsedTime = Ext.getCmp('MeetingElapsedTime');
        var MeetingDetail = Mentor.Global.MEETING_DETAIL; // From Meeting History

        if (MeetingDetail != null) {
            if (isNaN(Date.parse(Ext.getCmp('startSessionEditMettingDetail').getValue()))) {
                Ext.Msg.alert('MELS', "Start session date is not valid");
            } else if (isNaN(Date.parse(Ext.getCmp('endSessionEditMettingDetail').getValue()))) {
                Ext.Msg.alert('MELS', "End session date is not valid");
            } else {
                var TopicsTimingDetail = {
                    "StartDate": Ext.getCmp('startSessionEditMettingDetail').getValue(),
                    "MeetingEndDatetime": Ext.getCmp('endSessionEditMettingDetail').getValue(),
                    "TopicName": store.findRecord(TopicsSelectFieldTimingScreen.getValue()).get("TopicDescription"),
                    "TopicID": TopicsSelectFieldTimingScreen.getValue(),
                    "MeetingElapsedTime": ElapsedTime.getValue()
                };

                localStorage.setItem("storeMeetingTiming", Ext.encode(TopicsTimingDetail));
                Mentor.app.application.getController('Main').updateMeetingDetail();
            }
        }
    },
    btnUpdateMeetingDetails: function () {
        var MeetingDetailSubTopicsPanel = Ext.getCmp('idMeetingDetailSubTopic');
        var SubTopicsID;
        var SubTopicsName
        var isChecked = 0;
        for (i = 0; i < MeetingDetailSubTopicsPanel.getItems().length; i++) {
            if (MeetingDetailSubTopicsPanel.getItems().getAt(i).isChecked()) {
                if (isChecked == 0) {
                    SubTopicsID = MeetingDetailSubTopicsPanel.getItems().getAt(i).getValue();
                    SubTopicsName = MeetingDetailSubTopicsPanel.getItems().getAt(i).getLabel();
                    isChecked = isChecked + 1;
                } else {
                    SubTopicsID = SubTopicsID + "," + MeetingDetailSubTopicsPanel.getItems().getAt(i).getValue();
                    SubTopicsName = SubTopicsName + "," + MeetingDetailSubTopicsPanel.getItems().getAt(i).getLabel();
                    isChecked = isChecked + 1;
                }
            }
        }
        if (isChecked == 0) {
            Ext.Msg.alert('MELS', 'Please select at least one meeting detail.');
        } else {
            var MeetingDetailSubTopics = {
                "SubTopicsID": SubTopicsID,
                "SubTopicsTitle": SubTopicsName
            };
            localStorage.setItem("storeMeetingDetailSubTopics", Ext.encode(MeetingDetailSubTopics));
            Mentor.app.application.getController('Main').updateMeetingDetail();
        }
    }
});