Ext.define('Mentor.Global', {
    singleton: true,
    SESSION: null,
    CURRENT_LOGIN_USER: null,
    //LIVE CODE
    SERVER_URL: "",
    WEBVIEW_URL: "",
    WEBVIEW_URL_PART: ".melstm.net/mentor_staging/admin/",
    //HELP_SERVER_URL: "http://melstm.net/mentor/webservices/index.php/mentorService",
    //URL: "http://melstm.net/mentor/webservices/index.php/mentorService",
    //URL_PART: ".melstm.net/mentor/webservices/index.php/mentorService",
    //STAGING CODE
    HELP_SERVER_URL: "http://melstm.net/mentor_staging/webservices/index.php/mentorService",
    URL: "http://melstm.net/mentor_staging/webservices/index.php/mentorService",
    URL_PART: ".melstm.net/mentor_staging/webservices/index.php/mentorService",
    VERSION_NAME: "MELS v16.2 17/09/06",
    NavigationStack: [],
    TabNavigationStack: [],
    MEETING_DETAIL: null,
    MENTOR_PENDING_REQUEST: null,
    //Mentor Invites
    MENTOR_PENDING_TITLE: "Home",//MENTOR_PENDING_TITLE: "Pending",
    MENTOR_PENDING_REQUEST_TITLE: "Invites",
    MENTOR_PENDING_MEETING_TITLE: "Meetings",
    MENTOR_ISSUE_INVITE_TITLE: "New Invite",
    MENTEE_ACCEPTED_TITLE: "Yes",
    MENTEE_PENDING_TITLE: "May",
    MENTEE_DECLINED_TITLE: "No",
    MENTEE_ISSUE_INVITE_TITLE: "New",
    MENTEE_INVITES_TITLE: "Home",//MENTEE_INVITES_TITLE: "Invites",
    SET_INTERVAL_ID: '',
    INSTANT_MEETING_INTERVAL_ID: '',
    INSTANT_MEETING_DATA: '',
    SET_INTERVAL_TIME: 60000,
    // Local Storage ID
    APPLICATION_CONFIGURATION: "applicationConfiguration",
    MENTOR_NAME: "",
    MENTEE_NAME: "",
    IMPROMPTU_NAME: "",
    SANCHIT: "",
    INSTANT_MEETING_TAB_NAME: "",
    TEAM_MANAGER: "",
    PROFILE_IMAGE: "",
    INTERNET_ERROR_MESSAGE: "We are having trouble connecting to the Internet. Please check your internet connection and try again later",
    ERROR_MESSAGE: "The system has encounted an unexpected error.<br>It has been logged and we will attend to it immediately.<br> In the meantime, please try to repeat what you were doing in case the error is transient.<br> If that does not get the results you want, try a different approach, if possible, \nto get around the issue.",
    //VIDEO_URL: 'https://www.youtube.com/embed/Zr3ehm57ZJw?rel=0&autoplay=1',
    VIDEO_URL: 'https://www.youtube.com/embed/Mx_xJD6oSg0?rel=0&autoplay=1',
    //CLEAR_IMPROMPTU: false,
    Timer: '',
    Current_date: new Date(),
    clearNavigationStack: function () {
        NavigationStack = [];
    },
    doesConnectionExist: function () {
        var xhr = new XMLHttpRequest();
        var file = "http://melstm.net/mentor/assets/admin/images/ajax-loader_dark.gif";
        var randomNum = Math.round(Math.random() * 10000);
        xhr.open('HEAD', file + "?rand=" + randomNum, false);
        try {
            xhr.send();
            if (xhr.status >= 200 && xhr.status < 304) {
                return true;
            } else {
                return false;
            }
        } catch (e) {
            return false;
        }
    }
});
