Ext.define('Mentor.view.AddCommentPopupView', {
    extend: 'Ext.Panel',
    xtype: 'addcommentpopview',
    requires: [],
    config: {
        cls: 'addCommentPopupScreen',
        defaults: {
        },
        items: [
            {
                xtype: 'textfield',
                id: 'idAddEditCommentID',
                hidden: true
            },
            {
                xtype: 'textfield',
                id: 'idAddEditActionItemID',
                hidden: true
            },
            {
                xtype: 'textfield',
                id: 'idAddEditActionItemType',
                hidden: true
            },
            {
                xtype: 'textfield',
                id: 'idAddEditCommentMeetingID',
                hidden: true
            },
            {
                xtype: 'label',
                html: "Comment",
                style: 'font-weight: bold;',
                padding: '20 10 3 10'
            },
            {
                xtype: 'textareafield',
                //height: '197px',
                style: "background-color: #AFEEEE;",
                id: 'idAddEditCommentDescription',
                margin: '0 10 0 10',
                cls: 'textFieldDisableCLS',
                disabled: false,
                inputCls: 'inputCLS',
                clearIcon: false
            },
            {
                xtype: 'container',
                layout: {
                    type: 'hbox',
                    pack: 'center'
                },
                margin: '5 5 10 5',
                items: [
                    {
                        xtype: 'button',
                        text: 'SAVE',
                        //cls : 'tick',
                        //cls: 'signin-btn-cls-comment',
                        cls: 'signin-btn-cls',
                        pressedCls : 'press-btn-cls',
                        action: "doSubmitAddEditComment",
                        flex: 1,
                        margin: '10 5 0 5'
                    },
                    {
                        xtype: 'button',
                        text: 'CANCEL',
                        //cls : 'cancel',
                        //cls: 'decline-btn-cls-comment',
                        pressedCls : 'press-grey_btn-cls',
                        cls: 'grey-btn-cls',
                        flex: 1,
                        margin: '10 5 0 5',
                        listeners: {
                            tap: function () {
                                var popupObject = Ext.getCmp('addEditCommentPopUp');
                                popupObject.hide();
                            }
                        }
                    }
                ]
            }
        ]
    },
    listeners: [
        {
            event: "activate",
            fn: "onPageActivate"
        },
        {
            event: "painted",
            fn: "onPagePainted"
        }
    ],
    onPagePainted: function () {}
});