Ext.define('Mentor.view.ForgotPassword', {
    extend: 'Ext.Panel',
    xtype: 'forgotpassword',
    requires: [
    ],
    config: {
        cls: 'forgotpassword',
        defaults: {
            xtype: 'formpanel'
        },
        items: [
            {
                docked: 'top',
                xtype: 'toolbar',
                //title: 'Forgot Password',
                cls: 'my-titlebar',
                style : 'background:transparent;',
                defaults: {
                    iconMask: true
                },
                items: [
                    {
                        iconCls: 'btnBackGreyCLS',
                        action: 'btnBackForgotPassword',
                        docked: 'left'
                    },
                    {
                        //iconCls: 'helpIcon',
                        iconCls: 'loginHelpIcon',
                        docked: 'right',
                        listeners: {
                            element: 'element',
                            tap: function () {
                                var popup = Mentor.app.application.getController('AppDetail').addHelpPopup('forgot_password');
                                Ext.Viewport.add(popup);
                                popup.show();
                            }
                        }
                    }
                ]
            },
            {
                xtype: 'panel',
                cls: 'login-top',
                paddingTop: 10,
                layout: {
                    type: 'hbox',
                    pack: 'center'
                },
                items: [
                    {
                        xtype: 'container',
                        hidden :true,
                        layout: {
                            type: 'vbox',
                            pack: 'center'
                        },
                        items: [
                            {
                                xtype: 'label',
                                html: 'Please provide your registered email here, we will send your password to your registered email address.',
                                style: 'font-size:16px;color:#BBBBBB;margin:10px;'
                            }
                        ]
                    }
                ]
            },
            {
                xtype: 'panel',
                flex: 1,
                style: 'margin-top:25px;',
                items: [
                    {
                        xtype: 'image',
                        src: 'resources/images/app_logo.png',
                        height: 90,
                        width: 90,
                        margin: 'auto',
                   
                    },
                    {
                        xtype: 'label',
                        html: 'Forgot Password',
                        cls: 'signInLabelLoginCLS',
                        margin : '40 0 40 0',
                    },
                    {
                        xtype: 'selectfield',
                        id: 'idSchoolDetailSelectFieldForgotPasswordScreen',
                        store: 'SchoolDetail',
                        displayField: 'school_name',
                        valueField: 'subdomain_id',
                        margin: '20 40 20 40',
                        //style: "border-radius:0.4em",
                        cls: 'schoolNameLoginCLS',
                        listeners: {
                            change: function (field, value) {
                                if (value instanceof Ext.data.Model) {
                                    value = value.get(field.getValueField());
                                }
                                console.log(value);
                                var Store = Ext.getStore('SchoolDetail');
                                var record = Store.findRecord('subdomain_id', value);
                                if (record != null) {
                                    Mentor.Global.SERVER_URL = "http://" + record.data.subdomain_name + Mentor.Global.URL_PART;
                                    localStorage.setItem("ServerURL", Mentor.Global.SERVER_URL);
                                }
                            }
                        }
                    },
                    {
                        xtype: 'emailfield',
                        name: 'email',
                        id: 'emailForgotPassword',
                        placeHolder: 'Email',
                        cls: 'userNameLoginCLS',
                        margin: '20 40 20 40',
                        
                    },
                    {
                        xtype : 'panel',
                        layout: {
                            type: 'hbox',
                           
                        },
                        margin: '0 30',
                        items : [
                            {
                                xtype: 'button',
                                //text: 'RESET PASSWORD',
                                text: 'SUBMIT',
                                action: 'btnResetPassword',
                                pressedCls : 'press-btn-cls',
                                cls: 'signin-btn-cls',
                                flex:1
                            },
                            {
                                xtype: 'button',
                                //text: 'RESET PASSWORD',
                                text: 'CANCEL',
                                action: 'btnBackForgotPassword',
                                pressedCls : 'press-grey_btn-cls',
                                cls: 'grey-btn-cls',
                                flex:1
                            }
                        ]

                    }
                    
                ]
            }
        ]
    }
});