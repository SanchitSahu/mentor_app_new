Ext.define('Mentor.view.InviteStatusRejected', {
    extend: 'Ext.dataview.DataView',
    xtype: 'inviteStatusRejected',
    requires: [],
    config: {
        cls: 'meetinghistory',
        store: 'WaitScreenMentorRejected',
        inline: true,
        itemTpl: /*'<div class="myContent {Status}Invitation" style = "display: flex;min-height:55px;">' +
                    '<div style="width:20%; float:left; ">' +
                        '<img src="{MentorImage}" id="circle" height="50px" width="50px" style = "border-radius: 50%;" />' +
                    '</div>' +
                    '<div style="width:80%;float:left;padding: 0px 0px !important;vertical-align: bottom;min-height: 16px;">' +
                        '<div style="width:100%">' +
                            '<div style="width:69%; float:left;font-size: 14px;"><b>{MentorName}</b>'+
                            '</div>' +
                            '<div style="width:31%; float:right;">' +
                                //'<span style = "float:right;font-size: 11px;">{(Status == "Rejected") ? "Declined" : "Status"}</span>' +
                                //'<span style = "float:right;font-size: 11px;"><tpl if="Status == \'Rejected\'">Declined<tpl else >{Status}</tpl></span>' +
                                '<span style = "float:right;font-size: 11px;">' +
                                    '<tpl if="Status == \'Rejected\'">' +
                                        '<tpl if="rescheduledBy == \'1\'">Reschedule Denied' +
                                        '<tpl else>Declined<img class="removeEntry" src="./resources/images/cancel.png"  height="20px" width="20px">' +
                                        '</tpl>' +
                                    '<tpl else>{Status}<img class="removeEntry" src="./resources/images/cancel.png"  height="20px" width="20px">' +
                                    '</tpl>' +
                                '</span>' +
                            '</div>' +
               
                             //Main Topics
                             '<div style="width:100%;float:left;margin-top:5px;">' +
                                '<span style="float:left;font-size:11px;color: black;">Main Topic : {TopicDescription}'+
                                '</span>' +
                                '<div style="float:right;">' +
                                    '<span style="float:left;vertical-align: baseline;">' +
                                        '<img src="./resources/images/clock.png"  height="20px" width="20px">' +
                                    '</span>' +
                                    '<span style="vertical-align: text-top;float:right;font-size:11px;color: black;">{AcceptOrRejectTime}'+
                                    '</span>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                     '</div>' +
                '</div>',*/

                 '<div class="myContent {Status}Invitation" style = "max-width: 160px;min-height:225px;width:80%;margin:10px auto;box-shadow: 1px 1px 7px 2px #888888;">' +
                    '<div style="width:100%; float:left;text-align: center; ">' +
                        '<img src={MentorImage} id="cricle" height="90px" width="90px" style = "border-radius: 100%;background-color:#ffffff">' +
                    '</div>' +
                    '<div style="width:100%;float:left;padding: 0px 0px !important;vertical-align: bottom;min-height: 16px;">' +
                        '<div style="width:100%">'+
                            '<div style="width:100%; float:left;font-size: 14px;text-align: center;white-space: nowrap; overflow: hidden;text-overflow: ellipsis;">'+
                                '<b>{MentorName}</b>'+
                            '</div>' +
                            '<div style="width:100%;text-align: center;font-size:12px;">' +
                                '<span style = "font-size: 11px;">' +
                                    '<tpl if="Status == \'Rejected\'">' +
                                        '<tpl if="rescheduledBy == \'1\'">Reschedule Denied' +
                                        '<tpl else>Declined<img class="removeEntry" src="./resources/images/cancel.png"  height="20px" width="20px">' +
                                        '</tpl>' +
                                    '<tpl else>{Status}<img class="removeEntry" src="./resources/images/cancel.png"  height="20px" width="20px">' +
                                    '</tpl>' +
                                '</span>' +
                            '</div>' +
                        '</div>' +
                        //Main Topics
                        '<div style="width:100%;float:left;margin-top:5px;">' +
                            '<span style="float:left;font-size:11px;color: black;width: 100%;text-align: center;white-space: nowrap; overflow: hidden;text-overflow: ellipsis; ">Main Topic : {TopicDescription}'+
                            '</span>' +
                            '<div style="clear: both;text-align: center;">' +
                               /* '<span style="vertical-align: baseline;">'+
                                    '<img src="./resources/images/clock.png"  height="20px" width="20px">'+
                                '</span>'+*/
                                '<span style="padding-left:20px;position:relative;vertical-align: text-top;font-size:11px;color: black;">'+
                                    '<img src="./resources/images/clock.png" style="position:absolute;left:0;top:-1px;" height="15px" width="15px">{AcceptOrRejectTime}'+
                                '</span>'+
                            '</div>' +
                            '<div  style = "text-align: center;font-size:14px">'+
                                '<input class="btnViewDetail" type="button" id="btn{id}" value="View Detail">'+
                            '</div>'+
                        '</div>' + 
                    '</div>' + 
                '</div>',
        items: [
            {
                xtype: 'label',
                id: 'idUserNameInviteStatusList',
                style: 'text-align: right;padding-right: 10px;padding: 10px;background-color: #C2C2C2;',
                docked: 'top',
                hidden: true
            },
            {
                xtype: 'label',
                html: Mentor.Global.VERSION_NAME,
                style: 'text-align: right;padding-right: 10px;padding: 10px;',
                docked: 'bottom',
                hidden: true
            }
        ],
        listeners: [
            {
                event: "activate",
                fn: "onPageActivate"
            },
            {
                event: "painted",
                fn: "onPagePainted"
            },
            {
                event: "itemtap",
                fn: function (view, index, item, record, e) {
                    if (Ext.dom.Element.fly(e.target).hasCls('removeEntry')) {
                        Ext.Msg.confirm("MELS", "Are you sure you want to remove this entry?", function (btn) {
                            if (btn == 'yes')
                                //Mentor.app.application.getController('AppDetail').editNotePopup(view, index);
                                Mentor.app.application.getController('AppDetail').removeCancelledMeetingEntry(view, index);
                        });
                    } else {
                        Mentor.app.application.getController('AppDetail').displayInvitationSendDetailInWaitScreen(view, index, item, e);
                    }
                }
            }
        ]
    },
    onPageActivate: function () {
        //Check The Login User and display the name on the top header
        var MentorLoginDetail = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
        var MenteeLoginDetail = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));

        if (MentorLoginDetail != null) {
            Ext.getCmp('idUserNameInviteStatusList').setHtml(MentorLoginDetail.MentorName);
        } else if (MenteeLoginDetail != null) {
            Ext.getCmp('idUserNameInviteStatusList').setHtml(MenteeLoginDetail.MenteeName);
        }
    },
    onPagePainted: function () {
        //Mentor.app.application.getController('AppDetail').getMentorForWaitScreen();
        Mentor.app.application.getController('AppDetail').getMentorWaitScreenRejected();
    }
});
