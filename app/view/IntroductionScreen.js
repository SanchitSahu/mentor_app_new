Ext.define('Mentor.view.IntroductionScreen', {
    extend: 'Ext.Carousel',
    xtype: 'introductionscreen',
    requires: [
    ],
    config: {
        cls: 'forgotpassword',
        fullscreen: true,
        direction: 'horizontal',
        /*height: 'Ext.getBody().getSize().height+\'px\'',
         width: 'Ext.getBody().getSize().width+\'px\'',
         layout: {
         type: 'fit'
         },*/
        items: [
            /*{
             docked: 'top',
             xtype: 'toolbar',
             title: 'Introduction',
             cls: 'my-titlebar',
             defaults: {
             iconMask: true
             },
             items: [
             {
             xtype: 'spacer'
             },
             {
             action: 'btnSkipIntroductionScreen',
             text: 'SKIP'
             },
             ]
             },*/
            {
                xtype: 'panel',
                layout: {
                    type: 'vbox',
                    pack: 'center'
                },
                items: [
                    {
                        xtype: 'label',
                        html: 'Introduction to MELS App',
                        style: 'font-size:16px;color:#512b80;margin:10px;text-align: center;',
                        flex: 0.5
                    },
                    {
                        xtype: 'panel',
                        layout: {
                            pack: 'center'
                        },
//                        margin: '10 11 12 13',
                        html: [
                            '<div class="videoWrapper" align="center">',
                            '<iframe src="' + Mentor.Global.VIDEO_URL + '" id="introVideoPlayerId" width="' + ($('html').width() - 50) + '" height="' + ($('html').height() - 130) + '"  frameborder="0" allowfullscreen></iframe>',
                            '</div>'
                        ].join(''),
                        flex: 6
                    },
//                    {
//                        xtype: 'video',
//                        id: "introVideo",
//                        x: 600,
//                        y: 300,
//                        width: '90%',
//                        height: '78%',
//                        margin: '0 auto',
//                        url: 'resources/images/introVideo.mp4',
//                        //url: 'https://www.youtube.com/embed/jebJ9itYTJE',
//                        hidden: false,
//                        listeners: {
//                            play: function () {
//                                $('div.x-video-ghost').css('display', 'none !important');
//                                $('div.x-carousel-indicator').css('display', 'none');
//                                $('video').css('display', 'block !important');
//                            },
//                            ended: function () {
//                                Ext.getCmp('SkipIntroductionScreen').fireAction('tap', '', '');
//                            }
//                        }
//                    },
                    {
                        flex: 1,
                        xtype: 'panel',
                        layout: {
                            pack: 'center'
                        },
                        items: [
                            {
                                xtype: 'button',
                                text: 'SKIP',
                                id: 'SkipIntroductionScreen',
                                cls: 'signin-btn-cls',
                                pressedCls:'press-btn-cls',
                                action: "btnSkipIntroductionScreen",
                                margin: '5 100 0 100',
                                listeners: {
                                    tap: function () {
                                        $("#introVideoPlayerId").attr("src", '');
                                        //$("#introVideoPlayerId").attr("src", Mentor.Global.VIDEO_URL);
                                    }
                                }
                            }
                        ]
                    }
                ]
            }
        ],
        listeners: [
            {
                event: "activate",
                fn: "onPageActivate"
            },
            {
                event: "painted",
                fn: "onPagePainted"
            }
        ]
    },
    onPageActivate: function () {
        //Ext.getCmp('introVideo').play();
        //$('div.x-video-ghost').css('display', 'none !important');
        $('div.x-carousel-indicator').css('display', 'none');
        //$('video').css('display', 'block !important');
    },
    onPagePainted: function () {
        //Ext.getCmp('introVideo').play();
    }
});
