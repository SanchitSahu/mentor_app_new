Ext.define('Mentor.view.SignOut', {
    extend: 'Ext.Panel',
    xtype: 'signout',
    requires: [],
    config: {
        cls: 'signout',
        defaults: {
            xtype: 'container'
        },
        items: [
            {
                docked: 'top',
                xtype: 'toolbar',
                title: 'Sign Out',
                cls: 'my-titlebar',
                defaults: {
                    iconMask: true
                },
                items: [
                    {
                        iconCls: 'arrow_left',
                        action: 'btnbackSignOut'
                    }
                ]
            },
            {
                xtype: 'panel',
                cls: 'login-top',
                paddingTop: 10,
                layout: {
                    type: 'hbox',
                    pack: 'center'
                },
                items: [
                    {
                        xtype: 'container',
                        layout: {
                            type: 'vbox',
                            pack: 'center'
                        },
                        items: [
                            {
                                xtype: 'label',
                                html: 'Finished Logging Activity.',
                                style: 'font-size:20px;color:#66cc66;margin:10px;'
                            }
                        ]
                    }
                ]
            },
            {
                xtype: 'panel',
                flex: 1,
                style: 'margin-top:50px;',
                items: [
                    {
                        xtype: 'button',
                        text: 'SIGN OUT',
                        id: 'btn_singout',
                        cls: 'signout-btn-cls',
                        action: 'btnSignOut'
                    }
                ]
            }
        ]
    }
});
