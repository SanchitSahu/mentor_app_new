Ext.define('Mentor.view.TeamMenuPopupView', {
    extend: 'Ext.Panel',
    xtype: 'teammenupopupview',
    requires: [],
    config: {
        cls: 'teamMenuPopupScreen',
        defaults: {
        },
        items: [
            {
                xtype: 'container',
                layout: {
                    type: 'vbox',
                    pack: 'center'
                },
                items: [
                    {
                        xtype: 'button',
                        text: 'Join Team',
                        id: 'teamMenuJoin',
                        //cls : 'tick',
                        //cls: 'green-btn-cls',
                         cls: 'signin-btn-cls',
                        pressedCls : 'press-btn-cls',
                        //action: "doSubmitAddEditComment",
                        margin: '10 5 0 5',
                        listeners: {
                            tap: function () {
                                var itemId = Ext.getCmp('teamMenuDelete').getItemId();
                                Ext.getCmp('TeamMenuPopupView').hide();
                                var ownerTeamInfo = Ext.decode(localStorage.getItem('TeamAllDetails'));
                                if (ownerTeamInfo.OwnTeamDetail != false) {
                                    Ext.Msg.confirm("MELS", "Are you sure you want to Join another team? The Team you created will be dissolved.", function (btn) {
                                        if (btn == 'yes') {
                                            Mentor.app.application.getController('AppDetail').deleteTeam(itemId, true);
                                        }
                                    });
                                } else {
                                    Mentor.app.application.getController('AppDetail').teamListing();
                                }
                            }
                        }
                    },
                    {
                        xtype: 'button',
                        text: 'Switch Team',
                        id: 'teamMenuChange',
                        //cls: 'green-btn-cls',
                        cls: 'signin-btn-cls',
                        pressedCls : 'press-btn-cls',
                        margin: '10 5 0 5',
                        listeners: {
                            tap: function () {
                                var itemId = Ext.getCmp('teamMenuChange').getItemId();
                                Ext.getCmp('TeamMenuPopupView').hide();
                                var ownerTeamInfo = Ext.decode(localStorage.getItem('TeamAllDetails'));
                                Ext.Msg.confirm("MELS", "Are you sure you want to Change Your team?", function (btn) {
                                    if (btn == 'yes') {
                                        Mentor.app.application.getController('AppDetail').leaveTeam(itemId, true, 'join');
                                    }
                                });
                            }
                        }
                    },
                    {
                        xtype: 'button',
                        text: 'Leave Team',
                        id: 'teamMenuLeave',
                        //cls: 'orange-btn-cls',
                        pressedCls : 'press-grey_btn-cls',
                        cls: 'grey-btn-cls',
                        margin: '10 5 0 5',
                        listeners: {
                            tap: function () {
                                var itemId = Ext.getCmp('teamMenuLeave').getItemId();
                                Ext.getCmp('TeamMenuPopupView').hide();
                                var teamInfo = Ext.decode(localStorage.getItem('TeamAllDetails'));
                                Ext.Msg.confirm("MELS", "Are you sure you want to Leave " + teamInfo.MemberTeamDetail[0].TeamName + " Team?", function (btn) {
                                    if (btn == 'yes') {
                                        Mentor.app.application.getController('AppDetail').leaveTeam(itemId);
                                    }
                                });
                            }
                        }
                    },
                    {
                        xtype: 'button',
                        text: 'Create Your Team',
                        id: 'teamMenuAdd',
                        //cls: 'blue-btn-cls',
                        pressedCls : 'press-grey_btn-cls',
                        cls: 'grey-btn-cls',
                        margin: '10 5 0 5',
                        listeners: {
                            tap: function () {
                                var itemId = Ext.getCmp('teamMenuLeave').getItemId();
                                Ext.getCmp('TeamMenuPopupView').hide();
                                var teamInfo = Ext.decode(localStorage.getItem('TeamAllDetails'));
                                if (teamInfo.MemberTeamDetail != false) {
                                    Ext.Msg.confirm("MELS", "Are you sure you want to Add a new Team? This will make you leave " + teamInfo.MemberTeamDetail[0].TeamName + " team.", function (btn) {
                                        if (btn == 'yes') {
                                            Mentor.app.application.getController('AppDetail').leaveTeam(itemId, true, 'add');
                                        }
                                    });
                                } else {
                                    Mentor.app.application.getController('AppDetail').addEditTeam();
                                }
                            }
                        }
                    },
                    {
                        xtype: 'button',
                        text: 'Edit Your Team',
                        id: 'teamMenuEdit',
                        //cls: 'blue-btn-cls',
                        pressedCls : 'press-grey_btn-cls',
                        cls: 'grey-btn-cls',
                        margin: '10 5 0 5',
                        listeners: {
                            tap: function () {
                                var itemId = Ext.getCmp('teamMenuEdit').getItemId();
                                Ext.getCmp('TeamMenuPopupView').hide();
                                Mentor.app.application.getController('AppDetail').addEditTeam(itemId);
                            }
                        }
                    },
                    {
                        xtype: 'button',
                        text: 'View Team Details',
                        id: 'teamMenuView',
                        //cls: 'blue-btn-cls',
                        pressedCls : 'press-grey_btn-cls',
                        cls: 'grey-btn-cls',
                        margin: '10 5 0 5',
                        listeners: {
                            tap: function () {
                                var itemId = Ext.getCmp('teamMenuView').getItemId();
                                Ext.getCmp('TeamMenuPopupView').hide();
                                Mentor.app.application.getController('AppDetail').viewTeamDetail(itemId);
                            }
                        }
                    },
                    {
                        xtype: 'button',
                        text: 'Dissolve Your Team',
                        id: 'teamMenuDelete',
                        //cls: 'orange-btn-cls',
                        pressedCls : 'press-grey_btn-cls',
                        cls: 'grey-btn-cls',
                        margin: '10 5 0 5',
                        listeners: {
                            tap: function () {
                                var itemId = Ext.getCmp('teamMenuDelete').getItemId();
                                Ext.getCmp('TeamMenuPopupView').hide();
                                Ext.Msg.confirm("MELS", "Are you sure you want to Dissolve your team? All team members will be forcefully removed", function (btn) {
                                    if (btn == 'yes') {
                                        Mentor.app.application.getController('AppDetail').deleteTeam(itemId);
                                    }
                                });
                            }
                        }
                    },
                    {
                        xtype: 'button',
                        text: 'Cancel',
                        id: 'teamMenuCancel',
                        cls: 'red-btn-cls',
                        pressedCls : 'press-red_btn-cls',
                               
                        margin: '10 5 0 5',
                        handler : function (){
                             Ext.getCmp('TeamMenuPopupView').hide();
                        }
                       /* listeners: {
                            tap: function () {
                               Ext.getCmp('TeamMenuPopupView').hide();
                            }
                        }*/
                    }
                ]
            }
        ]
    },
    listeners: [
        {
            event: "activate",
            fn: "onPageActivate"
        },
        {
            event: "painted",
            fn: "onPagePainted"
        }
    ],
    onPagePainted: function () {}
});