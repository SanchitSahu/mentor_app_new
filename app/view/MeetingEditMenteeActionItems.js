Ext.define('Mentor.view.MeetingEditMenteeActionItems', {
    extend: 'Ext.Panel',
    xtype: 'meetingedit_mentee_action_items',
    requires: [],
    config: {
        cls: 'enterpreneur_action_items',
        scrollable: {
            direction: 'vertical',
            indicators: {
                y: {
                    autoHide: false
                }
            }
        },
        defaults: {
            xtype: 'container'
        },
        items: [
            {
                docked: 'top',
                xtype: 'toolbar',
                title: 'Mentee ToDo Items',
                id: 'enterpreneur_action_items_title',
                cls: 'my-titlebar',
                defaults: {
                    iconMask: true
                },
                items: [
                    {
                        iconCls: 'btnBackCLS',
                        action: 'btnBackMeetingEditMenteeActionItems',
                        docked: 'left'
                    },
//                    {
//                        //style: "visibility:hidden;"
//                        iconCls: 'addIcon',
//                        listeners: {
//                            element: 'element',
//                            tap: function () {
//                                var popup = Mentor.app.application.getController('AppDetail').addNotePopup();
//                                Ext.Viewport.add(popup);
//                                popup.show();
//                            }
//                        }
//                    },
                    {
                        iconCls: 'goToNotesIcon',
                        //docked: 'left',
                        //style: 'visibility:hidden'
                        id: 'addnotes',
                        action: 'addnotes'
                    },
                    {
                        iconCls: 'helpIcon',
                        docked: 'right',
                        listeners: {
                            element: 'element',
                            tap: function () {
                                var popup = Mentor.app.application.getController('AppDetail').addHelpPopup('mentee_action');
                                Ext.Viewport.add(popup);
                                popup.show();
                            }
                        }
                    }
//                    {
//                        iconCls: 'refreshIcon',
//                        docked: 'right',
//                        listeners: [
//                            {
//                                event: "tap",
//                                fn: "onPagePainted"
//                            }
//                        ]
//                    }
                ]
            },
            {
                xtype: 'label',
                id: 'idUserNameEnterpreneur',
                style: 'text-align: right;padding-right: 10px;padding: 10px;background-color: #C2C2C2;',
                hidden: true
            },
            {
                xtype: 'label',
                id: 'idVersionEnterpreneur',
                style: 'text-align: right;padding-right: 10px;padding: 10px;',
                docked: 'bottom',
                hidden: true
            },
            {
                xtype: 'panel',
                margin: '20 0 0 0',
                layout: {
                    type: 'vbox'
                },
                items: [
                    {
                        xtype: 'label',
                        html: 'Enterpreneur ToDo Items',
                        style: 'margin-left:10px;margin-right:10px;border: 0px solid #eee;border-left-width: 0;border-bottom: 1px solid #66cc66;font-weight: bold;',
                        id: 'idLabelMenteeActionItemEnterpreneurActionItemScreen',
                        hidden: true
                    },
                    {
                        xtype: 'panel',
                        id: 'idEnterpreneurActionItemHeader',
                        //margin: '10 0 0 0',
                        layout: {
                            type: 'hbox'
                        },
                         style : 'border-bottom: 1px solid #bfbfbf;padding-bottom: 5px;',
                        items: [
                            {
                                xtype: 'label',
                                style: 'margin-left: 30px; font-size: smaller;'
                            },
                            {
                                xtype: 'label',
                                html: 'ToDo',
                                style: 'margin-left: 9px; font-size: smaller;'
                            },
                            {
                                xtype: 'label',
                                html: 'Done',
                                style: 'margin-left: 17px; font-size: smaller;'
                            }
                        ]
                    },
                    {
                        xtype: 'panel',
                        margin: '10 0 0 0',
                        layout: {
                            type: 'hbox'
                        },
                        items: [
                            {
                                xtype: 'panel',
                                margin: '0 0 0 10',
                                id: 'idMenteeActionItemComments'
                            },
                            {
                                xtype: 'panel',
                                id: 'idEnterpreneurActionItem',
                                margin: '0 0 0 10'
                            },
                            {
                                xtype: 'panel',
                                margin: '0 0 0 10',
                                id: 'idMenteeActionItemDoneOnMeetingUpdate'
                            }
                        ]
                    },
                    {
                        xtype: 'container',
                        layout: {
                            type: 'hbox'
                        },
                         margin: '0 10 0 10 ',
                        items: [
                            {
                                xtype: 'button',
                                text: 'OK',
                                pressedCls : 'press-btn-cls',
                                cls: 'signin-btn-cls',
                                action: "btnNextMeetingEditMenteeActionItems",
                                flex: 1
                            },
                            {
                                xtype: 'button',
                                text: 'BACK',
                                //cls: 'decline-btn-cls',
                                pressedCls : 'press-grey_btn-cls',
                                cls: 'grey-btn-cls',
                                flex: 1,
                                hidden: false,
                                action: 'btnBackMeetingEditMenteeActionItems'
                            }
                        ]
                    }
                ]
            }
        ],
        listeners: [
            {
                event: "activate",
                fn: "onPageActivate"
            },
            {
                event: "deactivate",
                fn: "onPageDeactivate"
            },
            {
                event: "painted",
                fn: "onPagePainted"
            }
        ]
    },
    onPageDeactivate: function () {
        var MenteeActionItemID;
        var MenteeActionItemName;
        var MenteeActionItemIDDone;
        var MenteeActionItemNameDone;
        var isChecked = 0;
        var isCheckedDone = 0;
        var data = null;
        var MenteeActionItems = Ext.getCmp('idEnterpreneurActionItem');

        for (i = 0; i < MenteeActionItems.getItems().length; i++) {
            if (MenteeActionItems.getItems().getAt(i).isChecked()) {
                if (isChecked == 0) {
                    MenteeActionItemID = MenteeActionItems.getItems().getAt(i).getValue();
                    MenteeActionItemName = MenteeActionItems.getItems().getAt(i).getItemId();
                    isChecked = isChecked + 1;
                } else {
                    MenteeActionItemID = MenteeActionItemID + "," + MenteeActionItems.getItems().getAt(i).getValue();
                    MenteeActionItemName = MenteeActionItemName + "," + MenteeActionItems.getItems().getAt(i).getItemId();
                    isChecked = isChecked + 1;
                }
            }
        }

        var MenteeActionItemsDone = Ext.getCmp('idMenteeActionItemDoneOnMeetingUpdate');
        for (i = 0; i < MenteeActionItemsDone.getItems().length; i++) {
            if (MenteeActionItemsDone.getItems().getAt(i).isChecked()) {
                if (isCheckedDone == 0) {
                    MenteeActionItemIDDone = MenteeActionItemsDone.getItems().getAt(i).getValue();
                    MenteeActionItemNameDone = MenteeActionItemsDone.getItems().getAt(i).getItemId();
                    isCheckedDone = isCheckedDone + 1;
                } else {
                    MenteeActionItemIDDone = MenteeActionItemIDDone + "," + MenteeActionItemsDone.getItems().getAt(i).getValue();
                    MenteeActionItemNameDone = MenteeActionItemNameDone + "," + MenteeActionItemsDone.getItems().getAt(i).getItemId();
                    isCheckedDone = isCheckedDone + 1;
                }
            }
        }

        if (isChecked != 0) {
            data = {
                MenteeActionItemID: MenteeActionItemID,
                MenteeActionItemName: MenteeActionItemName,
                MenteeActionItemIDDone: MenteeActionItemIDDone,
                MenteeActionItemNameDone: MenteeActionItemNameDone
            };
            data = Ext.encode(data);
        }
        localStorage.setItem('CacheEditMenteeActionItems', data);
    },
    onPageActivate: function () {
        var CacheEditMenteeActionItems = Ext.decode(localStorage.getItem('CacheEditMenteeActionItems'));
        localStorage.setItem('CacheEditMenteeActionItems', null);

        var MenteeActionCommentsStore = Ext.getStore('MenteeActionComments').load();

        var EnterpreneurActionItemStore = Ext.getStore('EnterpreneurAction').load();
        var MeetingDetailPanel = Ext.getCmp('idEnterpreneurActionItem');
        MeetingDetailPanel.removeAll();

        var MenteeActionPanelDoneOnNewMeeting = Ext.getCmp('idMenteeActionItemDoneOnMeetingUpdate');
        MenteeActionPanelDoneOnNewMeeting.removeAll();

        var MenteeActionItemComments = Ext.getCmp('idMenteeActionItemComments');
        MenteeActionItemComments.removeAll();

        var MentorLoginDetail = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
        var MenteeLoginDetail = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));

        Ext.getCmp('idVersionEnterpreneur').setHtml(Mentor.Global.VERSION_NAME);

        if (MentorLoginDetail != null) {
            Ext.getCmp('idUserNameEnterpreneur').setHtml(MentorLoginDetail.MentorName);
        } else if (MenteeLoginDetail != null) {
            Ext.getCmp('idUserNameEnterpreneur').setHtml(MenteeLoginDetail.MenteeName);
            Ext.getCmp('idEnterpreneurActionItemHeader').setHidden(false); // Display headder
        }

        for (i = 0; i < EnterpreneurActionItemStore.data.length; i++) {
            var checked = false;

            if (CacheEditMenteeActionItems == null) {
                var SelectedActionItems = Ext.decode(localStorage.getItem("storeEntrepreneurActionItems"));
            } else {
                SelectedActionItems = CacheEditMenteeActionItems;
            }

            if (SelectedActionItems != "" && SelectedActionItems.MenteeActionItemName != null && SelectedActionItems.MenteeActionItemName != '') {
                SelectedActionItems = SelectedActionItems.MenteeActionItemName.split(",");
                if (SelectedActionItems.indexOf(EnterpreneurActionItemStore.data.getAt(i).data.MenteeActionName) != -1) {
                    checked = true;
                }
            }

            var checkBoxField = Ext.create('Ext.field.Checkbox', {
                name: 'recorded_stream',
                value: EnterpreneurActionItemStore.data.getAt(i).data.MenteeActionID,
                itemId: EnterpreneurActionItemStore.data.getAt(i).data.MenteeActionName,
                labelCls: 'radio-btn-cls',
                checked: checked,
                labelWidth: '90%',
                labelAlign: 'right',
                listeners: {
                    'check': function (radio, e, eOpts) {
                        if (radio.getItemId().toLowerCase() == "no action needed") {
                            var EntrepreneurItemsPanel = Ext.getCmp('idEnterpreneurActionItem');
                            var EntrepreneurItemsDonePanel = Ext.getCmp('idMenteeActionItemDoneOnMeetingUpdate');
                            for (var i = 0; i < EntrepreneurItemsPanel.getItems().length; i++) {
                                if (EntrepreneurItemsPanel.getItems().getAt(i).getItemId().toLowerCase() != "no action needed") {
                                    EntrepreneurItemsPanel.getItems().getAt(i).setChecked(false);
                                } else {
                                    EntrepreneurItemsDonePanel.getItems().getAt(i).setDisabled(false);
                                }
                            }
                        } else {
                            var EntrepreneurItemsPanel = Ext.getCmp('idEnterpreneurActionItem');
                            var EntrepreneurItemsDonePanel = Ext.getCmp('idMenteeActionItemDoneOnMeetingUpdate');
                            for (var i = 0; i < EntrepreneurItemsPanel.getItems().length; i++) {
                                if (EntrepreneurItemsPanel.getItems().getAt(i).isChecked()) {
                                    if (EntrepreneurItemsPanel.getItems().getAt(i).getItemId().toLowerCase() == "no action needed") {
                                        EntrepreneurItemsPanel.getItems().getAt(i).setChecked(false);
                                    } else {
                                        EntrepreneurItemsDonePanel.getItems().getAt(i).setDisabled(false);
                                    }
                                } else if (EntrepreneurItemsPanel.getItems().getAt(i).isChecked() == false) {
                                    EntrepreneurItemsDonePanel.getItems().getAt(i).setDisabled(true);
                                }
                            }
                        }
                    },
                    'uncheck': function (radio, e, eOpts) {
                        var EntrepreneurItemsPanel = Ext.getCmp('idEnterpreneurActionItem');
                        var EntrepreneurItemsDonePanel = Ext.getCmp('idMenteeActionItemDoneOnMeetingUpdate');
                        for (var i = 0; i < EntrepreneurItemsPanel.getItems().length; i++) {
                            if (!EntrepreneurItemsPanel.getItems().getAt(i).getChecked()) {
                                EntrepreneurItemsDonePanel.getItems().getAt(i).setChecked(false);
                                EntrepreneurItemsDonePanel.getItems().getAt(i).setDisabled(true);
                            }
                        }
                    }
                }
            });
            MeetingDetailPanel.add(checkBoxField);

            if (CacheEditMenteeActionItems == null) {
                var SelectedActionItemsDone = Ext.decode(localStorage.getItem("storeEntrepreneurActionItems"));
            } else {
                var SelectedActionItemsDone = CacheEditMenteeActionItems;
            }
            var doneChecked = false;

            if (SelectedActionItemsDone != null && SelectedActionItemsDone != "" && SelectedActionItemsDone.MenteeActionItemNameDone != null) {
                SelectedActionItemsDone = SelectedActionItemsDone.MenteeActionItemNameDone.split(",");
                if (SelectedActionItemsDone.indexOf(EnterpreneurActionItemStore.data.getAt(i).data.MenteeActionName) != -1) {
                    doneChecked = true;
                }
            }

            var Disable = (checked == true) ? false : true;

            var checkBoxFieldActionDone = Ext.create('Ext.field.Checkbox', {
                //labelWidth: 200,
                name: 'recorded_stream',
                value: EnterpreneurActionItemStore.data.getAt(i).data.MenteeActionID,
                label: EnterpreneurActionItemStore.data.getAt(i).data.MenteeActionName, // + " " + EnterpreneurActionItemStore.data.getAt(i).data.MenteeActionName,
                itemId: EnterpreneurActionItemStore.data.getAt(i).data.MenteeActionName,
                labelCls: 'radio-btn-cls',
                checked: doneChecked,
                disabled: Disable,
                labelWidth: '90%',
                labelAlign: 'right'
            });
            MenteeActionPanelDoneOnNewMeeting.add(checkBoxFieldActionDone);


            var cls = 'addnote';
            var icon = 'add_icon_filled.png';
            var isContinue = false;
            for (var c = 0; c < MenteeActionCommentsStore.data.length; c++) {
                if (isContinue)
                    continue;
                if (MenteeActionCommentsStore.data.getAt(c).data.MeetingID == Mentor.Global.MEETING_DETAIL.MeetingID) {
                    if (MenteeActionCommentsStore.data.getAt(c).data.MenteeActionItemID == EnterpreneurActionItemStore.data.getAt(i).data.MenteeActionID) {
                        cls = 'editnote_' + MenteeActionCommentsStore.data.getAt(c).data.MenteeActionItemCommentID;
                        icon = 'edit_note.png';
                    }
                }
            }

            var buttonFieldActionComment = Ext.create('Ext.Img', {
                src: 'resources/images/' + icon,
                height: 25,
                width: 25,
                itemId: EnterpreneurActionItemStore.data.getAt(i).data.MenteeActionID,
                cls: cls,
                listeners: {
                    'tap': function (radio, e, eOpts) {
                        var popup = Mentor.app.application.getController('AppDetail').addCommentPopup(1, radio.getItemId(), radio.getCls()[0], Mentor.Global.MEETING_DETAIL.MeetingID);
                        Ext.Viewport.add(popup);
                        popup.show();
                    }
                }
            });
            MenteeActionItemComments.add(buttonFieldActionComment);
        }
        //Change the lable of Mentor and Mentor where Name Exits according to Application Configuration
        Ext.getCmp('idLabelMenteeActionItemEnterpreneurActionItemScreen').setHtml(Mentor.Global.MENTEE_NAME + ' ToDo Items');
    },
    onPagePainted: function () {
        Ext.getCmp('enterpreneur_action_items_title').setTitle(Mentor.Global.MENTEE_NAME + ' ToDo Items');
    }
});
