Ext.define('Mentor.view.HelpTextPopup', {
    extend: 'Ext.Panel',
    xtype: 'helptextpopup',
    requires: [],
    config: {
        cls: 'signUp',
        defaults: {
            xtype: 'formpanel'
        },
        items: [
            {
                xtype: 'label',
                id: 'helptextID',
                style: 'text-align: justify;font-size:15px;',
                html: '',
                scrollable: {
                    direction: 'vertical',
                    directionLock: true
                }
            }
        ]
    }
});