Ext.define('Mentor.view.TeamDetail', {
    extend: 'Ext.Panel',
    xtype: 'teamdetail',
    requires: [],
    config: {
        cls: 'mentorprofile',
        //scrollable: "vertical",
        scrollable: {
            direction: 'vertical',
            indicators: {
                y: {
                    autoHide: false
                }
            }
        },
        defaults: {
            xtype: 'container'
        },
        items: [
            {
                docked: 'top',
                xtype: 'toolbar',
                title: 'Join a Team',
                cls: 'my-titlebar',
                id : 'idToolbarTeamDetail',
                defaults: {
                    iconMask: true
                },
                items: [
                    {
                        //style : 'visibility: hidden;',
                        iconCls: 'btnBackCLS',
                        docked: 'left',
                        action: 'btnBackActionTeamDetail'
                    },
                    {
                        iconCls: 'goToNotesIcon',
                        docked: 'left',
                        id: 'addnotes',
                        action: 'addnotes'
                    },
                    {
                        iconCls: 'helpIcon',
                        docked: 'right',
                        listeners: {
                            element: 'element',
                            tap: function () {
                                var popup = Mentor.app.application.getController('AppDetail').addHelpPopup('team_detail');
                                Ext.Viewport.add(popup);
                                popup.show();
                            }
                        }
                    },
                    {
                        iconCls: 'helpIcon',
                        style: 'visibility:hidden',
                        docked: 'right'
                    }
                ]
            },
            {
                xtype: 'panel',
                cls: 'mettingHistoryToolBar',
                layout: {
                    type: 'hbox',
                   // pack: 'center',
                   // align: 'middle'
                },
                items: [
                    {
                        xtype: "button",
                        id: 'id_TeamProfilePictureDisplay',
                        cls: "teamDetail_photo-preview-cls",
                        margin: '10 10 10 20',
                    },

                    {
                        xtype: 'panel',
                        defaults: {
                            iconMask: true
                        },
                        layout: {
                            type: 'hbox',
                            pack: 'justify',
                            align: 'middle'

                        },
                        flex : 1,
                        items: [
                            {
                                xtype: 'label',
                                id: 'idTeamNameDetailScreen',
                                style: 'color: white;'
                            },
                            {
                                xtype: 'button',
                                text: 'Join',
                                id: 'teamDetailJoin',
                                //cls: 'green-btn-cls small',
                                action: 'teamDetailJoinTeam',
                                //docked : 'right',
                                align: 'right',
                                margin:'0 20',
                                cls: 'white-btn-cls',
                                pressedCls : 'press-white_btn-cls',
                            }
                        ]
                    }
                ]
            },
            {
                xtype: 'label',
                html: 'Team Description',
                style: 'font-weight: normal;color:#161718',
                cls : 'appLabelCls',
                id: 'idLabelTeamDescDetailScreen',
                margin : '20 20 0 20',
            },
            {
                xtype: 'textfield',
                id: 'idTeamDescDetailScreen',
                disabled: true,
                cls: 'textFieldDisableCLS',
                clearIcon: false,
                inputCls: 'inputDisabledCLS',
                margin : '0 20 0 20',

               
            },
            
            /*{
                xtype: 'label',
                html: 'Team Description',
                cls: 'userProfileLabelCLS',
                margin: '10 0 0 0',
                id: 'idLabelTeamDescDetailScreen'
            },
            {
                xtype: 'label',
                cls: 'userProfileHtmlCLS',
                margin: '10 0 10 10',
                id: 'idTeamDescDetailScreen'
            },*/

            {
                xtype: 'label',
                html: 'Open Slots',
                style: 'font-weight: normal;color:#161718',
                cls : 'appLabelCls',
                id: 'idLabelTeamOpenSlotsDetailScreen',
                margin : '20 20 0 20',
            },
            {
                xtype: 'textfield',
                id: 'idTeamOpenSlotsDetailScreen',
                disabled: true,
                cls: 'textFieldDisableCLS',
                clearIcon: false,
                inputCls: 'inputDisabledCLS',
                margin : '0 20 0 20',

               
            },
            /*{
                xtype: 'panel',
                layout: {
                    type: 'hbox',
                    pack: 'left',
                    align: 'middle'
                },
                margin: '10 0 10 0',
                items: [
                    {
                        xtype: 'label',
                        html: 'Open Slots: ',
                        cls: 'userProfileLabelCLS',
                        margin: '5 0 5 0',
                        id: 'idLabelTeamOpenSlotsDetailScreen'
                    },
                    {
                        xtype: 'label',
                        cls: 'userProfileHtmlCLS',
                        margin: '0 0 5 10',
                        id: 'idTeamOpenSlotsDetailScreen'
                    }
                ]
            },
            {
                xtype: 'image',
                style: 'border-bottom: 1px #cacaca solid;'
            },*/
            {
                xtype: 'button',
                text: 'VIEW TEAM MEMBERS',
                id: 'idButtonViewTeamMember',
                cls: 'signin-btn-cls',
                pressedCls : 'press-btn-cls',
                action: "btnViewTeamMembers",
                flex: 1,
                data:"sanchit",
                margin : '40 20 0 20',
            },
            /*{
             xtype: 'label',
             html: 'Team Owner',
             cls: 'userProfileLabelCLS',
             margin: '10 0 0 0',
             id: 'idLabelTeamOwnerDetailScreen'
             },
             {
             xtype: 'panel',
             layout: {
             type: 'hbox',
             pack: 'left',
             align: 'middle'
             },
             items: [
             {
             xtype: "button",
             id: 'id_TeamOwnerPictureDisplay',
             cls: "photo-preview-cls",
             margin: 10
             },
             {
             xtype: 'label',
             id: 'idTeamOwnerNameDetailScreen',
             style: 'margin-bottom: 10px;margin-right: 150px;'
             }
             ]
             },
             {
             xtype: 'image',
             style: 'border-bottom: 1px #cacaca solid;'
             },*/
            
            /*{
                xtype: 'image',
                style: 'border-bottom: 1px #cacaca solid;'
            },*/
            /*{
                xtype: 'label',
                html: 'Required Skills',
                cls: 'userProfileLabelCLS',
                margin: '10 0 0 0',
                id: 'idLabelTeamReqSkillsDetailScreen'
            },*/
            {
                xtype: 'panel',
                style: 'background : #ebebeb;padding: 10px 30px;',
                margin: '20 0 0 0',
                items: [
                    {
                        xtype: 'label',
                        html: 'Required Skills',
                        cls: 'profileAddInterestCLS',
                        id: 'idLabelTeamReqSkillsDetailScreen'
                    },
                    {
                        xtype: 'label',
                        html: 'Tap on any interests to add them to your profile.',
                        cls: 'profileAddInterestDetailCLS',
                        id : 'idSkillsLabelTeamReqSkillsDetailScreen'
                    }
                ]
            },
            {
                xtype: 'panel',
                id: 'idTeamReqSkillsDetailScreen',
                margin: '10 30 5 30',
                defaults: {
                    style: {
                        float: 'left',
                        pack: 'center'
                    }
                }
            }
        ]
    }
});       