Ext.define('Mentor.view.WaitScreen', {
    extend: 'Ext.Panel',
    xtype: 'waitscreen',
    requires: [],
    config: {
        cls: 'waitscreen',
        //scrollable: "vertical",
        scrollable: {
            direction: 'vertical',
            indicators: {
                y: {
                    autoHide: false
                }
            }
        },
        defaults: {
            xtype: 'container'
        },
        items: [
            {
                docked: 'top',
                xtype: 'toolbar',
                title: 'Invite Status Detail',
                id: 'idTitleWaitscreen',
                cls: 'my-titlebar',
                defaults: {
                    iconMask: true
                },
                items: [
                    {
                        iconCls: 'btnBackCLS',
                        action: 'btnBackWaitScreen',
                        docked: 'left'
                    },
                    {
                        iconCls: 'goToNotesIcon',
                        docked: 'left',
                        //style: 'visibility:hidden'
                        id: 'addnotes',
                        action: 'addnotes'
                    },
//                    {
//                        iconCls: 'addIcon',
//                        docked: 'left',
//                        listeners: {
//                            element: 'element',
//                            tap: function () {
//                                var popup = Mentor.app.application.getController('AppDetail').addNotePopup();
//                                Ext.Viewport.add(popup);
//                                popup.show();
//                            }
//                        }
//                    },
//                    {
//                        iconCls: 'btnBackCLS',
//                        docked: 'left',
//                        style: 'visibility:hidden'
//                    },
                    {
                        iconCls: 'helpIcon',
                        docked: 'right',
                        listeners: {
                            element: 'element',
                            tap: function () {
                                if (Ext.getCmp('idTitleWaitscreen').getTitle().getHtml().toLowerCase() == "accepted invite") { //For Accepted Invite Detail Screen
                                    var popup = Mentor.app.application.getController('AppDetail').addHelpPopup('accepted_invite');

                                } else if (Ext.getCmp('idTitleWaitscreen').getTitle().getHtml().toLowerCase() == "pending invite") { //For Pending Invite Detail Screen
                                    var popup = Mentor.app.application.getController('AppDetail').addHelpPopup('pending_invite');

                                } else if (Ext.getCmp('idTitleWaitscreen').getTitle().getHtml().toLowerCase() == "declined invite") { //For Declined Invite Detail Screen
                                    var popup = Mentor.app.application.getController('AppDetail').addHelpPopup('declined_invite');

                                } else if (Ext.getCmp('idTitleWaitscreen').getTitle().getHtml().toLowerCase() == "cancelled detail") { //For Cancelled Invite Detail Screen, cancelled by the reciever
                                    var popup = Mentor.app.application.getController('AppDetail').addHelpPopup('cancelled_invite_detail');

                                } else if (Ext.getCmp('idTitleWaitscreen').getTitle().getHtml().toLowerCase() == "cancelled invite") { //For Cancelled Invite Detail Screen, cancelled by the issuer
                                    var popup = Mentor.app.application.getController('AppDetail').addHelpPopup('cancelled_invite');

                                } else if (Ext.getCmp('idTitleWaitscreen').getTitle().getHtml().toLowerCase() == "pending meeting") { //For Pending Meeting Detail Screen, Mentor Issues pending meeting for Mentee
                                    var popup = Mentor.app.application.getController('AppDetail').addHelpPopup('pending_meeting_detail');

                                } else if (Ext.getCmp('idTitleWaitscreen').getTitle().getHtml().toLowerCase() == "requested reschedule") { //For Rechedule request made by Mentor and still pending to be approved by Mentee
                                    var popup = Mentor.app.application.getController('AppDetail').addHelpPopup('reschedule_requested_by_mentee');
                                
                                } else if (Ext.getCmp('idTitleWaitscreen').getTitle().getHtml().toLowerCase() == "reschedule requested") { //For Rechedule request made by Mentee and still pending to be approved by Mentor
                                    var popup = Mentor.app.application.getController('AppDetail').addHelpPopup('reschedule_requested_by_mentor');

                                } else {
                                    var popup = Mentor.app.application.getController('AppDetail').addHelpPopup('no_help');

                                }

                                //var popup = Mentor.app.application.getController('AppDetail').addHelpPopup('invite_detail');
                                Ext.Viewport.add(popup);
                                popup.show();
                            }
                        }
                    },
                    {
                        action: 'btnRefreshWaitScreen',
                        /*xtype : 'button',
                         text : 'Refresh',
                         cls : 'signin-btn-cls',*/
                        docked: 'right',
                        iconCls: 'refreshIcon',
                        id: 'idBtnRefreshWaitScreen'

                    }
                ]
            },
            {
                xtype: 'label',
                id: 'idInfoMentorNameWaitScreen',
                style: 'text-align: center;padding: 15px;border-bottom:1px solid #dadada;color:#181717;font-size: 17px;',
                html : 'Meeting Request from Mentee1'
            },
            {
                xtype: 'textfield',
                id: 'idStoreName',
                hidden: true
            },
            {
                xtype: 'label',
                id: 'idUserNameWaitScreen',
                style: 'text-align: right;padding-right: 10px;padding: 10px;background-color: #C2C2C2;',
                hidden: true
            },
            {
                xtype: 'label',
                html: Mentor.Global.VERSION_NAME,
                style: 'text-align: right;padding-right: 10px;padding: 10px;',
                docked: 'bottom',
                hidden: true
            },
            {
                xtype: 'panel',
                layout: {
                    type: 'vbox'
                },
                margin: '0 10',
                items: [
                    {
                        xtype: 'fieldset',
                        title: 'Mentor Name',
                        id: 'idLabelMentorNameListWaitScreen',
                        hidden : true,
                        items: [
                            {
                                xtype: 'selectfield',
                                id: 'idMentorSelectField',
                                //store: 'WaitScreenMentor',
                                store: '',
                                displayField: 'MentorName',
                                valueField: 'InvitationId',
                                disabled: true,
                                inputCls: 'inputDisableCLS',
                                listeners: {
                                    change: function (field, value) {
                                        if (value instanceof Ext.data.Model) {
                                            value = value.get(field.getValueField());
                                        }
                                        console.log(value);
                                        //var Store = Ext.getStore('WaitScreenMentor');
                                        var Store = Ext.getStore(Ext.getCmp('idStoreName').getValue());
                                        var record = Store.findRecord('InvitationId', value);
                                        if (record != null) {
                                            Ext.getCmp('mentorCommentWaitScreen').setValue(record.data.MentorComments);
                                            Ext.getCmp('idBtnRefreshWaitScreen').setHidden(false);
                                            if (record.data.Status == "Accepted") {
                                                Ext.getCmp('radioBtnAcceptedWaitScreen').setChecked(true);
                                                Ext.getCmp('radioBtnDeclinedWaitScreen').setChecked(false);
                                                Ext.getCmp('waitScreenMeetingStatus').setValue('Invitation has accepted by ' + record.data.MentorName + '.');
                                            } else if (record.data.Status == "Rejected") {
                                                Ext.getCmp('radioBtnDeclinedWaitScreen').setChecked(true);
                                                Ext.getCmp('radioBtnAcceptedWaitScreen').setChecked(false);
                                                Ext.getCmp('waitScreenMeetingStatus').setValue('Invitation has declined by ' + record.data.MentorName + '.');
                                                Ext.getCmp('idBtnRefreshWaitScreen').setHidden(true);
                                                //Hide Meeting Place
                                                Ext.getCmp('idLabelMeetingPlaceWaitScreen').setHidden(true);
                                                Ext.getCmp('idMeetingPlaceWaitScreen').setHidden(true);
                                                //Hide Meeting Type
                                                Ext.getCmp('idLabelMeetingTypeWaitScreen').setHidden(true);
                                                Ext.getCmp('idMeetingTypeWaitScreen').setHidden(true);
                                                //Hide Meeting DateTime
                                                Ext.getCmp('idLabelMeetingDateTimeWaitScreen').setHidden(true);
                                                Ext.getCmp('idMeetingDateTimeWaitScreen').setHidden(true);
                                            } else if (record.data.Status == "Pending") {
                                                Ext.getCmp('radioBtnDeclinedWaitScreen').setChecked(false);
                                                Ext.getCmp('radioBtnAcceptedWaitScreen').setChecked(false);
                                                Ext.getCmp('waitScreenMeetingStatus').setValue(record.data.MentorName + " has not yet responded.");
                                            }
                                        }
                                        //Pass value parameter to the 2nd select field's store
                                    }
                                }
                            }
                        ]
                    },
                    {
                        xtype: 'label',
                        html: 'Main Topic',
                        style: 'font-weight: normal;color:#161718',
                        margin: '20 10 0 10',
                        cls : 'appLabelCls',
                    },
                    {
                        xtype: 'textfield',
                        id: 'waitScreenMeetingMainTopic',
                        disabled: true,
                        disabledCls: 'textFieldDisableCLS',
                        inputCls: 'inputDisableCLS',
                        margin: '0 10 0 10'
                    },
                    {
                        xtype: 'label',
                        html: 'Status',
                        style: 'font-weight: normal;color:#161718',
                        margin: '20 10 0 10',
                        cls : 'appLabelCls',
                    },
                    {
                        xtype: 'textfield',
                        name: 'waitScreenMeetingStatus',
                        id: 'waitScreenMeetingStatus',
                        disabled: true,
                        disabledCls: 'textFieldDisableCLS',
                        inputCls: 'inputDisableCLS',
                        margin: '0 10 0 10'
                    },
                    {
                        xtype: 'label',
                        html: 'Mentor Invitation Response :',
                        style: 'font-weight: normal;color:#161718',
                        margin: '20 10 0 10',
                        cls : 'appLabelCls',
                        hidden: true
                    },
                    {
                        xtype: 'panel',
                        hidden: true,
                        layout: {
                            type: 'hbox',
                            align: 'center'
                        },
                        items: [
                            {
                                xtype: 'radiofield',
                                name: 'color',
                                value: '0',
                                label: 'Accepted',
                                labelWidth: 100,
                                flex: 1,
                                checked: false,
                                labelCls: 'radio-btn-cls',
                                cls: 'statisfactionCLS',
                                id: 'radioBtnAcceptedWaitScreen',
                                disabled: true
                            },
                            {
                                xtype: 'radiofield',
                                name: 'color',
                                value: '1',
                                labelWidth: 100,
                                flex: 1,
                                label: 'Declined',
                                labelCls: 'radio-btn-cls',
                                cls: 'statisfactionCLS',
                                id: 'radioBtnDeclinedWaitScreen',
                                disabled: true
                            }
                        ]
                    },
                    {
                        xtype: 'label',
                        html: 'Meeting Type',
                        style: 'font-weight: normal;color:#161718',
                        margin: '20 10 0 10',
                        cls : 'appLabelCls',
                        id: 'idLabelMeetingTypeWaitScreen'
                    },
                    {
                        xtype: 'textfield',
                        id: 'idMeetingTypeWaitScreen',
                        disabled: true,
                        disabledCls: 'textFieldDisableCLS',
                        inputCls: 'inputDisableCLS',
                        margin: '0 10 0 10'
                    },
                    {
                        xtype: 'label',
                        html: 'Meeting Place',
                        style: 'font-weight: normal;color:#161718',
                        margin: '20 10 0 10',
                        cls : 'appLabelCls',
                        id: 'idLabelMeetingPlaceWaitScreen'
                    },
                    {
                        xtype: 'textfield',
                        id: 'idMeetingPlaceWaitScreen',
                        disabled: true,
                        disabledCls: 'textFieldDisableCLS',
                        inputCls: 'inputDisableCLS',
                        margin: '0 10 0 10'
                    },
                    {
                        xtype: 'label',
                        html: 'Meeting Date Time',
                        style: 'font-weight: normal;color:#161718',
                        margin: '20 10 0 10',
                        cls : 'appLabelCls',
                        id: 'idLabelMeetingDateTimeWaitScreen'
                    },
                    {
                        xtype: 'textfield',
                        id: 'idMeetingDateTimeWaitScreen',
                        disabled: true,
                        disabledCls: 'textFieldDisableCLS',
                        inputCls: 'inputDisableCLS',
                        margin: '0 10 0 10'
                    },
                    {
                        xtype: 'label',
                        //html : 'Mentee Email',
                        style: 'font-weight: normal;color:#161718',
                        margin: '20 10 0 10',
                        cls : 'appLabelCls',
                        id: 'idLabelMentorEmailWaitScreen'
                    },
                    {
                        xtype: 'textfield',
                        id: 'idMentorEmailWaitScreen',
                        disabled: true,
                        disabledCls: 'textFieldDisableCLS',
                        inputCls: 'inputDisableCLS',
                        margin: '0 10 0 10'
                    },
                    {
                        xtype: 'label',
                        //html : 'Mentee Phone No',
                        style: 'font-weight: normal;color:#161718',
                        margin: '20 10 0 10',
                        cls : 'appLabelCls',
                        id: 'idLabelMentorPhoneNoWaitScreen'
                    },
                    {
                        xtype: 'textfield',
                        id: 'idMentorPhoneWaitScreen',
                        disabled: true,
                        disabledCls: 'textFieldDisableCLS',
                        inputCls: 'inputDisableCLS',
                        margin: '0 10 0 10'
                    },
                    {
                        xtype: 'panel',
                        margin: '10 10 0 10',
                        layout: {
                            type: 'hbox'
                        },
                        items: [
                            {
                                xtype: 'checkboxfield',
                                id: 'idIsPhoneSMSCapableWaitScreen',
                                label: "Is above number Text Message (aka SMS) capable?",
                                labelCls: 'radio-btn-cls',
                                labelWidth: '90%',
                                labelAlign: 'right',
                                disabled: true,
                                cls: 'wordWrapSpan'
                            }
                        ]
                    },
                    {
                        xtype: 'label',
                        //html : Mentor.Global.MENTEE_NAME+' Comment',
                        style: 'font-weight: normal;color:#161718',
                        margin: '20 10 0 10',
                        cls : 'appLabelCls',
                        id: 'idLabelMenteeCommentsWaitScreen'
                    },
                    {
                        xtype: 'textareafield',
                        name: 'mentorComment',
                        id: 'menteeCommentWaitScreen',
                        disabledCls: 'textFieldDisableCLS',
                        // inputCls: 'inputCLS',
                        maxRows: 2,
                        disabled: true,
                        margin: '0 10 10 10',
                        clearIcon: false
                    },
                    {
                        xtype: 'label',
                        html: 'Pre-Meeting Prep',
                        id: 'idLabelMentorAcceptDeclineResponsesDisplay',
                        style: 'font-weight: normal;color:#161718',
                        margin: '20 10 0 10',
                        cls : 'appLabelCls',
                        hidden: true
                    },
                    {
                        xtype: 'textareafield',
                        id: 'idMentorAcceptDeclineResponsesDisplay',
                        disabledCls: 'textFieldDisableCLS',
                        inputCls: 'inputDisableCLS',
                        maxRows: 2,
                        readOnly: true,
                        margin: '0 10 10 10',
                        hidden: true
                    },
                    {
                        xtype: 'label',
                        html: 'Mentor Comment',
                        style: 'font-weight: normal;color:#161718',
                        margin: '20 10 0 10',
                        cls : 'appLabelCls',
                        id: 'idLabelMentorCommentsWaitScreen'
                    },
                    {
                        xtype: 'textareafield',
                        name: 'mentorComment',
                        id: 'mentorCommentWaitScreen',
                        disabledCls: 'textFieldDisableCLS',
                        inputCls: 'inputDisableCLS',
                        maxRows: 2,
                        disabled: true,
                        margin: '0 10 10 10'
                    },
                    {
                        xtype: 'label',
                        html: 'Mentor Comment for Cancellation',
                        style: 'font-weight: normal;color:#161718',
                        margin: '20 10 0 10',
                        cls : 'appLabelCls',
                        id: 'idLabelMentorCancellationCommentsWaitScreen'
                    },
                    {
                        xtype: 'textareafield',
                        //name: 'mentorComment',
                        id: 'mentorCancellationCommentWaitScreen',
                        disabledCls: 'textFieldDisableCLS',
                        inputCls: 'inputDisableCLS',
                        maxRows: 2,
                        disabled: true,
                        //hidden:true,
                        margin: '0 10 10 10'
                    },
                    {
                        xtype: 'label',
                        html: 'Comment for Reschedule',
                        style: 'font-weight: normal;color:#161718',
                        margin: '20 10 0 10',
                        cls : 'appLabelCls',
                        hidden: true,
                        id: 'idLabelRescheduleCommentsWaitScreen'
                    },
                    {
                        xtype: 'textareafield',
                        id: 'idRescheduleCommentsWaitScreen',
                        disabledCls: 'textFieldDisableCLS',
                        inputCls: 'inputDisableCLS',
                        maxRows: 2,
                        disabled: true,
                        hidden: true,
                        margin: '0 10 10 10'
                    }
                ]
            },
            {
                xtype: 'container',
                margin: '0 10 10 10',
                layout: {
                    type: 'hbox'
                },
                items: [
                    {
                        xtype: 'button',
                        text: 'Confirm Scheduled Meeting',
                        cls: 'signin-btn-cls',
                        pressedCls : 'press-btn-cls',
                        flex: 1,
                        action: 'AcceptRescheduledMeetingByMentor',
                        id: 'idAcceptRescheduledMeetingByMentor'
                    }
                ]
            },
            {
                xtype: 'container',
                margin: '0 10 10 10',
                id: 'idOKMentor',
                layout: {
                    type: 'hbox'
                },
                items: [
                    {
                        xtype: 'button',
                        text: 'OK',
                        cls: 'signin-btn-cls',
                        pressedCls : 'press-btn-cls',
                        flex: 1,
                        action: 'btnBackWaitScreen',
                        style:'font-size:17px;'
                    }
                ]
            },
            {
                xtype: 'container',
                margin: '0 10 10 10',
                layout: {
                    type: 'hbox'
                },
                items: [
//                    {
//                        xtype: 'button',
//                        text: 'Confirm that I will attend',
//                        cls: 'signin-btn-cls',
//                        flex: 1,
//                        action: 'AcceptAcceptedMeetingByMentor',
//                        id: 'idAcceptAcceptedMeetingByMentor'
//                    },
                    {
                        xtype: 'button',
                        text: 'ACCEPT INVITE',
                        //cls : 'tick',
                        id: 'idbtnAcceptMeetingMenteeNew',
                        cls: 'signin-btn-cls',
                        pressedCls : 'press-btn-cls',
                        action: 'btnAcceptMeetingMentee',
                        flex: 1,
                        hidden: true,
                        style:'font-size:17px;'
                    },
                ]
            },
            {
                xtype: 'container',
                layout: {
                    type: 'hbox',
                    pack: 'center'
                },
                margin: '0 20 10 20',
                items: [
                    {
                        xtype: 'button',
                        text: 'RESCHEDULE',
                        //cls : 'tick',
                        id: 'idbtnRescheduleMeetingMentee',
                        cls: 'signin-btn-cls',
                         pressedCls : 'press-btn-cls',
                        action: 'btnRescheduleMeetingMentee',
                        flex: 1,
                        margin: '0 5 0 0',
                        hidden: true,
                        style:'font-size:17px;'
                    },
                    {
                        xtype: 'button',
                        text: 'ACCEPT INVITE',
                        //cls : 'tick',
                        id: 'idbtnAcceptMeetingMentee',
                        cls: 'signin-btn-cls',
                        pressedCls : 'press-btn-cls',
                        action: 'btnAcceptMeetingMentee',
                        flex: 1,
                        margin: '0 5 0 0',
                        hidden: true,
                        style:'font-size:17px;'
                    },
                    {
                        xtype: 'button',
                        text: 'CANCEL INVITE',
                        //cls : 'cancel',
                        id: 'idBtnCancelMeetingMentee',
                        pressedCls : 'press-grey_btn-cls',
                        cls: 'grey-btn-cls',
                        action: 'btnCancelMeetingMentee',
                        flex: 1,
                        margin: '0 0 0 5',
                        hidden: true,
                        style:'font-size:17px;'
                    },
                    {
                        xtype: 'button',
                        text: 'DECLINE INVITE',
                        //cls : 'cancel',
                        id: 'idBtnDeclineMeetingMentee',
                        pressedCls : 'press-grey_btn-cls',
                        cls: 'grey-btn-cls',
                        action: 'btnDeclineMeetingMentee',
                        flex: 1,
                        margin: '0 0 0 5',
                        hidden: true,
                        style:'font-size:17px;',
                    }
                ]
            }
        ],
        listeners: [
            {
                event: "painted",
                fn: "onPagePainted"
            }
        ]
    },
    onPageActivate: function () {
        //Check The Login User and display the name on the top header
        var MentorLoginDetail = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
        var MenteeLoginDetail = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
        if (MentorLoginDetail != null) {
            Ext.getCmp('idUserNameWaitScreen').setHtml(MentorLoginDetail.MentorName);
        } else if (MenteeLoginDetail != null) {
            Ext.getCmp('idUserNameWaitScreen').setHtml(MenteeLoginDetail.MenteeName);
        }

        //Change the lable of Mentor and Mentor where Name Exits according to Application Configuration
        Ext.getCmp('idLabelMentorCommentsWaitScreen').setHtml(Mentor.Global.MENTOR_NAME + ' Comment');
        Ext.getCmp('idLabelMentorNameListWaitScreen').setTitle(Mentor.Global.MENTOR_NAME + ' Name');
        Ext.getCmp('idLabelMenteeCommentsWaitScreen').setHtml(Mentor.Global.MENTEE_NAME + ' Comment');
        Ext.getCmp('idLabelMentorEmailWaitScreen').setHtml(Mentor.Global.MENTOR_NAME + ' Email');
        Ext.getCmp('idLabelMentorPhoneNoWaitScreen').setHtml(Mentor.Global.MENTOR_NAME + ' Phone No');
    },
    onPagePainted: function () {
        clearInterval(Mentor.Global.SET_INTERVAL_ID);
        Mentor.Global.SET_INTERVAL_ID = 0;
    }
});
