Ext.define('Mentor.view.MeetingHistoryMentee', {
    extend: 'Ext.dataview.List',
    xtype: 'meetinghistorymentee',
    requires: [],
    config: {
        cls: 'meetinghistory',
        store: 'MeetingHistory',
        disableSelection: true,
        itemTpl: 
                /*'<div class="<tpl if="IsTeamMeeting == \'1\'">teammeeting</tpl> <tpl if="isCancelled == \'1\'">cancelledInvite</tpl> myContent Invitation{MeetingStatus}" style = "display: flex;min-height:100px;">' +
                    '<div style="width:20%; float:left; ">' +
                        '<img src={MentorImage} id="cricle" height="50px" width="50px" style = "border-radius: 50%;">' +
                        '<tpl if="NoteID != \'\'">' +
                            '<img class="edit" data-noteid="{NoteID}" src="./resources/images/edit_note.png" height="30px" width="30px" style="/* border-radius: 50%; * /margin-top: 15px;margin-left: 7px;">' +
                        '</tpl>' +
                    '</div>' +
                    '<div style="width:80%;float:left;padding: 0px 0px !important;vertical-align: bottom;min-height: 16px;">' +
                        '<div style="width:100%"><div style="width:50%; float:left;font-size: 14px;color: black;"><b>{MentorName}</b>'+
                    '</div>' +
                    //'<div style="width:50%; float:right;">'+
                    '<div style="float:right;">' +
                        '<tpl if="MeetingStartDatetime != \'0\'">' +
                            '<span style="vertical-align: baseline;"><img src="./resources/images/clock.png"  height="20px" width="20px"></span><span style = "vertical-align: text-top;font-size: 11px;color: black;">{MeetingStartDatetime}</span>' +
                        '<tpl else>' +
                            '<span style = "vertical-align: text-top;font-size: 11px;color: black;">Cancelled</span>' +
                        '</tpl>' +
                    '</div>'
                +'</div>' +
                //Ratting

                    '<tpl if="isCancelled == \'0\'">' +
                        '<div style="width:100%;float:left;">'+
                            '<span style="font-size:11px;background:#373737;padding: 1px 10px;color: white;">{averageText}</span>' +
                            '<tpl for=".">' +
                                '<img style = "margin-left: 2px;margin-right: 2px;"src="./resources/images/{average}.png" height="10px" >' +
                            '</tpl>' +
                        '</div>' +
                    '</tpl>' +
                
                    //Main Topics
                    '<div style="width:100%;float:left;">'+
                        '<span style="font-size:11px;color: black;">Main Topic : {TopicDescription}</span>' +
                        //Sub Topics Topics
                        '<tpl if="MeetingStatus != \'1\'">'+
                            '<div style="width:100%;float:left;">'+
                                '<span style="font-size:11px;color: black;">Sub Topics : {SubTopicName}</span>'+
                                '<tpl else >'+
                                    '<div style="width:100%;float:left;">'+
                                        '<span style="font-size:11px;color: transparent;">Sub Topics : {SubTopicName}</span>'+
                                '</tpl>' +
                                /*'<div style="width:100%;float:left;"><span style="font-size:11px;color: black;">Sub Topics : {SubTopicName}</span>'+* /
                            '<div>' +
                    '</div>',*/


                    '<div class="<tpl if="IsTeamMeeting == \'1\'">teammeeting</tpl> <tpl if="isCancelled == \'1\'">cancelledInvite</tpl> myContent Invitation{MeetingStatus}"  style = "display: flex;min-height:100px;box-shadow: 1px 1px 4px 0.2px #888888;margin-left:10px;margin-right:10px;padding: 10px;">' +
                    '<div style="width:30%; float:left; ">' +
                        '<img src={MentorImage} id="cricle" height="80px" width="80px" style = "border-radius: 50%;">' +
                        '<tpl if="NoteID != \'\'">' +
                            '<img class="edit" data-noteid="{NoteID}" src="./resources/images/edit_note.png" height="30px" width="30px" style="/* border-radius: 50%; */margin-top: 15px;margin-left: 7px;">' +
                        '</tpl>' +
                    '</div>' +
                    '<div style="width:70%;float:left;padding: 0px 0px !important;vertical-align: bottom;min-height: 16px;">' +
                        '<div style="width:100%">'+
                            '<div style="width:80%;float:left;line-height: 15px;">'+
                                '<div style="font-size: 14px;color: black;"><b>{MentorName}</b>'+
                                '</div>' +
                                //Main Topics
                                '<div style="margin-top:3px;">'+
                                    '<div style="font-size:11px;color:#858585;">Main Topic : {TopicDescription}</div>' +
                                    //Sub Topics Topics
                                    '<tpl if="MeetingStatus != \'1\'">'+
                                        '<div style="/*float:left;*/">'+
                                            '<span style="font-size:11px;color:#858585;">Sub Topics : {SubTopicName}</span> '+
                                        '</div>' +
                                    '<tpl else >'+
                                        '<div style="float:left;">'+
                                            '<span style="font-size:11px;color: transparent;">Sub Topics : {SubTopicName}</span>'+
                                        '</div>'+
                                    '</tpl>' +
                                    /*'<div style="width:100%;float:left;"><span style="font-size:11px;color: black;">Sub Topics : {SubTopicName}</span>'+*/
                                '</div>' +
                                '<div>' +
                                    '<tpl if="MeetingStartDatetime != \'0\'">' +
                                        '<span style=""><img src="./resources/images/clock_notelist.png"  height="13px" width="13px">'+
                                        '</span>'+
                                        '<span style = "font-size: 11px;color:#858585;"> {MeetingStartDatetime}'+
                                        '</span>' +
                                    '<tpl else>' +
                                        '<span style = "font-size: 11px;color:#858585;">Cancelled</span>' +
                                    '</tpl>' +
                                    //'<span style="vertical-align: baseline;"><img src="./resources/images/clock.png"  height="20px" width="20px"></span><span style = "vertical-align: text-top;font-size: 11px;color: black;">{MeetingStartDatetime}</span>' +
                                '</div>'+
                            '</div>' +
                            '<div style="width:20%; float:right;">'+
                                //Ratting
                                '<tpl if="isCancelled == \'0\'">' +
                                    '<div style="float:right;">' +
                                        '<span style="border-radius: 50px;float:right;font-size:10px;background:#512b80;padding: 1px 10px;color: white;">{averageText}</span>' +
                                        //'<tpl for=".">' +
                                        '<img style = "float:right;margin-top: 5px;margin-left: 2px;margin-right: 2px;"src="./resources/images/{average}.png"  height="10px" >' +
                                         //'</tpl>' +
                                    '</div>' +
                                '</tpl>' +
                            '</div>' +

                         
                            
                        '</div>' +
                       
                    '</div>'+
                 '</div>',
        items: [
            {
                //title: 'Meeting History',
                docked: 'top',
                xtype: 'panel',
                cls: 'mettingHistoryToolBar',
                defaults: {
                    iconMask: true
                },
                items: [
                    {
                        docked: 'top',
                        xtype: 'toolbar',
                        title: 'Meeting History',
                        cls: 'my-titlebar',
                        defaults: {
                            iconMask: true
                        },
                        items: [
                            {
                                iconCls: 'helpIcon',
                                docked: 'right',
                                listeners: {
                                    element: 'element',
                                    tap: function () {
                                        var popup = Mentor.app.application.getController('AppDetail').addHelpPopup('meeting_history');
                                        Ext.Viewport.add(popup);
                                        popup.show();
                                    }
                                }
                            },
                            {
                                iconCls: 'refreshIcon',
                                action: 'btnRefreshMeetingHistory',
                                id: 'idMenteeMeetingHistory',
                                docked: 'right'
                            },
                            /*{
                                //iconCls: 'addMettingMentorMettingHistory',
                                //action : 'btnBackMenteeReviewForm',
                                //style : 'visibility: hidden;',
                                iconCls: 'btnBackCLS',
                                docked: 'left',
                                handler: function () {
                                    //Ext.getCmp('idPendingInvitesTabInvitesMenteeBottomTabView').setActiveItem(0);
                                    var View = Mentor.Global.NavigationStack.pop();
                                    var viewport = Ext.Viewport,
                                    mainPanel = viewport.down("#mainviewport");
                                     
                                    mainPanel.animateActiveItem(View, {type: "slide",direction: "right"});
                                }
                            },*/
                            {
                                xtype: 'button',
                                id: 'listButton',
                                iconCls: 'leftMenuIconCLS',
                                 iconMask: true,
                                docked: 'left',
                                ui: 'plain',
                                handler: function(){
                                    if(Ext.Viewport.getMenus().left.isHidden()){
                                        Ext.Viewport.showMenu('left');
                                    }
                                    else
                                    {
                                        Ext.Viewport.hideMenu('left');
                                    }
                                }
                            },
                            {
                                //style: 'visibility:hidden'
                                iconCls: 'goToNotesIcon',
                                docked: 'left',
                                id: 'addnotes',
                                action: 'addnotes'
                            }
                        ]
                    },
                    {
                        //style : 'background: black !important;',
                        xtype: 'panel',
                        cls: 'searchFieldCls',
                        items: [
                            {
                                //style : 'background: black !important;',
                                xtype: 'searchfield',
                                 cls : 'searchNotesCLS',
                                inputCls : 'searchNotesInputCLS',
                                placeHolder: 'Search meeting history',
                                itemId: 'searchBoxMenteeMettingHistory'
                            }
                        ]
                    }
                ]
            },
            {
                xtype: 'label',
                id: 'idUserName',
                style: 'text-align: right;padding-right: 10px;padding: 10px;background-color: #C2C2C2;',
                docked: 'top',
                hidden: true
            },
            {
                xtype: 'label',
                id: 'idVersionMenteeMeetingHistory',
                style: 'text-align: right;padding-right: 10px;padding: 10px;',
                docked: 'bottom',
                hidden: true
            },
            {
                xtype: 'container',
                hidden: true,
                layout: {
                    type: 'hbox',
                    pack: 'center'
                },
                docked: 'top',
                items: [
                    {
                        xtype: 'button',
                        label: 'Add New Meeting',
                        cls: 'signin-btn-cls',
                        text: 'Issue Invite',
                        style: 'width:45%;margin-bottom:10px;',
                        action: 'btnInviteForMeeting'
                    },
                    {
                        xtype: 'button',
                        label: '',
                        cls: 'signin-btn-cls',
                        text: 'Invite Status',
                        style: 'width:45%;margin-bottom:10px;',
                        action: 'btnInviteStatusMeetingHistory'
                    }
                ]
            }
        ],
        listeners: [
            {
                event: 'itemtap',
                fn: function (view, index, item, record, e) {
                    if (Ext.dom.Element.fly(e.target).hasCls('edit')) {
                        var editNotePopup = Mentor.app.application.getController('AppDetail').editNotePopup('', '', Ext.dom.Element.fly(e.target).getAttribute('data-noteid'));
                        Ext.Viewport.add(editNotePopup);
                        editNotePopup.show();
                    } else {
                        Mentor.app.application.getController('AppDetail').displayDetail(view, index, item, record, e);
                    }
                }
            },
            {
                event: "activate",
                fn: "onPageActivate"
            },
            {
                event: "painted",
                fn: "onPagePainted"
            },
        ]
    },
    onPageActivate: function () {
        Mentor.Global.TabNavigationStack = 0;
    },
    onPagePainted: function () {
        Mentor.app.application.getController('AppDetail').getMeetingHistory();

        clearInterval(Mentor.Global.SET_INTERVAL_ID);
        Mentor.Global.SET_INTERVAL_ID = 0;
        Mentor.Global.SET_INTERVAL_ID = setInterval(function () {
            Mentor.app.application.getController('AppDetail').getMeetingHistory(true);
        }, Mentor.Global.SET_INTERVAL_TIME);
    }
});
