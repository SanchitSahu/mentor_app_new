Ext.define('Mentor.view.AcceptRejectPendingRequest', {
    extend: 'Ext.Panel',
    xtype: 'acceptRejectPendingRequest',
    requires: [
    ],
    config: {
        cls: 'forgotpassword',
        //scrollable: "vertical",
        scrollable: {
            direction: 'vertical',
            indicators: {
                y: {
                    autoHide: false
                }
            }
        },
        defaults: {
            xtype: 'formpanel'
        },
        items: [
            {
                docked: 'top',
                xtype: 'toolbar',
                title: 'Respond Pending Invite',
                cls: 'my-titlebar',
                defaults: {
                    iconMask: true
                },
                items: [
                    {
                        //iconCls: 'arrow_left',
                        iconCls: 'btnBackCLS',
                        action: 'btnBackAcceptRejectPendingRequest',
                        docked: 'left'
                    },
                    {
                        iconCls: 'goToNotesIcon',
                        docked: 'left',
                        //style: 'visibility:hidden'
                        id: 'addnotes',
                        action: 'addnotes'
                    },
                    /*{
                     iconCls: 'addIcon',
                     docked: 'left',
                     listeners: {
                     element: 'element',
                     tap: function () {
                     var popup = Mentor.app.application.getController('AppDetail').addNotePopup();
                     Ext.Viewport.add(popup);
                     popup.show();
                     }
                     }
                     },*/
                    {
                        //iconCls: 'arrow_left',
                        iconCls: 'helpIcon',
                        docked: 'right',
                        listeners: {
                            element: 'element',
                            tap: function () {
                                var popup = Mentor.app.application.getController('AppDetail').addHelpPopup('pending_invite_detail');
                                Ext.Viewport.add(popup);
                                popup.show();
                            }
                        }
                    },
                    {
                        iconCls: 'goToNotesIcon',
                        docked: 'right',
                        style: 'visibility:hidden'
                    }
                ]
            },
            {
                xtype: 'label',
                id: 'idUserName',
                style: 'text-align: right;padding-right: 10px;padding: 10px;background-color: #C2C2C2;',
                docked: 'top',
                hidden: true
            },
            {
                xtype: 'label',
                id: 'idVersionAcceptRejectPendingRequest',
                style: 'text-align: right;padding-right: 10px;padding: 10px;',
                docked: 'bottom',
                hidden: true
            },
//            {
//                xtype: 'panel',
//                cls: 'login-top',
//                paddingTop: 10,
//                layout: {
//                    type: 'hbox',
//                    pack: 'center'
//                },
//                items: [
//                    {
//                        xtype: 'container',
//                        layout: {
//                            type: 'vbox',
//                            pack: 'center'
//                        },
//                        items: [
//                            {
//                                xtype: 'label',
//                                id: 'idMeetingFromReject',
//                                style: 'font-size:16px;color:#000000;margin:10px;'
//
//                            }
//                        ]
//                    }
//                ]
//            },
            {
                xtype: 'label',
                id: 'idLabelMenteeNameAcceptReject',
                style: 'font-weight: normal;',
                //margin: '10 10 3 10'
                margin: '20 20 0 20',
                cls : 'appLabelCls',
                hidden : true,
            },
            {
                xtype: 'textfield',
                id: 'idMenteeNameAcceptReject',
                disabled: true,
                //disabledCls: 'textFieldDisableAcceptAndRejectMenteeRequestCLS',
                disabledCls: 'textFieldDisableCLS',
                inputCls: 'inputDisableCLS',
                margin: '0 20 0 20',
                hidden : true,
            },
            {
                xtype: 'label',
                id: 'idInfoMenteeNameAcceptRejectPendingRequest',
                style: 'text-align: center;padding: 15px;border-bottom:1px solid #ccd4ee;color:#181717;font-size: 17px;',
                //html : 'Meeting Request from Mentee1',

            },
            {
                xtype: 'label',
                html: 'Meeting Topic',
                style: 'font-weight: normal;',
                margin: '20 20 3 20',
                cls : 'appLabelCls',
            },
            {
                xtype: 'textfield',
                name: 'acceptRejectPendingRequestMeetingTopic',
                id: 'idMeetingTopicAcceptRejectPendingRequest',
                disabled: true,
                //disabledCls: 'textFieldDisableAcceptAndRejectMenteeRequestCLS',
                disabledCls: 'textFieldDisableCLS',
                inputCls: 'inputDisableCLS',
                margin: '0 20 0 20'
            },
            
            {
                xtype: 'label',
                html: Mentor.Global.MENTEE_NAME + ' Comments',
                style: 'font-weight: normal;',
                margin: '20 20 3 20',
                id: 'idLabelMenteeRescheduleCommentsAcceptRejectPendingRequest',
                hidden: true,
                cls : 'appLabelCls',
            },
            {
                xtype: 'textareafield',
                name: 'menteeCommentAcceptRejectPendingRequest',
                id: 'menteeRescheduleCommentAcceptRejectPendingRequest',
                //disabledCls: 'textFieldDisableAcceptAndRejectMenteeRequestCLS',
                 disabledCls: 'textFieldDisableCLS',
                inputCls: 'inputDisableCLS',
                maxRows: 2,
                disabled: true,
                margin: '0 20 0 20',
                clearIcon: false,
                hidden: true
            },
            {
                xtype: 'label',
                html: Mentor.Global.MENTEE_NAME + " Email",
                style: 'font-weight: normal;',
                margin: '20 20 3 20',
                cls : 'appLabelCls',
            },
            {
                xtype: 'textfield',
                id: 'idMenteeEmailAcceptRejectPendingRequest',
                disabled: true,
                //disabledCls: 'textFieldDisableAcceptAndRejectMenteeRequestCLS',
                disabledCls: 'textFieldDisableCLS',
                inputCls: 'inputDisableCLS',
                margin: '0 20 0 20'
            },
            {
                xtype: 'label',
                html: Mentor.Global.MENTEE_NAME + " Phone No",
                style: 'font-weight: normal;',
                margin: '20 20 3 20',
                cls : 'appLabelCls',
            },
            {
                xtype: 'textfield',
                id: 'idMenteePhoneNoAcceptRejectPendingRequest',
                disabled: true,
                //disabledCls: 'textFieldDisableAcceptAndRejectMenteeRequestCLS',
                disabledCls: 'textFieldDisableCLS',
                inputCls: 'inputDisableCLS',
                margin: '0 20 0 20'
            },
            {
                xtype: 'panel',
                margin: '10 20 10 20',
                layout: {
                    type: 'hbox'
                },
                items: [
                    {
                        xtype: 'checkboxfield',
                        id: 'idIsPhoneSMSCapableAcceptRejectPendingRequest',
                        //label: "Is above number Text Message(aka SMS) capable?",
                        label: "Mentee can not receive Text Message(aka SMS) on this phone",
                        labelCls: 'radio-btn-cls',
                        labelWidth: '100%',
                        labelAlign: 'left',
                        disabled: true,
                        cls: 'wordWrapSpan',
                        style : 'background: transparent;'
                    }
                ]
            },
            {
                xtype: 'panel',
                layout: {
                    type: 'hbox'
                },
                margin : '0 10',
                style: 'margin-top:-20px;margin-left:-10px,margin-right:-10px',
                items: [
                    {
                        xtype: 'fieldset',
                        title: "Meeting Date",
                        flex: 1,
                        items: [
                            /*{
                             xtype: 'datetimepickerfield',
                             name : '24hrdt',
                             hidden:true,
                             id:'datePickerAcceptRejectPendingRequest',
                             //label: '24 Hr DateTime',
                             //value: new Date(),
                             dateTimeFormat : 'Y-m-d',
                             picker: {
                             yearFrom: new Date().getFullYear(),
                             yearTo : new Date(new Date().setYear((new Date().getFullYear()+1))).getFullYear(),
                             minuteInterval : 1,
                             ampm : false,
                             slotOrder: ['month', 'day', 'year']
                             },
                             listeners: {
                             change: function (eOpts,value ) {
                             var MeetingTime = Ext.getCmp('timeAcceptRejectPendingRequest').getValue();
                             
                             var currentDate  = new Date();
                             currentDate.setHours(0,0,0,0);
                             var selectedDate = new Date(value);
                             selectedDate.setHours(0,0,0,0);
                             if(currentDate  > selectedDate ){
                             Ext.Msg.alert("MELS","Please check the date");
                             //Ext.getCmp('datePickerAcceptRejectPendingRequest').setValue(new Date());
                             
                             }
                             else{
                             if(MeetingTime!="" ){
                             var MeetingDate = Ext.getCmp('datePickerAcceptRejectPendingRequest').getFormattedValue();
                             var MeetingDate = new Date(MeetingDate + " "+MeetingTime);
                             if(new Date()  > MeetingDate ){
                             Ext.Msg.alert("MELS","Please select future date.");
                             
                             }
                             else{
                             Ext.getCmp('dateTimeAcceptRejectPendingRequest').setValue(this.getFormattedValue());
                             }
                             }
                             else
                             Ext.getCmp('dateTimeAcceptRejectPendingRequest').setValue(this.getFormattedValue());
                             }
                             },
                             
                             }
                             },	*/
                            {
                                xtype: 'textfield',
                                disabled: true,
                                //disabledCls: 'textFieldDisableAcceptAndRejectMenteeRequestCLS',
                                //disabledCls: 'textFieldDisableCLS',
                                inputCls: 'textFieldDateTimeAcceptAndRejectMenteeRequestCLS',
                                id: 'dateTimeAcceptRejectPendingRequest',
                                listeners: {
                                    element: 'element',
                                    tap: function () {
                                        var input = $('#timeAcceptRejectPendingRequest');
                                        input.clockpicker('hide');
                                        /*var Datepicker  = Ext.getCmp('datePickerAcceptRejectPendingRequest').getPicker();
                                         Datepicker.show();*/
                                        var popup = new Ext.Panel({
                                            floating: true,
                                            centered: true,
                                            modal: true,
                                            hideOnMaskTap: true,
                                            autoDestroy: true,
                                            width: 310,
                                            height: 410,
                                            items: [
                                                {
                                                    xtype: 'popview',
                                                    id: 'idCalenderViewAcceptRejectPendingRequest',
                                                    listeners: {
                                                        selectionchange: function (a, newDate, oldDate, eOpts)
                                                        {
                                                            console.log(newDate);
                                                            //var MeetingTime = Ext.getCmp('timeAcceptRejectPendingRequest').getValue();
                                                            var MeetingTime = '';
                                                            var currentDate = '';
                                                            var selectedDate = '';
                                                            var Year = '';
                                                            var Month = '';
                                                            var date = '';
                                                            var MeetingDate = '';
                                                            var SelectedMeetingDate = '';

                                                            MeetingTime = Ext.getCmp('timeAcceptRejectPendingRequest').getValue();
                                                            currentDate = new Date();
                                                            currentDate.setHours(0, 0, 0, 0);
                                                            selectedDate = new Date(newDate);
                                                            selectedDate.setHours(0, 0, 0, 0);
                                                            if (currentDate > selectedDate) {
                                                                Ext.Msg.alert("MELS", "Please select future date");
                                                                popup.hide();
                                                                Ext.Viewport.remove(popup);
                                                            } else {
                                                                Year = newDate.getFullYear();
                                                                Month = (newDate.getMonth() + 1);
                                                                if (Month < 10) {
                                                                    Month = "0" + Month;
                                                                }
                                                                date = newDate.getDate();
                                                                if (date < 10) {
                                                                    date = "0" + date;
                                                                }
                                                                SelectedMeetingDate = Year + "-" + Month + "-" + date;
                                                                if (MeetingTime != "") {
                                                                    MeetingDate = SelectedMeetingDate;
                                                                    MeetingDate = Date.parse(MeetingDate + " " + MeetingTime);
                                                                    if (new Date() > MeetingDate) {
                                                                        Ext.Msg.alert("MELS", "Please select future date.");
                                                                    } else {
                                                                        Mentor.Global.Current_date = new Date(MeetingDate);
                                                                        Ext.getCmp('dateTimeAcceptRejectPendingRequest').setValue(SelectedMeetingDate);
                                                                    }
                                                                    popup.hide();
                                                                    Ext.Viewport.remove(popup);
                                                                } else {
                                                                    Ext.getCmp('dateTimeAcceptRejectPendingRequest').setValue(SelectedMeetingDate);
                                                                    Mentor.Global.Current_date = Date.parse(SelectedMeetingDate + ' 00:00:01');
                                                                    popup.hide();
                                                                    Ext.Viewport.remove(popup);
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            ],
                                            listeners: {
                                                hide: function (a, b) {
                                                    popup.hide();
                                                    Ext.Viewport.remove(popup);
                                                }
                                            }
                                        });
                                        Ext.Viewport.add(popup);
                                        Ext.getCmp('calendarViewID').setViewConfig({value: Mentor.Global.Current_date});
                                        Ext.getCmp('calendarViewID').setActiveItem(2);
                                        Ext.getCmp('calendarViewID').setActiveItem(2);
                                        Ext.getCmp('calendarViewID').setActiveItem(-2);
                                        Ext.getCmp('calendarViewID').setActiveItem(-2);
                                        popup.show();
                                    }
                                }
                            }
                        ]
                    },
                    {
                        xtype: 'fieldset',
                        title: "Meeting Time",
                        flex: 1,
                        items: [
                            {
                                xtype: 'datetimepickerfield',
                                name: '24hrdt',
                                id: 'timePickerAcceptRejectPendingRequest',
                                hidden: true,
                                dateTimeFormat: 'H:i',
                                picker: {
                                    yearFrom: new Date().getFullYear(),
                                    yearTo: new Date(new Date().setYear((new Date().getFullYear() + 1))).getFullYear(),
                                    minuteInterval: 1,
                                    ampm: false,
                                    slotOrder: ['hour', 'minute']
                                }
                            },
                            {
                                xtype: 'textfield',
                                disabled: false,
                                //disabledCls: 'textFieldDisableAcceptAndRejectMenteeRequestCLS',
                                 disabledCls: 'textFieldDisableCLS',
                                inputCls: 'textFieldDateTimeAcceptAndRejectMenteeRequestCLS',
                                id: 'timeAcceptRejectPendingRequest',
                                readOnly: true,
                                listeners: {
                                    element: 'element',
                                    painted: function () {
                                        var input = $('#timeAcceptRejectPendingRequest');
                                        input.clockpicker({
                                            align: 'right',
                                            beforeDone: function () {
                                                Ext.getCmp("TimePickerAcceptRejectPendingOldValue").setValue(Ext.getCmp('timeAcceptRejectPendingRequest').getValue());
                                            },
                                            afterDone: function () {
                                                console.log("a");
                                                var MeetingDate = Ext.getCmp('dateTimeAcceptRejectPendingRequest').getValue();
                                                var value = $('#timeAcceptRejectPendingRequest input').val();
                                                if (value.length > 6 || typeof value.length == "undefined") {
                                                    return;
                                                }
                                                if (MeetingDate != "") {
                                                    MeetingDate = Date.parse(MeetingDate + " " + value);
                                                    if (new Date() > MeetingDate) {
                                                        var old_value = Ext.getCmp('TimePickerAcceptRejectPendingOldValue').getValue();
                                                        Ext.getCmp('timeAcceptRejectPendingRequest').setValue(null);
                                                        Ext.Msg.alert("MELS", "Please select future time");
                                                    } else {
                                                        Ext.getCmp("TimePickerAcceptRejectPendingOldValue").setValue(this.value);
                                                        Ext.getCmp('timeAcceptRejectPendingRequest').setValue(value);
                                                    }
                                                } else {
                                                    Ext.getCmp('timeAcceptRejectPendingRequest').setValue(value);
                                                }
                                            }
                                        });
                                    },
                                    tap: function () {
                                        var input = $('#timeAcceptRejectPendingRequest');
                                        input.clockpicker({
                                            align: 'right',
                                            beforeDone: function () {
                                                Ext.getCmp("TimePickerAcceptRejectPendingOldValue").setValue(Ext.getCmp('timeAcceptRejectPendingRequest').getValue());
                                            },
                                            afterDone: function () {
                                                console.log("a");
                                                var MeetingDate = Ext.getCmp('dateTimeAcceptRejectPendingRequest').getValue();
                                                var value = $('#timeAcceptRejectPendingRequest input').val();
                                                if (value.length > 6 || typeof value.length == "undefined") {
                                                    return;
                                                }
                                                if (MeetingDate != "") {
                                                    MeetingDate = Date.parse(MeetingDate + " " + value);
                                                    if (new Date() > MeetingDate) {
                                                        var old_value = Ext.getCmp('TimePickerAcceptRejectPendingOldValue').getValue();
                                                        Ext.getCmp('timeAcceptRejectPendingRequest').setValue(null);
                                                        Ext.Msg.alert("MELS", "Please select future time");
                                                    } else {
                                                        Ext.getCmp("TimePickerAcceptRejectPendingOldValue").setValue(this.value);
                                                        Ext.getCmp('timeAcceptRejectPendingRequest').setValue(value);
                                                    }
                                                } else {
                                                    Ext.getCmp('timeAcceptRejectPendingRequest').setValue(value);
                                                }
                                            }
                                        });
                                    },
                                    blur: function () {
                                        var input = $('#timeAcceptRejectPendingRequest');
                                        input.clockpicker('hide');
                                    }
                                }
                            }
                        ]
                    }
                ]
            },
            {
                xtype: 'textfield',
                id: 'TimePickerAcceptRejectPendingOldValue',
                hidden: true
            },
            {
                xtype: 'fieldset',
                //title: "Meeting Type<span class='spanAsterisk'>*</span>",
                title: "Meeting Type",
                margin : '0 20',
                style: 'margin-top:-20px;',
                items: [
                    {
                        xtype: 'selectfield',
                        id: 'idMeetingTypeSelectFieldAcceptRejectPendingRequest',
                        store: 'MeetingType',
                        displayField: 'MeetingTypeName',
                        valueField: 'MeetingTypeID',
                        // autoSelect: false,
                        // placeHolder: '-- Select Meeting Type --',
                        listeners: {
                            change: function (field, value) {
                                if (value instanceof Ext.data.Model) {
                                    value = value.get(field.getValueField());
                                }
                                console.log(value);
                                var Store = Ext.getStore('MeetingType');
                                var record = Store.findRecord('MeetingTypeID', value);
                                if (record.data.MeetingTypeID == "") {
                                    Ext.getCmp('idMeetingPlaceSelectFieldAcceptRejectPendingRequest').setDisabled(false);
                                    Ext.getCmp('idMeetingPlaceSelectFieldAcceptRejectPendingRequest').removeCls('inputDisableCLS');
                                } else if (record.data.MeetingTypeName != "In Person") {
                                    Ext.getCmp('idMeetingPlaceSelectFieldAcceptRejectPendingRequest').setDisabled(true);
                                    Ext.getCmp('idMeetingPlaceSelectFieldAcceptRejectPendingRequest').setValue(3);
                                    Ext.getCmp('idMeetingPlaceSelectFieldAcceptRejectPendingRequest').addCls('inputDisableCLS');
                                } else {
                                    Ext.getCmp('idMeetingPlaceSelectFieldAcceptRejectPendingRequest').setDisabled(false);
                                    Ext.getCmp('idMeetingPlaceSelectFieldAcceptRejectPendingRequest').removeCls('inputDisableCLS');
                                }
                            }
                        }
                    }
                ]
            },
            {
                xtype: 'fieldset',
                //title: "Meeting Place<span class='spanAsterisk'>*</span>",
                title: "Meeting Place",
                id: 'idLabelSelectMeetingPlaceAcceptRejectPendingRequest',
                margin : '10 20 20 20',
                //style: 'margin-top:-20px;',
                items: [
                    {
                        // autoSelect: false,
                        // placeHolder: '-- Select Meeting Place --'
                        xtype: 'selectfield',
                        id: 'idMeetingPlaceSelectFieldAcceptRejectPendingRequest',
                        store: 'MeetingPlace',
                        displayField: 'MeetingPlaceName',
                        valueField: 'MeetingPlaceID'
                    }
                ]
            },
            {
                xtype: 'label',
                html: Mentor.Global.MENTEE_NAME + ' Comments',
                style: 'font-weight: normal;',
                margin: '20 20 3 20',
                id: 'idLabelMenteeCommentsAcceptRejectPendingRequest',
                cls : 'appLabelCls',
                hidden : true,

            },
            {
                xtype: 'textareafield',
                name: 'menteeCommentAcceptRejectPendingRequest',
                id: 'menteeCommentAcceptRejectPendingRequest',
                //disabledCls: 'textFieldDisableAcceptAndRejectMenteeRequestCLS',
                disabledCls: 'textFieldDisableCLS',
                inputCls: 'inputDisableCLS',
                maxRows: 2,
                disabled: true,
                margin: '0 20 0 20',
                clearIcon: false,
                hidden : true,
            },

            {
                xtype: 'label',
                //html: Mentor.Global.MENTOR_NAME + 'Comments', Add title in PageActivate function
                style: 'font-weight: normal;',
                margin: '20 20 3 20',
                cls : 'appLabelCls',
                id: 'idLabelMentorCommentsAcceptRejectPendingRequest',

            },
            {
                xtype: 'textareafield',
                cls: 'textFieldDisableCLS',
                maxRows: 2,
                //placeHolder : Mentor.Global.MENTOR_NAME + 'Comments on '+Mentor.Global.MENTEE_NAME  ,Add palceholder in PageActivate function
                margin: '0 20 0 20',
                clearIcon: false,
                inputCls: 'inputCLS',
                id: 'mentorCommentAcceptRejectPendingRequest',
            },
            /*{
             xtype: 'fieldset',
             title: "Meeting Date Time<span class='spanAsterisk'>*</span>",
             style : 'margin-top:-20px;',
             items: [
             {
             xtype: 'datetimepickerfield',
             name : '24hrdt',
             id:'dateTimeAcceptRejectPendingRequest',
             //label: '24 Hr DateTime',
             value: new Date(),
             dateTimeFormat : 'Y-m-d H:i',
             picker: {
             yearFrom: new Date().getFullYear(),
             yearTo : new Date(new Date().setYear((new Date().getFullYear()+1))).getFullYear(),
             minuteInterval : 1,
             ampm : false,
             slotOrder: ['month', 'day', 'year','hour','minute']
             },
             listeners: {
             change: function (eOpts,value ) {
             if(new Date()  > value ){
             Ext.Msg.alert("MELS","Please check the date");
             Ext.getCmp('dateTimeAcceptRejectPendingRequest').setValue(new Date());
             
             }
             },
             
             }
             },
             ]
             },*/
//            {
//                xtype: 'panel',
//                flex: 1,
//                style: 'margin-top:10px;',
//                items: [
//                    {
//                        xtype: 'fieldset',
//                        id: 'idMentorCommetnsFieldSetAcceptRejectPendingRequest',
//                        title: Mentor.Global.MENTOR_NAME + " Comments",
//                        items: [
//                            {
//                                xtype: 'textareafield',
//                                id: 'commentsAcceptRejectPendingRequest',
//                                placeHolder: Mentor.Global.MENTOR_NAME + ' Comments',
//                                inputCls: 'inputCLS',
//                                clearIcon: false,
//                            }
//
//                        ]
//                    },
//                ]
//            },
            {
                xtype: 'container',
                id: 'NormalAcceptReject',
                layout: {
                    type: 'hbox',
                    pack: 'center'
                },
                margin: '20 20 10 20',
                hidden: true,
                items: [
                    {
                        xtype: 'button',
                        text: 'NEXT',
                        //cls : 'tick',
                        cls: 'signin-btn-cls',
                         pressedCls : 'press-btn-cls',
                        action: 'doAcceptRequest',
                        flex: 1,
                        margin: '0 5 0 0'
                    },
                    {
                        xtype: 'button',
                        text: 'DECLINE',
                        //cls : 'cancel',
                        pressedCls : 'press-grey_btn-cls',
                        cls: 'grey-btn-cls',
                        action: 'doDeclineRequest',
                        flex: 1,
                        margin: '0 0 0 5'
                    }
                ]
            },
            {
                xtype: 'container',
                id: 'RescheduleAcceptReject',
                layout: {
                    type: 'vbox',
                    pack: 'center'
                },
                 margin: '20 10 10 10',
                hidden: true,
                items: [
                    {
                        xtype: 'button',
                        text: 'Grant Reschedule',
                        //cls : 'tick',
                        cls: 'signin-btn-cls',
                        pressedCls : 'press-btn-cls',
                        action: 'doAcceptRequest',
//                        flex: 1,
                        //margin: '0 5 0 5'
                    },
                    {
                        xtype: 'button',
                        text: 'Deny Reschedule',
                        //cls : 'cancel',
                        pressedCls : 'press-grey_btn-cls',
                        cls: 'grey-btn-cls',
                        action: 'doDeclineRequest',
//                        flex: 1,
                        //margin: '5 5 0 5'
                    }
                ]
            }
        ],
        listeners: [
            {
                event: "activate",
                fn: "onPageActivate"
            },
            {
                event: "painted",
                fn: "onPagePainted"
            }
        ]
    },
    onPageActivate: function () {
        localStorage.setItem('CacheAcceptResponse', null);
        localStorage.setItem('CacheDeclineResponse', null);

        //Change the lable of Mentor and Mentor where Name Exits according to Application Configuration
        Ext.getCmp('idLabelMenteeNameAcceptReject').setHtml(Mentor.Global.MENTEE_NAME + ' Name');
        Ext.getCmp('idLabelMenteeCommentsAcceptRejectPendingRequest').setHtml(Mentor.Global.MENTEE_NAME + ' Comments');
        Ext.getCmp('idLabelMentorCommentsAcceptRejectPendingRequest').setHtml( Mentor.Global.MENTOR_NAME + ' Comments');
        Ext.getCmp('mentorCommentAcceptRejectPendingRequest').setPlaceHolder(Mentor.Global.MENTOR_NAME + " Comments on "+Mentor.Global.MENTEE_NAME+"'s request");
              
        //Ext.getCmp('commentsAcceptRejectPendingRequest').setPlaceHolder(Mentor.Global.MENTOR_NAME + ' Comments');
        //Ext.getCmp('idMentorCommetnsFieldSetAcceptRejectPendingRequest').setTitle(Mentor.Global.MENTOR_NAME + ' Comments');
        Ext.getStore('SchoolDetail').insert(0, {"subdomain_id": "-1", "subdomain_name": "Select School", "school_name": "-- Select School --"});
        //Ext.getCmp('dateTimeAcceptRejectPendingRequest').setValue(new Date());
    },
    onPagePainted: function () {
        clearInterval(Mentor.Global.SET_INTERVAL_ID);
        Mentor.Global.SET_INTERVAL_ID = 0;
        Ext.getStore('MeetingType').load();
        localStorage.setItem('mentorAcceptInvitationDetail', null);
        localStorage.setItem('mentorDeclineInvitationDetail', null);

    }
});