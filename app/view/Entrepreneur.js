Ext.define('Mentor.view.Entrepreneur', {
    extend: 'Ext.Panel',
    xtype: 'entrepreneur',
    requires: [
    ],
    config: {
        cls: 'entrepreneur',
        scrollable: {
            direction: 'vertical',
            indicators: {
                y: {
                    autoHide: false
                }
            }
        },
        defaults: {
            xtype: 'container',
            
        },
        items: [
            {
                docked: 'top',
                xtype: 'toolbar',
                title: 'New Meeting',
                cls: 'my-titlebar',
                id: 'idTitleEntrepreneur',
                defaults: {
                    iconMask: true
                },
                items: [
                    {
                        iconCls: 'helpIcon',
                        docked: 'right',
                        listeners: {
                            element: 'element',
                            tap: function () {
                                var MeetingDetail = Mentor.Global.MEETING_DETAIL;
                                if (MeetingDetail == null) {
                                    var popup = Mentor.app.application.getController('AppDetail').addHelpPopup('new_meeting');
                                    Ext.Viewport.add(popup);
                                    popup.show();
                                } else {
                                    var popup = Mentor.app.application.getController('AppDetail').addHelpPopup('update_meeting');
                                    Ext.Viewport.add(popup);
                                    popup.show();
                                }
                            }
                        }
                    },
                     {
                        iconCls: 'btnBackCLS',
                        action: '',
                        docked: 'right',
                        style: "visibility:hidden"
                    },
                    {
                        iconCls: 'btnBackCLS',
                        docked: 'left',
                        action: 'btnBackEnterpreneur'

                    },
                    {
                        iconCls: 'goToNotesIcon',
                        docked: 'left',
                        id: 'addnotes',
                        action: 'addnotes'
                    }
                ]
            },
            {
                xtype: 'textfield',
                id: 'FollowUpMeetingID',
                hidden: true,
                margin : '0 20'
            },
            {
                xtype: 'label',
                id: 'idUserNameEnterpreneur',
                style: 'text-align: right;padding-right: 10px;padding: 10px;background-color: #C2C2C2;',
                hidden: true,
                margin : '0 20'
            },
            {
                xtype: 'label',
                id: 'idVersionEnterpreneur',
                style: 'text-align: right;padding-right: 10px;padding: 10px;',
                docked: 'bottom',
                hidden: true,
                margin : '0 20'
            },
            {
                xtype: 'fieldset',
                title: 'Select Entrepreneur',
                id: 'idLabelSelectMenteeEnterpreneurScreen',
                cls: 'fieldSetCLS',
                labelCls: 'fieldSetLabelCLS',
                margin : '0 20',
                hidden : true,
                items: [
                    {
                        xtype: 'selectfield',
                        id: 'idEnterpreneurSelectField',
                        store: 'Mentee',
                        displayField: 'MenteeName',
                        valueField: 'MenteeID',
                        inputCls: 'inputDisableCLS',
                        listeners: {
                            change: function (field, value) {
                                if (value instanceof Ext.data.Model) {
                                    value = value.get(field.getValueField());

                                }
                                console.log(value);
                                var Store = Ext.getStore('Mentee');
                                var record = Store.findRecord('MenteeID', value);
                                if (record != null) {
                                    Ext.getCmp('idTopicsTimingScreen').setValue(record.data.TopicID);
                                    Ext.getCmp('idMenteeCommentEnterpreneurScreen').setValue(record.data.MenteeComments);
                                } else {
                                    Ext.getCmp('idMenteeCommentEnterpreneurScreen').setValue("");
                                }
                            }
                        }
                    }
                ]
            },
            {
                xtype: 'label',
                id: 'idLabelMenteeNameEnterpreneurScreen',
                //style: 'text-align: right;padding-right: 10px;padding: 10px;',
                html : 'Metting with Mentee',
                margin : '1em 20px 0 20px'
            },
            {
                xtype: 'fieldset',
                title: 'Meeting Type',
                id: 'idLabelSelectMeetingTypeEnterpreneurScreen',
                margin : '0 20',
                items: [
                    {
                        xtype: 'selectfield',
                        id: 'idMettingTypeSelectField',
                        store: 'MeetingType',
                        displayField: 'MeetingTypeName',
                        valueField: 'MeetingTypeID',
                        listeners: {
                            change: function (field, value) {
                                if (value instanceof Ext.data.Model) {
                                    value = value.get(field.getValueField());
                                }
                                console.log(value);
                                var Store = Ext.getStore('MeetingType');
                                var record = Store.findRecord('MeetingTypeID', value);
                                if (record.data.MeetingTypeID == "") {
                                    Ext.getCmp('idMeetingPlaceSelectField').setDisabled(false);
                                    Ext.getCmp('idMeetingPlaceSelectField').removeCls('inputDisableCLS');
                                } else if (record.data.MeetingTypeName != "In Person") {
                                    Ext.getCmp('idMeetingPlaceSelectField').setDisabled(true);
                                    Ext.getCmp('idMeetingPlaceSelectField').setValue(3);
                                    Ext.getCmp('idMeetingPlaceSelectField').addCls('inputDisableCLS');
                                } else {
                                    Ext.getCmp('idMeetingPlaceSelectField').setDisabled(false);
                                    Ext.getCmp('idMeetingPlaceSelectField').removeCls('inputDisableCLS');
                                }
                                //Pass value parameter to the 2nd select field's store
                            }
                        }

                    }
                ]

            },
            {
                xtype: 'fieldset',
                title: 'Meeting Place',
                id: 'idLabelSelectMeetingPlaceEnterpreneurScreen',
                margin : '0 20',
                items: [
                    {
                        xtype: 'selectfield',
                        id: 'idMeetingPlaceSelectField',
                        store: 'MeetingPlace',
                        displayField: 'MeetingPlaceName',
                        valueField: 'MeetingPlaceID'
                    }
                ]
            },
            //Timing Screen
            {
                xtype: 'fieldset',
                title: 'Select Main Topic',
                id: 'idLabelSelectMainTopicEnterpreneurScreen',
                margin : '0 20',
                items: [
                    {
                        xtype: 'selectfield',
                        id: 'idTopicsTimingScreen',
                        store: 'Topics',
                        displayField: 'TopicDescription',
                        inputCls: 'inputDisableCLS',
                        valueField: 'TopicID',
                        disabled: true
                    }
                ]
            },
            {
                xtype: 'fieldset',
                title: 'Mentee Comment',
                id: 'idLabelMenteeCommentEnterpreneurScreen',
                margin : '0 20',
                items: [
                    {
                        xtype: 'textareafield',
                        //name: 'mainIssueDiscussed',
                        id: 'idMenteeCommentEnterpreneurScreen',
                        disabled: true,
                        inputCls: 'inputDisableCLS',
                        maxRows: 2,
                        flex: 3
                    }
                ]
            },
            {
                xtype: 'panel',
                layout :{ type: 'hbox',},
                margin : '0 10',
                items: [
                    {
                        /*handler : function(){
                         Mentor.app.application.getController('Main').doStartSession();
                         }*/
                        xtype: 'button',
                        text: 'START SESSION',
                        //cls: 'startSession-btn-cls',
                        cls : 'signin-btn-cls',
                        id: 'startSession',
                        style: 'margin-right: 0.5em; margin-left: 0.5em;',
                        flex: 1,
                        action: 'doStartSession',
                        pressedCls : 'press-btn-cls',
                    },
                    {
                        xtype: 'fieldset',
                        title: 'Meeting Start Time',
                        id: 'startMeetingLabelEditMettingDetail',
                        margin: '-20 10 0 10',
                        items: [
                            {
                                xtype: 'textfield',
                                disabled: false,
                                id: 'startSessionEditMettingDetail',
                                //style : 'margin-top: 10px;margin-right: 0.5em; margin-left: 0.5em;',
                                cls: 'textFieldDisableCLS',
                                hidden: true,
                                flex: 1,
                                listeners: {
                                    keyup: function (form, e) {
                                        Mentor.app.application.getController('Main').calculateElapsedTime();
                                    }
                                }

                            }
                        ]
                    },
                    {
                        /*handler : function(){
                         Mentor.app.application.getController('Main').doEndSession();
                         }*/
                        xtype: 'button',
                        text: 'END SESSION',
                        //cls: 'endSession-btn-cls',
                        cls : 'grey-btn-cls',
                        id: 'endSession',
                        style: 'margin-right: 0.5em; margin-left: 0.5em;',
                        flex: 1,
                        action: 'doEndSession',
                        pressedCls:'press-grey_btn-cls'
                    },
                    {
                        xtype: 'fieldset',
                        title: 'Meeting End Time',
                        id: 'endMeetingLabelEditMettingDetail',
                        margin: '0 10 0 10',
                        items: [
                            {
                                xtype: 'textfield',
                                disabled: false,
                                id: 'endSessionEditMettingDetail',
                                //style : 'margin-top: 10px;margin-right: 0.5em; margin-left: 0.5em;',
                                cls: 'textFieldDisableCLS',
                                hidden: true,
                                flex: 1,
                                listeners: {
                                    keyup: function (form, e) {
                                        Mentor.app.application.getController('Main').calculateElapsedTime();
                                    }
                                }
                            }
                        ]
                    }
                ]
            },
            {
                xtype: 'fieldset',
                title: 'Elapsed Time (Rounded up to the next minute)',
                style: 'margin-top: 20px;font-size: 14px;',
                hidden: true,
                margin : '0 20',
                items: [
                    {
                        xtype: 'textfield',
                        id: 'MeetingElapsedTime',
                        placeHolder: 'Session Length',
                        disabled: true,
                        hidden: true
                    }
                ]
            },
            {
                xtype: 'container',
                layout: {
                    type: 'vbox',
                    pack: 'center',
                    align: 'center'
                },
                margin : '0 20',
                items: [
                    {
                        xtype: 'label',
                        //html: 'Elapsed Time (Rounded up to the next minute)',
                        html : 'Elapsed Time :',
                        style: 'margin-top: 20px;font-size: 16px;'
                    },
                    {
                        //flex: 5,
                        items: [
                            {
                                xtype: 'container',
                                id: 'stopWatchPanel',
                                cls: 'stopWatchPanel',
                                html: '00:00',
                                hidden: true
                            },
                            {
                                xtype: 'container',
                                flex: 1,
                                layout: 'hbox',
                                defaults: {
                                    xtype: 'label',
                                    //ui: 'round',
                                    action: 'stopWatchButton',
                                    cls: 'stopWatchButton'
                                },
                                items: [
                                    {
                                        xtype: 'panel',
                                        pack: 'center',
                                        layout: {
                                            type: 'vbox',
                                            pack: 'center',
                                            xtype: 'label'
                                        },
                                        items: [
                                            {
                                                html: '00',
                                                id: 'hrs',
                                                cls: 'timeBlock',
                                                hidden: false
                                            },
                                            {
                                                xtype: 'numberfield',
                                                id: 'idHrsEdited',
                                                clearIcon: false,
                                                inputCls: 'timeBlockInputCLSEditable',
                                                cls: 'timeBlockEditable',
                                                minValue: 00,
                                                maxValue: 23,
                                                maxLength: 2,
                                                hidden: true,
                                                listeners: {
                                                    keyup: function (form, e) {
                                                        Mentor.app.application.getController('Main').calculateEditedElapsedTime(form, e);
                                                    }
                                                }
                                            },
                                            {
                                                html: 'Hours',
                                                cls: 'labeltime'
                                            }
                                        ]
                                    },
                                    {
                                        xtype: 'panel',
                                        pack: 'center',
                                        layout: {
                                            type: 'vbox',
                                            pack: 'center',
                                            xtype: 'label'
                                        },
                                        items: [
                                            {
                                                html: ':',
                                                id: 'seperator',
                                                cls: 'timeBlock'
                                            }
                                        ]
                                    },
                                    {
                                        xtype: 'panel',
                                        pack: 'center',
                                        layout: {
                                            type: 'vbox',
                                            pack: 'center',
                                            xtype: 'label'
                                        },
                                        items: [
                                            {
                                                html: '00',
                                                id: 'min',
                                                cls: 'timeBlock',
                                                hidden: false
                                            },
                                            {
                                                xtype: 'numberfield',
                                                id: 'idMinEdited',
                                                clearIcon: false,
                                                inputCls: 'timeBlockInputCLSEditable',
                                                cls: 'timeBlockEditable',
                                                minValue: 00,
                                                maxValue: 59,
                                                maxLength: 2,
                                                hidden: true,
                                                listeners: {
                                                    keyup: function (form, e) {
                                                        Mentor.app.application.getController('Main').calculateEditedElapsedTime(form, e);
                                                    }
                                                }
                                            },
                                            {
                                                html: 'Minutes',
                                                cls: 'labeltime'
                                            }
                                        ]
                                    }
                                ]
                            }
                        ]
                    }
                ]
            },
            {
                xtype: 'panel',
                layout: {
                    type: 'vbox'
                },
                margin : '0 80 10 80',
                items: [
                    {
                        xtype: 'container',
                        layout: {
                            type: 'hbox'
                        },
                        items: [
                            {
                                xtype: 'button',
                                text: 'NEXT',
                                cls: 'signin-btn-cls',
                                action: "btnNextEnterpreneur",
                                pressedCls : 'press-btn-cls',
                                flex: 1,
                                //width : '45%',
                                id: 'idBtnNextEnterpreneur'
                            },
                            {
                                //width : '45%'
                                xtype: 'button',
                                text: 'UPDATE',
                                cls: 'signin-btn-cls',
                                flex: 1,
                                hidden: true,
                                id: 'idBtnUpdateEnterpreneur',
                                action: 'btnUpdateEnterpreneur' //UpdateMeetingDetail Controller
                            }
                        ]
                    }
                ]
            }
        ],
        listeners: [
            {
                event: "activate",
                fn: "onPageActivate"
            },
            {
                event: "painted",
                fn: "onPagePainted"
            },
            {
                event: "deactivate",
                fn: "onPageDeactivate"
            }
        ]
    },
    onPageDeactivate: function () {

    },
    onPageActivate: function () {
        console.log("asaasdsadasf");

        Ext.getCmp('startSession').setText('START SESSION');
        Ext.getCmp('startSession').setDisabled(false);
        Ext.getCmp('endSession').setText('END SESSION');
        Ext.getCmp('endSession').setDisabled(true);
        //Ext.getCmp('idBtnNextEnterpreneur').setDisabled(true)
        Ext.getCmp('endSessionEditMettingDetail').removeCls("invalidDate");
        Ext.getCmp('idBtnUpdateEnterpreneur').setDisabled(false);

        var MentorLoginDetail = Ext.decode(localStorage.getItem("idMentorLoginDetail"));

        var MeetingDetail = Mentor.Global.MEETING_DETAIL; // From Meeting History

        Ext.getCmp('idEnterpreneurSelectField').getStore().load();
        var MeetingTypeStore = Ext.getStore('MeetingType').load();
        var MeenteeStore = Ext.getStore('Mentee').load();
        var MeetingPlaceStore = Ext.getStore('MeetingPlace').load();

        Ext.getCmp('idUserNameEnterpreneur').setHtml(MentorLoginDetail.MentorName);
        Ext.getCmp('idVersionEnterpreneur').setHtml(Mentor.Global.VERSION_NAME);


        /*** Timing Screen***/
        var TopicsStore = Ext.getStore('Topics').load();
        var TopicsStore = Ext.getStore('Topics').load();

        var MeetingDetail = Mentor.Global.MEETING_DETAIL; // From Meeting History
        var TopicsSelectField = Ext.getCmp('idTopicsTimingScreen');

        Ext.getCmp('MeetingElapsedTime').setValue('');

        var MentorLoginDetail = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
        Ext.getCmp('idUserNameEnterpreneur').setHtml(MentorLoginDetail.MentorName);

        if (MeetingDetail != null) {
            Ext.getCmp('idTitleEntrepreneur').setTitle('Update Meeting');
            Ext.getCmp('idTopicsTimingScreen').setValue(MeetingDetail.MeetingTopicID);
            Ext.getCmp('MeetingElapsedTime').setValue(MeetingDetail.MeetingElapsedTime);

            Ext.getCmp('startSessionEditMettingDetail').setHidden(false);
            Ext.getCmp('endSessionEditMettingDetail').setHidden(false);
            //Ext.getCmp('MeetingElapsedTime').setDisabled(false);

            Ext.getCmp('startSessionEditMettingDetail').setValue(MeetingDetail.MeetingStartDatetime);
            Ext.getCmp('endSessionEditMettingDetail').setValue(MeetingDetail.MeetingEndDatetime);


            //Ext.getCmp('startSession').setText(MeetingDetail.MeetingStartDatetime);
            //Ext.getCmp('endSession').setText(MeetingDetail.MeetingEndDatetime);
            Ext.getCmp('startSession').setHidden(true);
            Ext.getCmp('endSession').setHidden(true);

            var EditedHours = Ext.getCmp('idHrsEdited');
            EditedHours.setHidden(false);
            var EditedMinutes = Ext.getCmp('idMinEdited');
            EditedMinutes.setHidden(false);

            Ext.getCmp('hrs').setHidden(true);
            Ext.getCmp('min').setHidden(true);

            EditedHours.setValue(00);
            EditedMinutes.setValue(00);

            timer = "";

            //Ext.getCmp('idBtnUpdateTimingScreen').setHidden(false);
            //Ext.getCmp('idBtnNextEnterpreneur').setDisabled(false);


            var Hours = parseInt(MeetingDetail.MeetingElapsedTime / 60);
            var Minutes = MeetingDetail.MeetingElapsedTime % 60;
            if (Minutes < 9)
                EditedMinutes.setValue("0" + Minutes)//Ext.getCmp("min").setHtml("0"+Minutes);
            else
                EditedMinutes.setValue(Minutes)//Ext.getCmp("min").setHtml(Minutes);

            if (Hours < 9)
                EditedHours.setValue("0" + Hours);//Ext.getCmp("hrs").setHtml("0"+Hours);
            else
                EditedHours.setValue(Hours);//Ext.getCmp("hrs").setHtml(Hours);

            if (isNaN(Hours) || Hours < 0) {
                EditedHours.setValue(00);//Ext.getCmp("hrs").setHtml("00");

            }
            if (isNaN(Minutes) || Minutes < 0) {
                EditedMinutes.setValue(00);//Ext.getCmp("min").setHtml("00");

            }
            if (Hours == 0 && Minutes == 1 && MeetingDetail.MeetingElapsedTime == 1)
                EditedMinutes.setValue(01);//Ext.getCmp("min").setHtml("01"); // Set Min to 1 if start and End date are same
            /*else
             Mentor.app.application.getController('Main').calculateElapsedTime();*/


            // Second if else START
            Ext.getStore('Mentee').removeAll();
            var MenteeStore = Ext.getStore('Mentee');
            MenteeStore.insert(0, {
                "MenteeName": MeetingDetail.MenteeName,
                "MenteeID": MeetingDetail.MenteeID,
                "TopicID": MeetingDetail.MeetingTopicID,
                "MenteeComments": MeetingDetail.MenteeInvitationComment
            });

            Ext.getCmp('idEnterpreneurSelectField').setDisabled(true);

            Ext.getCmp('idEnterpreneurSelectField').setValue(MeetingDetail.MenteeID);

            Ext.getCmp('idMettingTypeSelectField').setValue(MeetingDetail.MeetingTypeID);
            Ext.getCmp('idMeetingPlaceSelectField').setValue(MeetingDetail.MeetingPlaceID);
            Ext.getCmp('idBtnUpdateEnterpreneur').setHidden(false);

            // Second if else END

        } else {
            Ext.getCmp('idTitleEntrepreneur').setTitle('New Meeting');
            //Ext.getCmp('idBtnNextEnterpreneur').setDisabled(true);
            //
            /*timer = setInterval(function(){
             Mentor.app.application.getController('Main').calculateElapsedTime();
             console.log("a")
             },10000);*/

            Ext.getCmp('startSessionEditMettingDetail').setHidden(true);
            Ext.getCmp('endSessionEditMettingDetail').setHidden(true);
            Ext.getCmp('startSession').setHidden(false);
            Ext.getCmp('endSession').setHidden(false);

            Ext.getCmp('idHrsEdited').setHidden(true);
            Ext.getCmp('idMinEdited').setHidden(true);
            Ext.getCmp('hrs').setHidden(false);
            Ext.getCmp('min').setHidden(false);

            // Second if else START
            Ext.getCmp('idBtnUpdateEnterpreneur').setHidden(true);
            Ext.getCmp('idEnterpreneurSelectField').setDisabled(true);//Ext.getCmp('idEnterpreneurSelectField').setDisabled(false);
            Ext.getCmp("min").setHtml("00");
            Ext.getCmp("hrs").setHtml("00");
            // Second if else END

            var existingMeetingTimer = Ext.decode(localStorage.getItem('SaveMeetingStartDateTime'));
            if (existingMeetingTimer != null && existingMeetingTimer.StartTime != null && existingMeetingTimer.StartTime != '') {
                var StartTime = existingMeetingTimer.StartTime;
                Ext.getCmp('startSession').setText(StartTime).setDisabled(true);
                Ext.getCmp('endSession').setDisabled(false);
                clearInterval(Mentor.Global.Timer);
                Mentor.app.application.getController('Main').calculateElapsedTime();
                Mentor.Global.Timer = setInterval(function () {
                    Mentor.app.application.getController('Main').calculateElapsedTime();
                    console.log("a")
                }, 60000);
            }
        }
        /*** End ***/

        //Change the lable of Mentor and Mentor where Name Exits according to Application Configuration
        Ext.getCmp('idLabelSelectMenteeEnterpreneurScreen').setTitle('Select ' + Mentor.Global.MENTEE_NAME);
        Ext.getCmp('idLabelMenteeCommentEnterpreneurScreen').setTitle(Mentor.Global.MENTEE_NAME + ' Comment');

    },
    onPagePainted: function () {
        var MeetingDetail = Mentor.Global.MEETING_DETAIL; // From Meeting History
        if (MeetingDetail != null) {
            Ext.getCmp('startMeetingLabelEditMettingDetail').setHidden(false);
            Ext.getCmp('endMeetingLabelEditMettingDetail').setHidden(false);

        } else {
            Ext.getCmp('startMeetingLabelEditMettingDetail').setHidden(true);
            Ext.getCmp('endMeetingLabelEditMettingDetail').setHidden(true);
        }

    }
});
