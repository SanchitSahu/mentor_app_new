Ext.define('Mentor.view.AcceptResponses', {
    extend: 'Ext.Panel',
    xtype: 'acceptresponses',
    requires: [
    ],
    config: {
        cls: 'forgotpassword',
        scrollable: {
            direction: 'vertical',
            indicators: {
                y: {
                    autoHide: false
                }
            }
        },
        defaults: {
            xtype: 'formpanel'
        },
        items: [
            {
                docked: 'top',
                xtype: 'toolbar',
                title: 'Pre-Meeting Prep',
                cls: 'my-titlebar',
                defaults: {
                    iconMask: true
                },
                items: [
                    {
                        iconCls: 'btnBackCLS',
                        action: 'btnBackAcceptDeclineResponses',
                        docked: 'left'
                    },
                    {
                        iconCls: 'goToNotesIcon',
                        docked: 'left',
                        //style: 'visibility:hidden'
                        id: 'addnotes',
                        action: 'addnotes'
                    },
//                    {
//                        iconCls: 'addIcon',
//                        docked: 'left',
//                        listeners: {
//                            element: 'element',
//                            tap: function () {
//                                var popup = Mentor.app.application.getController('AppDetail').addNotePopup();
//                                Ext.Viewport.add(popup);
//                                popup.show();
//                            }
//                        }
//                    },
                    {
                        iconCls: 'helpIcon',
                        docked: 'right',
                        listeners: {
                            element: 'element',
                            tap: function () {
                                var popup = Mentor.app.application.getController('AppDetail').addHelpPopup('pre-meeting-prep');
                                Ext.Viewport.add(popup);
                                popup.show();
                            }
                        }
                    },
                    {
                        iconCls: 'refreshIcon',
                        docked: 'right',
                        handler: function () {
                            Mentor.app.application.getController('Main').getAcceptResponses(true);
                        }
                    }
                ]
            },
            {
                xtype: 'label',
                id : 'idLabelAcceptRejectPendingRequest',
                html: 'Preparations<span class="spanAsterisk">*</span>',
                style: 'padding-left:20px;color: #000000;border-bottom: 1px solid #bfbfbf;padding-bottom:10px;',
                //style: 'font-weight: bold;',
                margin: '15 0 0 0'
            },
            {
                xtype: 'panel',
                margin: '0 0 0 0',
                items: [
                    {
                        xtype: 'panel',
                        id: 'idMentorAcceptResponses',
                        //margin: '0 0 0 10'
                    }
                ]
            },
            {
                xtype: 'panel',
                flex: 1,
                //style: 'margin-top:10px;',
                margin : '10 10 10 10',
                hidden : true,
                items: [
                    {
                        xtype: 'fieldset',
                        id: 'idMentorCommetnsFieldSetAcceptRejectPendingRequest',
                        title: Mentor.Global.MENTOR_NAME + " Comments",
                        items: [
                            {
                                xtype: 'textareafield',
                                id: 'commentsAcceptPendingRequest',
                                placeHolder: Mentor.Global.MENTOR_NAME + ' Comments',
                                inputCls: 'inputCLS',
                                clearIcon: false
                            }
                        ]
                    }
                ]
            },
            {
                xtype: 'container',
                layout: {
                    type: 'hbox',
                    pack: 'center'
                },
                margin: '10 20 10 20',
                items: [
                    {
                        xtype: 'button',
                        text: 'ACCEPT',
                        cls: 'signin-btn-cls',
                        pressedCls : 'press-btn-cls',
                        id: 'idDoSubmitAcceptRequest',
                        action: 'doSubmitAcceptRequest',
                        flex: 1,
                        margin: '0 5 0 0',
                        hidden: true
                    },
                    {
                        xtype: 'button',
                        text: 'SEND INVITE',
                        cls: 'signin-btn-cls',
                        pressedCls : 'press-btn-cls',
                        id: 'idDoSubmitIssueInviteRequest',
                        action: 'doSubmitIssueInviteRequest',
                        flex: 1,
                        margin: '0 5 0 0',
                        hidden: true
                    },
                    {
                        xtype: 'button',
                        text: 'CANCEL',
                        pressedCls : 'press-grey_btn-cls',
                        cls: 'grey-btn-cls',
                        action: 'btnBackAcceptDeclineResponses',
                        flex: 1,
                        margin: '0 0 0 5'
                    }
                ]
            }
        ],
        listeners: [
            {
                event: "activate",
                fn: "onPageActivate"
            },
            {
                event: "painted",
                fn: "onPagePainted"
            },
            {
                event: "deactivate",
                fn: "onPageDeactivate"
            }
        ]
    },
    onPageDeactivate: function () {
        var MentorAcceptResponsesID;
        var MentorAcceptResponsesName;
        var isChecked = 0;
        var data = null;
        var MentorAcceptResponses = Ext.getCmp('idMentorAcceptResponses');
        for (i = 0; i < MentorAcceptResponses.getItems().length; i++) {
            if (MentorAcceptResponses.getItems().getAt(i).isChecked()) {
                if (isChecked == 0) {
                    MentorAcceptResponsesID = MentorAcceptResponses.getItems().getAt(i).getValue();
                    MentorAcceptResponsesName = MentorAcceptResponses.getItems().getAt(i).getItemId();
                    isChecked = isChecked + 1;
                } else {
                    MentorAcceptResponsesID = MentorAcceptResponsesID + "," + MentorAcceptResponses.getItems().getAt(i).getValue();
                    MentorAcceptResponsesName = MentorAcceptResponsesName + ";" + MentorAcceptResponses.getItems().getAt(i).getItemId();
                    isChecked = isChecked + 1;
                }
            }
        }
        if (isChecked != 0) {
            data = {
                MentorAcceptResponsesID: MentorAcceptResponsesID,
                MentorAcceptResponsesName: MentorAcceptResponsesName
            };
            data = Ext.encode(data);
        }
        localStorage.setItem('CacheAcceptResponse', data);
    },
    onPageActivate: function () {
        var CachedAcceptResponse = Ext.decode(localStorage.getItem('CacheAcceptResponse'));
        localStorage.setItem('CacheAcceptResponse', null);

        var MentorAcceptResponsesStore = Ext.getStore('AcceptResponses');
        var MentorAcceptResponses = Ext.getCmp('idMentorAcceptResponses');
        MentorAcceptResponses.removeAll();
        for (i = 0; i < MentorAcceptResponsesStore.data.length; i++) {
            var checked = false;
            if (CachedAcceptResponse != null) {
                var SelectedAcceptResponse = CachedAcceptResponse;
                if (SelectedAcceptResponse != "" && SelectedAcceptResponse.MentorAcceptResponsesName != null && SelectedAcceptResponse.MentorAcceptResponsesName != '') {
                    SelectedAcceptResponse = SelectedAcceptResponse.MentorAcceptResponsesName.split(";");
                    if (SelectedAcceptResponse.indexOf(MentorAcceptResponsesStore.data.getAt(i).data.ResponseName) != -1) {
                        checked = true;
                    }
                }
            }
            var checkBoxField = Ext.create('Ext.field.Checkbox', {
                name: 'recorded_stream',
                value: MentorAcceptResponsesStore.data.getAt(i).data.ResponseID,
                itemId: MentorAcceptResponsesStore.data.getAt(i).data.ResponseName,
                label: MentorAcceptResponsesStore.data.getAt(i).data.ResponseName,
                labelCls: 'radio-btn-cls',
                checked: checked,
                labelWidth: '90%',
                labelAlign: 'right',
                cls : 'checkboxitem-cls',
                style : 'background: transparent;padding-left:20px;padding-right:20px;'
            });
            MentorAcceptResponses.add(checkBoxField);
        }

        //Change the lable of Mentor and Mentor where Name Exits according to Application Configuration
        Ext.getCmp('commentsAcceptPendingRequest').setValue('');
        Ext.getCmp('commentsAcceptPendingRequest').setPlaceHolder(Mentor.Global.MENTOR_NAME + ' Comments');
        Ext.getCmp('idMentorCommetnsFieldSetAcceptRejectPendingRequest').setTitle(Mentor.Global.MENTOR_NAME + ' Comments');
        Ext.getCmp('idLabelAcceptRejectPendingRequest').setHtml(Mentor.Global.MENTEE_NAME + ' Preparations');
    },
    onPagePainted: function () {
        clearInterval(Mentor.Global.SET_INTERVAL_ID);
        Mentor.Global.SET_INTERVAL_ID = 0;
    }
});