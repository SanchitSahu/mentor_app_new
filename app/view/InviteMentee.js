Ext.define('Mentor.view.InviteMentee', {
    extend: 'Ext.Panel',
    xtype: 'invitementee',
    requires: [],
    config: {
        cls: 'meetinghistory',
        scrollable: {
            direction: 'vertical',
            indicators: {
                y: {
                    autoHide: false
                }
            }
        },
        defaults: {
            xtype: 'container',
           
        },
        items: [
            {
                docked: 'top',
                xtype: 'toolbar',
                title: 'Invite Screen',
                cls: 'my-titlebar',
                hidden: true,
                defaults: {
                    iconMask: true
                },
                items: [
                    {
                        iconCls: 'arrow_left',
                        action: 'btnBackInviteBack',
                        docked: 'left'
                    },
                    {
                        iconCls: 'arrow_right',
                        action: 'btnGotoWaitScreenInviteMenteeScreen',
                        docked: 'right'
                    }
                ]
            },
            {
                xtype: 'label',
                id: 'idUserName',
                style: 'text-align: right;padding-right: 10px;padding: 10px;background-color: #C2C2C2;',
                docked: 'top',
                hidden: true
            },
            {
                xtype: 'label',
                id: 'idVersionInviteMentor',
                style: 'text-align: right;padding-right: 10px;padding: 10px;',
                docked: 'bottom',
                hidden: true
            },
            {
                xtype: 'label',
                html: 'All mentees assigned to you are unavailable at this time. Please try again later.',
                id: 'idNoMenteeAvailable',
                style: 'text-align: center;font-size: 17px;padding-right: 10px;padding: 10px;',
                //docked: 'bottom',
                hidden: true
            },
            {
                xtype: 'fieldset',
                /*title: 'Select Available Mentee',
                id: 'idLabelSelectMentorInviteMentee',*/
                margin : '20 20 20 20',
                items: [
                    {
                        xtype: 'selectfield',
                        id: 'idMenteeSelectFieldInviteMenteeScreen',
                        store: 'InviteMentee',
                        displayField: 'MenteeName',
                        placeHolder: '-- Select Mentee --',
                        valueField: 'MenteeID',
                        style : 'border-radius: 0px;',
                        listeners: {
                            change: function (field, value) {
                                if (value instanceof Ext.data.Model) {
                                    value = value.get(field.getValueField());
                                }
                                var Store = Ext.getStore('InviteMentee');
                                var record = Store.findRecord('MenteeID', value);
                                if (record != null) {
                                    if (record.data.rStatus == "TRUE") {
                                        Ext.getCmp('idSendBtnInviteMenteeScreen').setDisabled(true);
                                        Ext.getCmp('idSendBtnInviteMenteeScreen').setText("INVITATION STILL PENDING");
                                        Ext.getCmp('idTopicsSelectFieldInviteMenteeScreen').setDisabled(true);
                                        Ext.getCmp('mentorCommentInviteMenteeScreen').setDisabled(true);
                                        Ext.getCmp('idCancelBtnInviteMenteeScreen').setHidden(true);
                                    } else {
                                        Ext.getCmp('idSendBtnInviteMenteeScreen').setDisabled(false);
                                        Ext.getCmp('idSendBtnInviteMenteeScreen').setText("NEXT");
                                        Ext.getCmp('idTopicsSelectFieldInviteMenteeScreen').setDisabled(false);
                                        Ext.getCmp('mentorCommentInviteMenteeScreen').setDisabled(false);
                                    }
                                }
                            }
                        }
                    }
                ]
            },
            {
                xtype: 'fieldset',
                /*title: 'Select Main Topic <span class="spanAsterisk">*</span>',*/ 
                margin: '0 20 0 20',
                items: [
                    {
                        xtype: 'selectfield',
                        id: 'idTopicsSelectFieldInviteMenteeScreen',
                        store: 'Topics',
                        displayField: 'TopicDescription',
                        valueField: 'TopicID'
                    }
                ]
            },
            {
                xtype: 'label',
                html: Mentor.Global.MENTEE_NAME + " Email",
                style: 'font-weight: bold;',
                margin: '20 10 0 20',
                hidden: true
            },
            {
                xtype: 'textfield',
                id: 'idMentorEmailInviteMenteeScreen',
                margin: '0 10 0 10',
                cls: 'textFieldDisableCLS',
                inputCls: 'inputCLS',
                clearIcon: false,
                hidden: true
            },
            {
                xtype: 'label',
                html: Mentor.Global.MENTOR_NAME + " Phone No",
                style: 'font-weight: bold;',
                margin: '20 10 0 20',
                hidden: true
            },
            {
                xtype: 'textfield',
                id: 'idMentorPhoneNoInviteMenteeScreen',
                margin: '0 10 0 10',
                cls: 'textFieldDisableCLS',
                inputCls: 'inputCLS',
                clearIcon: false,
                hidden: true
            },
            {
                xtype: 'panel',
                margin: '10 0 10 10',
                layout: {
                    type: 'hbox'
                },
                hidden: true,
                items: [
                    {
                        xtype: 'checkboxfield',
                        id: 'idIsPhoneSMSCapableInviteMenteeScreen',
                        label: "Is above number Text Message(aka SMS) capable ?",
                        labelCls: 'radio-btn-cls',
                        labelWidth: '90%',
                        labelAlign: 'right',
                        hidden: true
                    }
                ]
            },
            {
                xtype: 'panel',
                layout: {
                    type: 'hbox'
                },
                /*style: 'margin-top:-20px;margin-left:-20px,margin-right:-20px',*/
                margin: '10 10 -10 10',
                items: [
                    {
                        xtype: 'fieldset',
                        /*title: "Meeting Date <span class='spanAsterisk'>*</span>",*/
                        flex: 1,
                        items: [
                            {
                                xtype: 'textfield',
                                disabled: false,
                                readOnly : true,
                                disabledCls: 'textFieldDisableAcceptAndRejectMenteeRequestCLS',
                                inputCls: 'textFieldDateTimeAcceptAndRejectMenteeRequestCLS',
                                id: 'dateTimeIssueInviteMentee',
                                placeHolder : 'Meeting Date',
                                listeners: {
                                    element: 'element',
                                    tap: function () {
                                        var input = $('#timeIssueInviteMentee');
                                        input.clockpicker('hide');
                                        var popup = new Ext.Panel({
                                            floating: true,
                                            centered: true,
                                            modal: true,
                                            hideOnMaskTap: true,
                                            autoDestroy: true,
                                            width: 310,
                                            height: 410,
                                            items: [
                                                {
                                                    xtype: 'popview',
                                                    id: 'idCalenderViewIssueInviteMentee',
                                                    listeners: {
                                                        selectionchange: function (a, newDate, oldDate, eOpts)
                                                        {
                                                            console.log(newDate);
                                                            var MeetingTime = '';
                                                            var currentDate = '';
                                                            var selectedDate = '';
                                                            var Year = '';
                                                            var Month = '';
                                                            var date = '';
                                                            var MeetingDate = '';
                                                            var SelectedMeetingDate = '';

                                                            MeetingTime = Ext.getCmp('timeIssueInviteMentee').getValue();
                                                            currentDate = new Date();
                                                            currentDate.setHours(0, 0, 0, 0);
                                                            selectedDate = new Date(newDate);
                                                            selectedDate.setHours(0, 0, 0, 0);
                                                            if (currentDate > selectedDate) {
                                                                Ext.Msg.alert("MELS", "Please select future date");
                                                                popup.hide();
                                                                Ext.Viewport.remove(popup);
                                                            } else {
                                                                Year = newDate.getFullYear();
                                                                Month = (newDate.getMonth() + 1);
                                                                if (Month < 10) {
                                                                    Month = "0" + Month;
                                                                }
                                                                date = newDate.getDate();
                                                                if (date < 10) {
                                                                    date = "0" + date;
                                                                }
                                                                SelectedMeetingDate = Year + "-" + Month + "-" + date;
                                                                if (MeetingTime != "") {
                                                                    MeetingDate = SelectedMeetingDate;
                                                                    MeetingDate = Date.parse(MeetingDate + " " + MeetingTime);
                                                                    if (new Date() > MeetingDate) {
                                                                        Ext.Msg.alert("MELS", "Please select future date.");
                                                                    } else {
                                                                        Mentor.Global.Current_date = new Date(MeetingDate);
                                                                        Ext.getCmp('dateTimeIssueInviteMentee').setValue(SelectedMeetingDate);
                                                                    }
                                                                    popup.hide();
                                                                    Ext.Viewport.remove(popup);
                                                                } else {
                                                                    Ext.getCmp('dateTimeIssueInviteMentee').setValue(SelectedMeetingDate);
                                                                    Mentor.Global.Current_date = Date.parse(SelectedMeetingDate + ' 00:00:01');
                                                                    popup.hide();
                                                                    Ext.Viewport.remove(popup);
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            ],
                                            listeners: {
                                                hide: function (a, b) {
                                                    popup.hide();
                                                    Ext.Viewport.remove(popup);
                                                }
                                            }
                                        });
                                        Ext.Viewport.add(popup);
                                        Ext.getCmp('calendarViewID').setViewConfig({value: new Date()});
                                        Ext.getCmp('calendarViewID').setActiveItem(2);
                                        Ext.getCmp('calendarViewID').setActiveItem(2);
                                        Ext.getCmp('calendarViewID').setActiveItem(-2);
                                        Ext.getCmp('calendarViewID').setActiveItem(-2);
                                        popup.show();
                                    }
                                }
                            }
                        ]
                    },
                    {
                        xtype: 'fieldset',
                        /*title: "Meeting Time <span class='spanAsterisk'>*</span>",*/
                        flex: 1,
                        items: [
                            {
                                xtype: 'datetimepickerfield',
                                name: '24hrdt',
                                id: 'timePickerIssueInviteMentee',
                                hidden: true,
                                dateTimeFormat: 'H:i',
                                picker: {
                                    yearFrom: new Date().getFullYear(),
                                    yearTo: new Date(new Date().setYear((new Date().getFullYear() + 1))).getFullYear(),
                                    minuteInterval: 1,
                                    ampm: false,
                                    slotOrder: ['hour', 'minute']
                                }
                            },
                            {
                                xtype: 'textfield',
                                disabled: false,
                                disabledCls: 'textFieldDisableAcceptAndRejectMenteeRequestCLS',
                                inputCls: 'textFieldDateTimeAcceptAndRejectMenteeRequestCLS',
                                id: 'timeIssueInviteMentee',
                                readOnly: true,
                                placeHolder : 'Meeting Time',
                                listeners: {
                                    element: 'element',
                                    painted: function () {
                                        var input = $('#timeIssueInviteMentee');
                                        input.clockpicker({
                                            align: 'right',
                                            beforeDone: function () {
                                                Ext.getCmp("TimePickerIssueInviteMenteeOldValue").setValue(Ext.getCmp('timeIssueInviteMentee').getValue());
                                            },
                                            afterDone: function () {
                                                console.log("a");
                                                var MeetingDate = Ext.getCmp('dateTimeIssueInviteMentee').getValue();
                                                var value = $('#timeIssueInviteMentee input').val();
                                                if (value.length > 6 || typeof value.length == "undefined") {
                                                    return;
                                                }
                                                if (MeetingDate != "") {
                                                    MeetingDate = Date.parse(MeetingDate + " " + value);
                                                    if (new Date() > MeetingDate) {
                                                        var old_value = Ext.getCmp('TimePickerIssueInviteMenteeOldValue').getValue();
                                                        Ext.getCmp('timeIssueInviteMentee').setValue(null);
                                                        Ext.Msg.alert("MELS", "Please select future time");
                                                    } else {
                                                        Ext.getCmp("TimePickerIssueInviteMenteeOldValue").setValue(this.value);
                                                        Ext.getCmp('timeIssueInviteMentee').setValue(value);
                                                    }
                                                } else {
                                                    Ext.getCmp('timeIssueInviteMentee').setValue(value);
                                                }
                                            }
                                        });
                                    },
                                    tap: function () {
                                        var input = $('#timeIssueInviteMentee');
                                        input.clockpicker({
                                            align: 'right',
                                            beforeDone: function () {
                                                Ext.getCmp("TimePickerIssueInviteMenteeOldValue").setValue(Ext.getCmp('timeIssueInviteMentee').getValue());
                                            },
                                            afterDone: function () {
                                                console.log("a");
                                                var MeetingDate = Ext.getCmp('dateTimeIssueInviteMentee').getValue();
                                                var value = $('#timeIssueInviteMentee input').val();
                                                if (value.length > 6 || typeof value.length == "undefined") {
                                                    return;
                                                }
                                                if (MeetingDate != "") {
                                                    MeetingDate = Date.parse(MeetingDate + " " + value);
                                                    if (new Date() > MeetingDate) {
                                                        var old_value = Ext.getCmp('TimePickerIssueInviteMenteeOldValue').getValue();
                                                        Ext.getCmp('timeIssueInviteMentee').setValue(null);
                                                        Ext.Msg.alert("MELS", "Please select future time");
                                                    } else {
                                                        Ext.getCmp("TimePickerIssueInviteMenteeOldValue").setValue(this.value);
                                                        Ext.getCmp('timeIssueInviteMentee').setValue(value);
                                                    }
                                                } else {
                                                    Ext.getCmp('timeIssueInviteMentee').setValue(value);
                                                }
                                            }
                                        });
                                    },
                                    blur: function () {
                                        var input = $('#timeIssueInviteMentee');
                                        input.clockpicker('hide');
                                    }
                                }
                            }
                        ]
                    }
                ]
            },
            {
                xtype: 'textfield',
                id: 'TimePickerIssueInviteMenteeOldValue',
                hidden: true
            },
            {
                xtype: 'fieldset',
                //title: "Meeting Type<span class='spanAsterisk'>*</span>",
                /*title: "Meeting Type  <span class='spanAsterisk'>*</span>",*/
                /*style: 'margin-top:-20px;',*/
                margin : '0 20',
                items: [
                    {
                        xtype: 'selectfield',
                        id: 'idMeetingTypeSelectFieldInviteMenteeScreen',
                        store: 'MeetingType',
                        displayField: 'MeetingTypeName',
                        valueField: 'MeetingTypeID',
                        listeners: {
                            change: function (field, value) {
                                if (value instanceof Ext.data.Model) {
                                    value = value.get(field.getValueField());
                                }
                                console.log(value);
                                var Store = Ext.getStore('MeetingType');
                                var record = Store.findRecord('MeetingTypeID', value);
                                if (record.data.MeetingTypeID == "") {
                                    Ext.getCmp('idMeetingPlaceSelectFieldInviteMenteeScreen').setDisabled(false);
                                    Ext.getCmp('idMeetingPlaceSelectFieldInviteMenteeScreen').removeCls('inputDisableCLS');
                                } else if (record.data.MeetingTypeName != "In Person") {
                                    Ext.getCmp('idMeetingPlaceSelectFieldInviteMenteeScreen').setDisabled(true);
                                    Ext.getCmp('idMeetingPlaceSelectFieldInviteMenteeScreen').setValue(3);
                                    Ext.getCmp('idMeetingPlaceSelectFieldInviteMenteeScreen').addCls('inputDisableCLS');
                                } else {
                                    Ext.getCmp('idMeetingPlaceSelectFieldInviteMenteeScreen').setDisabled(false);
                                    Ext.getCmp('idMeetingPlaceSelectFieldInviteMenteeScreen').removeCls('inputDisableCLS');
                                }
                            }
                        }
                    }
                ]
            },
            {
                xtype: 'fieldset',
                /*title: "Meeting Place<span class='spanAsterisk'>*</span>",*/
                //title: "Meeting Place",
                id: 'idLabelSelectMeetingPlaceInviteMenteeScreen',
                /*style: 'margin-top:-20px;',*/
                margin : '20 20 0 20',
                items: [
                    {
                        xtype: 'selectfield',
                        id: 'idMeetingPlaceSelectFieldInviteMenteeScreen',
                        store: 'MeetingPlace',
                        displayField: 'MeetingPlaceName',
                        valueField: 'MeetingPlaceID'
                    }
                ]
            },
            {
                xtype: 'label',
                html: 'Reason for Meeting Request <span class="spanAsterisk">*</span>',
                margin: '20 0 0 15',
                style: 'font-weight: bold;',
                hidden : true,
            },
            {
                xtype: 'textareafield',
                name: 'mentorComment',
                id: 'mentorCommentInviteMenteeScreen',
                cls: 'textFieldDisableCLS',
                maxRows: 2,
                disabled: false,
               
                inputCls: 'inputCLS',
                clearIcon: false,
                margin : '20',
                placeHolder : 'Reason for Meeting Request *'
            },
            {
                xtype: 'container',
                layout: {
                    type: 'hbox'
                },
                margin : '20 10',
                items: [
                    {
                        xtype: 'button',
                        //text: 'SEND INVITATION',
                        text: 'NEXT',
                        cls: 'signin-btn-cls',
                         pressedCls : 'press-btn-cls',
                        action: "btnSendInviteMenteeScreen",
                        id: 'idSendBtnInviteMenteeScreen',
                        flex: 1,
                        style : 'font-size:0.9em',
                    },
                    {
                        xtype: 'button',
                        text: 'CANCEL',
                        pressedCls : 'press-grey_btn-cls',
                        cls: 'grey-btn-cls',
                        pressedCls : 'press-btn-cls',
                        id: 'idCancelBtnInviteMenteeScreen',
                        flex: 1,
                        style : 'font-size:0.9em',
                        handler : function(){
                             Ext.getCmp('idPendingInvitesTabInvitesMentorBottomTabView').getTabBar().setActiveTab(0);
                        }
                    },

                ]
            }
        ],
        listeners: [
            {
                event: "activate",
                fn: "onPageActivate"
            },
            {
                event: "painted",
                fn: "onPagePainted"
            }
        ]
    },
    /*onPageActivate: function () {
        var TopicsStore = Ext.getStore('Topics').load();
        Ext.getStore('InviteMentee').load();
        Ext.getCmp('idVersionInviteMentor').setHtml(Mentor.Global.VERSION_NAME);

        //Change the lable of Mentor and Mentor where Name Exits according to Application Configuration
        //Ext.getCmp('idLabelSelectMentorInviteMentee').setTitle('Select Available ' + Mentor.Global.MENTEE_NAME + " <span class='spanAsterisk'>*</span>");
        Ext.getCmp('idMenteeSelectFieldInviteMenteeScreen').setPlaceHolder('Select Available ' + Mentor.Global.MENTEE_NAME);

    },*/
    onPageActivate: function () {

        //Avinash
        var TopicsStore = Ext.getStore('Topics').load();
        Ext.getStore('InviteMentee').load();
        Ext.getCmp('idVersionInviteMentor').setHtml(Mentor.Global.VERSION_NAME);
        Mentor.Global.Current_date = new Date();
        Ext.getCmp('idMenteeSelectFieldInviteMenteeScreen').reset();

        //Change the lable of Mentor and Mentor where Name Exits according to Application Configuration
        //Ext.getCmp('idLabelSelectMentorInviteMentee').setTitle('Select Available ' + Mentor.Global.MENTEE_NAME + " <span class='spanAsterisk'>*</span>");
        Ext.getCmp('idMenteeSelectFieldInviteMenteeScreen').setPlaceHolder('Select Available ' + Mentor.Global.MENTEE_NAME);







        Ext.getCmp('dateTimeIssueInviteMentee').setValue('');
        Ext.getCmp('timeIssueInviteMentee').setValue('');
        Ext.getCmp('mentorCommentInviteMenteeScreen').setValue('');


        //var TopicsStore = Ext.getStore('Topics').load();
        Ext.getStore('InviteMentee').load();
        Ext.getCmp('idVersionInviteMentor').setHtml(Mentor.Global.VERSION_NAME);

        //Change the lable of Mentor and Mentor where Name Exits according to Application Configuration
        //Ext.getCmp('idLabelSelectMentorInviteMentee').setTitle('Select Available ' + Mentor.Global.MENTEE_NAME + " <span class='spanAsterisk'>*</span>");
        Ext.getCmp('idMenteeSelectFieldInviteMenteeScreen').setPlaceHolder('Select Available ' + Mentor.Global.MENTEE_NAME);
        Mentor.app.application.getController('AppDetail').btnInviteForMeetingMentor();

        var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));

        Ext.getCmp('idMentorEmailInviteMenteeScreen').setValue(MentorLoginUser.MentorEmail);
        Ext.getCmp('idMentorPhoneNoInviteMenteeScreen').setValue(MentorLoginUser.MentorPhone);
        Ext.getCmp('idIsPhoneSMSCapableInviteMenteeScreen').setChecked(MentorLoginUser.DisableSMS);
        Ext.getCmp('idTopicsSelectFieldInviteMenteeScreen').reset();
        Ext.getCmp('idMeetingTypeSelectFieldInviteMenteeScreen').reset();
        Ext.getCmp('idMeetingPlaceSelectFieldInviteMenteeScreen').reset();
    }
});