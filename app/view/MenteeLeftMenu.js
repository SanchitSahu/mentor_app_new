Ext.define('Mentor.view.MenteeLeftMenu', {
    extend: 'Ext.Container',
    xtype: 'menteeLeftMenu',
    id: 'idMenteeLeftMenu',
    requires: [
        'Ext.Menu'
    ],
    config: {
 
        layout: {
            type: 'card'
        },
 
        items: [
            /*{
                xtype: 'toolbar',
                docked: 'top',
                title: 'Sliding Menu',
                items: [
                    {
                        xtype: 'button',
                        id: 'listButton',
                        iconCls: 'list',
                        ui: 'plain',
                        handler: function(){
                            if(Ext.Viewport.getMenus().left.isHidden()){
                                Ext.Viewport.showMenu('left');
                            }
                            else
                            {
                                Ext.Viewport.hideMenu('left');
                            }
                        }
                    }
                ]
            },*/{
                //xtype : 'mentorPendingRequest',
                //xtype : 'mentorPendingRequest',
                xtype: 'menteeInvitesTabView',
                id: 'idPendingInvitesTabInvitesMenteeBottomTabView',
                //iconCls: 'pendingInvitesIconMentorBottomTabView',
                //iconMask: true,
                //title: Mentor.Global.MENTOR_PENDING_TITLE,
                //id: 'idPendingInvitesTabInvitesMentorBottomTabView',
                //badgeText: ''
                //id: 'idPendingInvitesTabInvitesMentorBottomTabView',
                
            },{
                xtype: 'meetinghistorymentee',
                id: 'idMeetingListHistoryMenteeLeftMenu',
                
            },{
                xtype : 'mentorprofile',
                id: 'idProfileMentorLeftMenu',
            }
        ],
        listeners: [
            {
                event: "activate",
                fn: "onPageActivate"
            },
            {
                event: "painted",
                fn: "onPagePainted"
            },
            {
                event:'show',
                fn:"showMenu",
            }
            
        ]
    },

    
 
    initialize: function(){
        Ext.Viewport.setMenu(this.createMenu(),{
            side: 'left',
            reveal: true
        });
    },

    onPageActivate: function () {
        var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
        if (MentorLoginUser != null) {
            Ext.getCmp('idUserNameMentorLeftMenuScreen').setHtml(MentorLoginUser.MentorName);
            Ext.getCmp('idImgUserImageMentorLeftMenuScreen').setSrc(MentorLoginUser.MentorImage);

            var TeamDetail = Ext.decode(localStorage.getItem("TeamAllDetails"));
            try{
                       
                //Ext.ComponentQuery.query('#id_TeamPicture')[0].element.setStyle("backgroundImage", 'url("resources/images/group_avatar.png")');
                Ext.getCmp('idImgTeamImageMentorLeftMenuScreen').setSrc("resources/images/group_avatar.png");
                if (TeamDetail!=null && TeamDetail.OwnTeamDetail != false) {
                    Ext.getCmp('idTeamNameMenuLeftMenuScreen').setHtml(data.getTeamDetail.data.OwnTeamDetail[0].TeamName);
                    //Ext.ComponentQuery.query('#id_TeamPicture')[0].element.setStyle("backgroundImage", 'url("' + data.getTeamDetail.data.OwnTeamDetail[0].TeamFullImage + '")');
                    Ext.getCmp('idImgTeamImageMentorLeftMenuScreen').setSrc(TeamDetail.OwnTeamDetail[0].TeamFullImage);
                   
                } else if (TeamDetail!=null && TeamDetail.MemberTeamDetail != false) {
                    Ext.getCmp('idTeamNameMenuLeftMenuScreen').setHtml(TeamDetail.MemberTeamDetail[0].TeamName);
                    //Ext.ComponentQuery.query('#id_TeamPicture')[0].element.setStyle("backgroundImage", 'url("' + data.getTeamDetail.data.MemberTeamDetail[0].TeamFullImage + '")');
                     Ext.getCmp('idImgTeamImageMentorLeftMenuScreen').setSrc(TeamDetail.MemberTeamDetail[0].TeamFullImage);
                    
                } else {
                    Ext.getCmp('idTeamNameMenuLeftMenuScreen').setHtml('No Team');
                   
                }
                
            }catch (e){
                console.log(e);
            }

        }
    },
   
    createMenu: function(){
        var menu = Ext.create('Ext.Menu', {
            width: 250,
            scrollable: 'vertical',
            cls : 'menuCls',
            items: [
                {
                    xtype : 'panel',
                    layout: {
                        type: 'hbox',
                        align: 'middle',
                        pack : 'center'
                    },
                    style : 'background:#f2f2f2;',
                    padding: '30 0',
                    items : [
                        {
                            xtype : 'container',
                            layout: {
                                type: 'vbox',
                                pack: 'center',
                                    align: 'middle'
                               
                            },  
                             flex:1,
                            items:[
                                {
                                    xtype: 'image',
                                    //src: 'resources/images/default_img.png',
                                    id : 'idImgUserImageMentorLeftMenuScreen',
                                    height: 70,
                                    width: 70,
                                    cls: "left-menu-image-cls",
                                    style : 'background-size: cover;'
                                    //margin: 'auto',
                                },
                                {
                                    xtype: 'label',
                                    id : 'idUserNameMentorLeftMenuScreen',
                                    html : ' ',
                                    style: 'color: #4c4c4c;margin-bottom: 10px;font-size: 16px;'
                                }
                            ]
                        },
                        {
                            xtype : 'container',
                            flex:1,
                            layout: {
                                type: 'vbox',
                                pack: 'center',
                                    align: 'middle'
                               
                            },  
                            items:[
                                {
                                    xtype: 'image',
                                    //src: 'resources/images/group_avatar.png',
                                    id : 'idImgTeamImageMentorLeftMenuScreen',
                                    height: 70,
                                    width: 70,
                                    cls: "left-menu-image-cls",
                                    style : 'background-size: cover;'
                                    //margin: 'auto',
                                },
                                {
                                    xtype: 'label',
                                    html : ' ',
                                    id : 'idTeamNameMenuLeftMenuScreen',
                                    style: 'color:#4c4c4c;margin-bottom: 10px;font-size: 16px;'
                                }
                            ]
                        },
                        
                        
                    ]

                },
                {
                    xtype: 'button',
                    text: 'Pending',
                    cls: 'leftmenu-active-btn-cls',
                    //labelCls : 'leftMenuLabelCLS',
                    id : 'idPendingInvitesMenteeLeftMenu',
                    iconCls: 'pendingInvitesIconActiveMentorLeftMenu',
                    iconMask: true,
                    handler : function(){
                        Ext.Viewport.hideMenu('left');
                        Ext.getCmp('idBtnMeetingHistoryeMenteeLeftMenu').setCls('leftmenu-inactive-btn-cls');
                        Ext.getCmp('idBtnProfileMenteeLeftMenu').setCls('leftmenu-inactive-btn-cls');
                        Ext.getCmp('idPendingInvitesMenteeLeftMenu').setCls('leftmenu-active-btn-cls');


                        Ext.getCmp('idPendingInvitesMenteeLeftMenu').setIconCls('pendingInvitesIconActiveMentorLeftMenu');
                        Ext.getCmp('idBtnProfileMenteeLeftMenu').setIconCls('profileIconInActiveMentorLeftMenu');
                        Ext.getCmp('idBtnMeetingHistoryeMenteeLeftMenu').setIconCls('historyIconInActiveMentorLeftMenu');

                        Ext.getCmp('idMenteeLeftMenu').setActiveItem(0);
                    }
                },
                {
                    xtype: 'button',
                    text: 'History',
                    cls: 'leftmenu-inactive-btn-cls',
                    id : 'idBtnMeetingHistoryeMenteeLeftMenu',
                    iconCls: 'historyIconInActiveMentorLeftMenu',
                    iconMask: true,
                    handler: function(){
                       /*Ext.Viewport.hideMenu('left');
                        var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
                        var mainMenu = mainPanel.down("meetinghistorymentee");
                        if (!mainMenu) {
                            mainMenu = mainPanel.add({xtype: "meetinghistorymentee"});
                        }
                        Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
                        mainPanel.setActiveItem(mainMenu);*/
                        Ext.Viewport.hideMenu('left');
                        Ext.getCmp('idBtnMeetingHistoryeMenteeLeftMenu').setCls('leftmenu-active-btn-cls');
                        Ext.getCmp('idBtnProfileMenteeLeftMenu').setCls('leftmenu-inactive-btn-cls');
                        Ext.getCmp('idPendingInvitesMenteeLeftMenu').setCls('leftmenu-inactive-btn-cls');


                        Ext.getCmp('idPendingInvitesMenteeLeftMenu').setIconCls('pendingInvitesIconInActiveMentorLeftMenu');
                        Ext.getCmp('idBtnProfileMenteeLeftMenu').setIconCls('profileIconInActiveMentorLeftMenu');
                        Ext.getCmp('idBtnMeetingHistoryeMenteeLeftMenu').setIconCls('historyIconActiveMentorLeftMenu');

                        Ext.getCmp('idMenteeLeftMenu').setActiveItem(1);
                    }
                },
                {
                    xtype: 'button',
                    text: 'Profile',
                    cls: 'leftmenu-inactive-btn-cls',
                    id : 'idBtnProfileMenteeLeftMenu',
                    iconCls: 'profileIconInActiveMentorLeftMenu',
                    iconMask: true,
                    handler : function(){
                        Ext.Viewport.hideMenu('left');
                        Ext.getCmp('idBtnMeetingHistoryeMenteeLeftMenu').setCls('leftmenu-inactive-btn-cls');
                        Ext.getCmp('idBtnProfileMenteeLeftMenu').setCls('leftmenu-active-btn-cls');
                        Ext.getCmp('idPendingInvitesMenteeLeftMenu').setCls('leftmenu-inactive-btn-cls');
                        
                        Ext.getCmp('idPendingInvitesMenteeLeftMenu').setIconCls('pendingInvitesIconInActiveMentorLeftMenu');
                        Ext.getCmp('idBtnProfileMenteeLeftMenu').setIconCls('profileIconActiveMentorLeftMenu');
                        Ext.getCmp('idBtnMeetingHistoryeMenteeLeftMenu').setIconCls('historyIconInActiveMentorLeftMenu');
                        
                        Ext.getCmp('idMenteeLeftMenu').setActiveItem(2);

                        
                    }
                },
                {
                    xtype: 'button',
                    text: 'Logout',
                    cls: 'leftmenu-inactive-btn-cls',
                    //id : 'instantMeetingMentorBottomTabView',
                    iconCls: 'logoutIconMentorLeftMenu',
                    iconMask: true,
                    handler: function () {
                        Ext.Viewport.hideMenu('left');
                        Ext.Msg.confirm("MELS", "Are you sure you want to sign out?", function (btn) {
                            if (btn == 'yes') {
                                Mentor.app.application.getController('Main').btnSignOut();
                            }
                        });
                    }
                },
            ],
            listeners: [
                
                {
                    event:'show',
                    //fn:"showMenu",
                    fn : function ( parma1, param2) {
                         Mentor.app.application.getController('LeftMenu').showMenu(parma1, param2);
                    }
                }
            
            ],
            showMenu:function(){
                //var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
                var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
                if (MenteeLoginUser != null) {
                    Ext.getCmp('idUserNameMentorLeftMenuScreen').setHtml(MenteeLoginUser.MenteeName);
                    Ext.getCmp('idImgUserImageMentorLeftMenuScreen').setSrc(MenteeLoginUser.MenteeImage);

                    var TeamDetail = Ext.decode(localStorage.getItem("TeamAllDetails"));
                    try{
                               
                        //Ext.ComponentQuery.query('#id_TeamPicture')[0].element.setStyle("backgroundImage", 'url("resources/images/group_avatar.png")');
                        Ext.getCmp('idImgTeamImageMentorLeftMenuScreen').setSrc("resources/images/group_avatar.png");
                        if (TeamDetail!=null && TeamDetail.OwnTeamDetail != false) {
                            Ext.getCmp('idTeamNameMenuLeftMenuScreen').setHtml(TeamDetail.OwnTeamDetail[0].TeamName);
                            //Ext.ComponentQuery.query('#id_TeamPicture')[0].element.setStyle("backgroundImage", 'url("' + data.getTeamDetail.data.OwnTeamDetail[0].TeamFullImage + '")');
                            Ext.getCmp('idImgTeamImageMentorLeftMenuScreen').setSrc(TeamDetail.OwnTeamDetail[0].TeamFullImage);
                           
                        } else if (TeamDetail!=null && TeamDetail.MemberTeamDetail != false) {
                            Ext.getCmp('idTeamNameMenuLeftMenuScreen').setHtml(TeamDetail.MemberTeamDetail[0].TeamName);
                            //Ext.ComponentQuery.query('#id_TeamPicture')[0].element.setStyle("backgroundImage", 'url("' + data.getTeamDetail.data.MemberTeamDetail[0].TeamFullImage + '")');
                             Ext.getCmp('idImgTeamImageMentorLeftMenuScreen').setSrc(TeamDetail.MemberTeamDetail[0].TeamFullImage);
                            
                        } else {
                            Ext.getCmp('idTeamNameMenuLeftMenuScreen').setHtml('No Team');
                           
                        }
                     }
                     catch (e){
                        console.log(e);
                    }
                }
            },
 
        });
        return menu;
    }
});