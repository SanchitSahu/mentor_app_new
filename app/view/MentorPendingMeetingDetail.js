Ext.define('Mentor.view.MentorPendingMeetingDetail', {
    extend: 'Ext.Panel',
    xtype: 'mentorPendingMeetingDetail',
    id:'mentorPendingMeetingDetail',
    requires: [],
    config: {
        cls: 'mentor_pending_metting_detail',
        scrollable: {
            direction: 'vertical',
            indicators: {
                y: {
                    autoHide: false
                }
            }
        },
        defaults: {
            xtype: 'container',
           
        },
        items: [
            {
                docked: 'top',
                xtype: 'toolbar',
                title: 'Pending Meeting Details',
                cls: 'my-titlebar',
                style : "font-weight:normal;",
                defaults: {
                    iconMask: true
                },
                items: [
                    {
                        iconCls: 'btnBackCLS',
                        action: 'btnBackMentorPendingMeetingDetail',
                        docked: 'left',
                    },
                    {
                        iconCls: 'goToNotesIcon',
                        docked: 'left',
                        id: 'addnotes',
                        action: 'addnotes'
                    },
                    {
                        iconCls: 'helpIcon',
                        docked: 'right',
                        listeners: {
                            element: 'element',
                            tap: function () {
                                var popup = Mentor.app.application.getController('AppDetail').addHelpPopup('pending_meeting_detail');
                                Ext.Viewport.add(popup);
                                popup.show();
                            }
                        }
                    }
                ]
            },
            {
                xtype: 'label',
                id: 'idInfoMentorPendingMettingDetailScreen',
                style: 'text-align: center;padding: 15px;border-bottom:1px solid #ccd4ee;color:#181717;font-size: 17px;',
                html : 'Meeting Request from Mentee1'
            },
            {
                xtype: 'label',
                id: 'idUserNameWaitScreen',
                style: 'text-align: right;padding-right: 10px;padding: 10px;background-color: #C2C2C2;',
                hidden: true
            },
            {
                xtype: 'label',
                html: Mentor.Global.VERSION_NAME,
                style: 'text-align: right;padding-right: 10px;padding: 10px;',
                docked: 'bottom',
                hidden: true
            },
            {
                xtype: 'panel',
                layout: {type: 'vbox', },
                 margin: '0 10',
                items: [
                    {
                        xtype: 'label',
                        style: 'font-weight: normal;color:#161718',
                        margin: '20 10 0 10',
                        id: 'idLabelMenteeNameMentorPendingMeetingDetail',
                        cls : 'appLabelCls',
                         hidden : true,
                    },
                    {
                        xtype: 'textfield',
                        id: 'idMenteeNameMentorPendingMeetingDetail',
                        disabled: true,
                        disabledCls: 'textFieldDisableCLS',
                        inputCls: 'inputDisableCLSPendingMeetingDetailDisableCLS',
                        margin: '0 10 0 10',
                        hidden : true,
                    },
                    {
                        xtype: 'label',
                        html: 'Main Topics',
                        style: 'font-weight: normal;color:#161718',
                        margin: '20 10 0 10',
                        cls : 'appLabelCls',
                    },
                    {
                        xtype: 'textfield',
                        id: 'idMainTopicsMentorPendingMeetingDetail',
                        disabled: true,
                        disabledCls: 'textFieldDisableCLS',
                        inputCls: 'inputDisableCLSPendingMeetingDetailDisableCLS',
                        margin: '0 10 0 10'
                    },
                    {
                        xtype: 'label',
                        html: 'Meeting Type',
                         style: 'font-weight: normal;color:#161718',
                        margin: '20 10 0 10',
                        cls : 'appLabelCls',
                    },
                    {
                        xtype: 'textfield',
                        id: 'idMainTypeMentorPendingMeetingDetail',
                        disabled: true,
                        disabledCls: 'textFieldDisableCLS',
                        inputCls: 'inputDisableCLSPendingMeetingDetailDisableCLS',
                        margin: '0 10 0 10'
                    },
                    {
                        xtype: 'label',
                        html: 'Meeting Place',
                        style: 'font-weight: normal;color:#161718',
                        margin: '20 10 0 10',
                        cls : 'appLabelCls',
                    },
                    {
                        xtype: 'textfield',
                        id: 'idMeetingPlaceMentorPendingMeetingDetail',
                        disabled: true,
                        disabledCls: 'textFieldDisableCLS',
                        inputCls: 'inputDisableCLSPendingMeetingDetailDisableCLS',
                        margin: '0 10 0 10'
                    },
                    {
                        xtype: 'label',
                        html: 'Meeting Date Time',
                         style: 'font-weight: normal;color:#161718',
                        margin: '20 10 0 10',
                        cls : 'appLabelCls',
                    },
                    {
                        xtype: 'textfield',
                        id: 'idMeetingDateTimeMentorPendingMeetingDetail',
                        disabled: true,
                        disabledCls: 'textFieldDisableCLS',
                        inputCls: 'inputDisableCLSPendingMeetingDetailDisableCLS',
                        margin: '0 10 0 10'
                    },
                    {
                        xtype: 'label',
                        style: 'font-weight: normal;color:#161718',
                        margin: '20 10 0 10',
                        id: 'idLabelMenteeEmailMentorPendingMeetingDetail',
                        cls : 'appLabelCls',
                    },
                    {
                        xtype: 'textfield',
                        id: 'idMeenteeEmailMentorPendingMeetingDetail',
                        disabled: true,
                        disabledCls: 'textFieldDisableCLS',
                        inputCls: 'inputDisableCLSPendingMeetingDetailDisableCLS',
                        margin: '0 10 0 10'
                    },
                    {
                        xtype: 'label',
                        style: 'font-weight: normal;color:#161718',
                        margin: '20 10 0 10',
                        id: 'idLabelMenteePhoneNoMentorPendingMeetingDetail',
                        cls : 'appLabelCls',

                    },
                    {
                        xtype: 'textfield',
                        id: 'idMeenteePhoneMentorPendingMeetingDetail',
                        disabled: true,
                        disabledCls: 'textFieldDisableCLS',
                        inputCls: 'inputDisableCLSPendingMeetingDetailDisableCLS',
                        margin: '0 10 0 10'
                    },
                    {
                        xtype: 'panel',
                        margin: '10 0 0 10',
                        layout: {
                            type: 'hbox'
                        },
                        items: [
                            {
                                xtype: 'textfield',
                                id: 'idFollowUpMeetingIDTemp',
                                hidden: true
                            },
                            {
                                xtype: 'checkboxfield',
                                id: 'idIsPhoneSMSCapableMentorPendingMeetingDetail',
                                label: "Is above number Text Message(aka SMS) capable?",
                                labelCls: 'radio-btn-cls',
                                labelWidth: '90%',
                                labelAlign: 'right',
                                disabled: true,
                                cls: 'wordWrapSpan',
                                style : 'background: transparent;'
                            }
                        ]
                    },
                    {
                        xtype: 'label',
                        margin: '10 10 0 10',
                         style: 'font-weight: normal;color:#161718',
                        id: 'idLabelMenteeCommentsMentorPendingMeetingDetail',
                        cls : 'appLabelCls',
                    },
                    {
                        xtype: 'textareafield',
                        id: 'idMenteeCommentMentorPendingMeetingDetail',
                        disabledCls: 'textFieldDisableCLS',
                        inputCls: 'inputDisableCLSPendingMeetingDetailDisableCLS',
                        maxRows: 2,
                        disabled: true,
                        margin: '0 10 0 10'
                    },
                    {
                        xtype: 'label',
                        margin: '20 10 0 10',
                         style: 'font-weight: normal;color:#161718',
                        id: 'idLabelMentorCommentsMentorPendingMeetingDetail',
                        cls : 'appLabelCls',
                    },
                    {
                        xtype: 'textareafield',
                        id: 'idMentorCommentMentorPendingMeetingDetail',
                        disabledCls: 'textFieldDisableCLS',
                        inputCls: 'inputDisableCLSPendingMeetingDetailDisableCLS',
                        maxRows: 2,
                        disabled: true,
                        margin: '0 10 10 10'
                    },
                    {
                        xtype: 'label',
                        html: "Is this a follow up meeting?",
                         style: 'font-weight: normal;color:#161718',
                        margin: '10 10 0 10',
                        cls : 'appLabelCls',
                    },
                    {
                        xtype: 'panel',
                        margin: '0 0 0 10',
                        layout: {
                            type: 'hbox'
                        },
                        items: [
                            {
                                xtype: 'radiofield',
                                name: 'color',
                                value: '0',
                                label: 'Yes',
                                labelWidth: 100,
                                flex: 1,
                                labelCls: 'radio-btn-cls',
                                labelAlign: 'right',
                                cls : 'radioBtnPendingMeetinDetail',
                                id: 'idIsFollowUpMeetingMentorPendingMeetingDetailYes',
                                listeners: {
                                    'check': function (radio, e, eOpts) {
                                        Ext.getCmp('idLabelFollowUpListMentorPendingMeetingDetail').setHidden(false)
                                        Ext.getCmp('idFieldsetFollowUpListMentorPendingMeetingDetail').setHidden(false);
                                    }
                                }
                            },
                            {
                                xtype: 'radiofield',
                                id: 'idIsFollowUpMeetingMentorPendingMeetingDetailNo',
                                name: 'color',
                                value: '1',
                                labelWidth: 100,
                                checked: true,
                                flex: 1,
                                label: 'No',
                                labelCls: 'radio-btn-cls',
                                labelAlign: 'right',
                                cls : 'radioBtnPendingMeetinDetail',
                                listeners: {
                                    'check': function (radio, e, eOpts) {
                                        Ext.getCmp('idLabelFollowUpListMentorPendingMeetingDetail').setHidden(true);
                                        Ext.getCmp('idFieldsetFollowUpListMentorPendingMeetingDetail').setHidden(true);
                                    }
                                }
                            }
                        ]
//                        items: [
//                            {
//                                xtype: 'checkboxfield',
//                                id: 'idIsFollowUpMeetingMentorPendingMeetingDetail',
//                                label: "Is this a follow up meeting?",
//                                labelCls: 'radio-btn-cls',
//                                labelWidth: '90%',
//                                labelAlign: 'right',
//                                cls: 'wordWrapSpan',
//                                listeners: {
//                                    'check': function (radio, e, eOpts) {
//                                        Ext.getCmp('idLabelFollowUpListMentorPendingMeetingDetail').setHidden(false)
//                                        Ext.getCmp('idFieldsetFollowUpListMentorPendingMeetingDetail').setHidden(false);
//                                    },
//                                    'uncheck': function (radio, e, eOpts) {
//                                        Ext.getCmp('idLabelFollowUpListMentorPendingMeetingDetail').setHidden(true);
//                                        Ext.getCmp('idFieldsetFollowUpListMentorPendingMeetingDetail').setHidden(true);
//                                    }
//                                }
//                            }
//                        ]
                    },
                    {
                        xtype: 'label',
                        id: "idLabelFollowUpListMentorPendingMeetingDetail",
                        html: 'FollowUp Meeting for:',
                        style: 'font-weight: normal;color:#161718',
                        margin: '10 10 0 10',
                        hidden: true,
                        cls : 'appLabelCls',
                    },
                    {
                        xtype: 'fieldset',
                        id: "idFieldsetFollowUpListMentorPendingMeetingDetail",
                        title: "",
                        style: 'margin-top:-20px;',
                        hidden: true,
                        items: [
                            {
                                xtype: 'selectfield',
                                id: 'idFollowUpListMentorPendingMeetingDetail',
                                displayField: 'MeetingDetail',
                                valueField: 'MeetingID'
                            }
                        ]
                    },
                    {
                        xtype: 'button',
                        text: 'Begin New Meeting',
                        cls: 'signin-btn-cls',
                        action: "btnAddMeetingDetail",
                        id: 'idBtnStartNewMeetingMentorPendingMeetingDetail',
                        pressedCls : 'press-btn-cls',
                        margin: '0 10 10 10'
                    },
                    {
                        xtype: 'button',
                        text: 'WAIT FOR ACKNOWLEDGEMENT',
                        cls: 'waitfor-acknowledgement-btn-cls',
                        //action: "btnAddMeetingDetail",
                        id: 'idBtnWaitForAcknowledgementMentorPendingMeetingDetail',
                        //pressedCls : 'press-grey_btn-cls',
                        margin: '20 10 20 10',
                        hidden:true,
                        ui : 'plain',
                    },
                    {
                        xtype: 'container',
                        layout: {
                            type: 'hbox',
                            pack: 'center'
                        },
                        margin: '0 10 10 10',
                        items: [
                            {
                                xtype: 'button',
                                text: 'RESCHEDULE',
                                id: 'idBtnRescheduleMeetingMentorPendingMeetingDetail',
                                cls: 'signin-btn-cls',
                                 pressedCls : 'press-btn-cls',
                                action: 'doReschedule',
                                flex: 1,
                                margin: '0 5 0 0',
                                style : 'font-size: 0.9em;',
                            },
                            {
                                xtype: 'button',
                                text: 'CANCEL MEETING',
                                id: 'idBtnCancelMeetingMentorPendingMeetingDetail',
                                /*cls: 'decline-btn-cls',*/
                                pressedCls : 'press-grey_btn-cls',
                                cls: 'grey-btn-cls',
                                action: 'doCancel',
                                flex: 1,
                                margin: '0 0 0 5',
                                style : 'font-size: 0.9em;',
                            }
                        ]
                    }
                ]
            }
        ],
        listeners: [
            {
                event: "painted",
                fn: "onPagePainted"
            },
            {
                fn: 'onRadioChange',
                event: 'check',
                delegate: '#idIsFollowUpMeetingMentorPendingMeetingDetailYes'
            },
            {
                event: "deactivate",
                fn: "onPageDeactivate"
            }
        ]
    },
    onRadioChange: function () {
        var FollowUpDropdown = Ext.getCmp('idFollowUpListMentorPendingMeetingDetail');

        var MeetingHistory = Ext.getStore('MeetingHistory');
        MeetingHistory.clearFilter();

        MeetingHistory.filterBy(function (rec) {
            return (rec.get('MenteeName') == Ext.getCmp('idMenteeNameMentorPendingMeetingDetail').getValue()) && (rec.get('isCancelled') == 0) && (rec.get('IsTeamMeeting') == 0);
        });
        FollowUpDropdown.setStore(MeetingHistory);

        if (Ext.getCmp('idFollowUpMeetingIDTemp').getValue() > 0) {
            FollowUpDropdown.setValue(Ext.getCmp('idFollowUpMeetingIDTemp').getValue()).setDisabled(true);
        }
    },
    onPagePainted: function () {
        clearInterval(Mentor.Global.SET_INTERVAL_ID);
        Mentor.Global.SET_INTERVAL_ID = 0;

        var FollowUpDropdown = Ext.getCmp('idFollowUpListMentorPendingMeetingDetail');
        if (Ext.getCmp('idFollowUpMeetingIDTemp').getValue() > 0) {
            setTimeout(function () {
                FollowUpDropdown.setValue(Ext.getCmp('idFollowUpMeetingIDTemp').getValue()).setDisabled(true);
            }, 1000);
        }
    },
    onPageDeactivate: function () {
        var MeetingHistory = Ext.getStore('MeetingHistory');
        MeetingHistory.clearFilter();
    }
});
