Ext.define('Mentor.view.MentorReviewForm', {
    extend: 'Ext.form.Panel',
    xtype: 'mentorreviewform',
    id: 'idMentorReviewFormScreen',
    requires: [],
    config: {
        cls: 'mentorreviewform',
        scrollable: {
            direction: 'vertical',
            indicators: {
                y: {
                    autoHide: false
                }
            }
        },
        defaults: {
            xtype: 'formpanel'
        },
        items: [
            {
                docked: 'top',
                xtype: 'toolbar',
                title: 'Mentor Review Form',
                cls: 'my-titlebar-review_form',
                id: 'idMentorReviewFormTitle',
                defaults: {
                    iconMask: true
                },
                items: [
                    {
                        iconCls: 'btnBackCLS',
                        action: 'btnBackMentorReviewForm',
                        docked: 'left'
                    },
                    {
                        iconCls: 'goToNotesIcon',
                        //docked: 'left',
                        //style: 'visibility:hidden'
                        id: 'addnotes',
                        action: 'addnotes'
                    },
                    //{
                    //iconCls: 'addIcon',
                    //style: "visibility:hidden;"
                    //listeners: {
                    //    element: 'element',
                    //    tap: function () {
                    //        var popup = Mentor.app.application.getController('AppDetail').addNotePopup();
                    //        Ext.Viewport.add(popup);
                    //        popup.show();
                    //    }
                    //}
                    //},
                    {
                        iconCls: 'helpIcon',
                        docked: 'right',
                        listeners: {
                            element: 'element',
                            tap: function () {
                                var popup = Mentor.app.application.getController('AppDetail').addHelpPopup('review_form');
                                Ext.Viewport.add(popup);
                                popup.show();
                            }
                        }
                    }
                ]
            },
            {
                xtype: 'label',
                id: 'idUserNameEnterpreneur',
                style: 'text-align: right;padding-right: 10px;padding: 10px;background-color: #C2C2C2;',
                hidden: true
            },
            {
                xtype: 'label',
                html: Mentor.Global.VERSION_NAME,
                style: 'text-align: right;padding-right: 10px;padding: 10px;',
                docked: 'bottom',
                hidden: true
            },
            {
                xtype: 'panel',
                margin: '10 20',
                layout: {
                    type: 'vbox'
                },
                items: [
                    {
                        xtype: 'panel',
                        margin: '10 0 0 0 ',
                        layout: {
                            type: 'vbox'
                        },
                        items: [
                            {
                                xtype: 'label',
                                html: 'Meeting With Whom',
                                style: 'font-weight: normal;color:#161718',
                                cls : 'appLabelCls',
                                 
                            },
                            {
                                xtype: 'textfield',
                                name: 'meetingWithWhom',
                                id: 'meetingWithWhomMentorReviewForm',
                                disabled: true,
                                cls: 'textFieldDisableCLS',
                                clearIcon: false,
                                inputCls: 'inputDisabledCLS',

                                //flex: 3
                                        //listeners: {
                                        //    element: 'element',
                                        //    tap: function () {
                                        //        Mentor.app.application.getController('AppDetail').changeMenteeDetail();
                                        //    }
                                        //}
                            }
                        ]
                    },
                    {
                        xtype: 'panel',
                        id: 'mentorMeetingTypePanel',
                        margin: '20 0 0 0 ',
                        layout: {
                            type: 'vbox'
                        },
                        items: [
                            {
                                xtype: 'label',
                                html: 'Meeting Type',
                                style: 'font-weight: normal;color:#161718',
                                cls : 'appLabelCls',
                            },
                            {
                                //title: 'Meeting Type',
                                xtype: 'fieldset',
                                //flex: 3,
                                id: 'idLabelSelectMeetingTypeEnterpreneurScreen',
                                items: [
                                    {
                                        xtype: 'selectfield',
                                        id: 'meetingTypeMentorReviewForm',
                                        store: 'MeetingType',
                                        displayField: 'MeetingTypeName',
                                        valueField: 'MeetingTypeID',
                                        listeners: {
                                            change: function (field, value) {
                                                if (value instanceof Ext.data.Model) {
                                                    value = value.get(field.getValueField());
                                                }
                                                console.log(value);
                                                var Store = Ext.getStore('MeetingType');
                                                var record = Store.findRecord('MeetingTypeID', value);
                                                if (record != null) {
                                                    if (record.data.MeetingTypeName != "In Person") {
                                                        Ext.getCmp('meetingPlaceMentorReviewForm').setDisabled(true);
                                                        Ext.getCmp('meetingPlaceMentorReviewForm').setValue(3);
                                                    } else {
                                                        Ext.getCmp('meetingPlaceMentorReviewForm').setDisabled(false);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                ]
                            }
                            //{
                            //    xtype: 'selectfield',
                            //    id: 'meetingTypeMentorReviewForm',
                            //    store: 'MeetingType',
                            //    displayField: 'MeetingTypeName',
                            //    valueField: 'MeetingTypeID',
                            //    xtype: 'textfield',
                            //    name: 'meetingType',
                            //    id: 'meetingTypeMentorReviewForm',
                            //    disabled: false,
                            //    cls: 'textFieldDisableCLS',
                            //    clearIcon: false,
                            //    inputCls: 'inputCLS',
                            //    flex: 3,
                            //    listeners: {
                            //        element: 'element',
                            //        tap: function () {
                            //           Mentor.app.application.getController('AppDetail').changeMenteeDetail();
                            //        }
                            //    }
                            //},
                        ]
                    },
                    {
                        xtype: 'panel',
                        id: 'mentorMeetingPlacePanel',
                        margin: '20 0 0 0 ',
                        layout: {
                            type: 'vbox'
                        },
                        items: [
                            {
                                xtype: 'label',
                                html: 'Meeting Place',
                                /*cls: 'reviewFormLabelCLS',
                                flex: 2*/
                                style: 'font-weight: normal;color:#161718',
                                cls : 'appLabelCls',
                            },
                            {
                                //title: 'Meeting Type',
                                xtype: 'fieldset',
                                //flex: 3,
                                id: 'idLabelSelectMeetingPlaceEnterpreneurScreen',
                                items: [
                                    {
                                        xtype: 'selectfield',
                                        id: 'meetingPlaceMentorReviewForm',
                                        store: 'MeetingPlace',
                                        displayField: 'MeetingPlaceName',
                                        valueField: 'MeetingPlaceID'
                                    }
                                ]
                            }
                            //{
                            //    xtype: 'textfield',
                            //    name: 'meetingType',
                            //    id: 'meetingPlaceMentorReviewForm',
                            //    disabled: false,
                            //    cls: 'textFieldDisableCLS',
                            //    clearIcon: false,
                            //    inputCls: 'inputCLS',
                            //    flex: 3,
                            //    listeners: {
                            //        element: 'element',
                            //        tap: function () {
                            //            Mentor.app.application.getController('AppDetail').changeMenteeDetail();
                            //        }
                            //    }
                            //},
                        ]
                    },
                    {
                        xtype: 'panel',
                        margin: '20 0 0 0 ',
                        layout: {
                            type: 'vbox'
                        },
                        items: [
                            {
                                xtype: 'label',
                                html: 'Meeting Main Topic',
                                /*cls: 'reviewFormLabelCLS',
                                flex: 2*/
                                style: 'font-weight: normal;color:#161718',
                                cls : 'appLabelCls',
                            },
                            {
                                xtype: 'fieldset',
                                //flex: 3,
                                id: 'idLabelSelectMainTopicEnterpreneurScreen',
                                items: [
                                    {
                                        xtype: 'selectfield',
                                        id: 'meetingSubjectMentorReviewForm',
                                        store: 'Topics',
                                        displayField: 'TopicDescription',
                                        valueField: 'TopicID',
                                        listeners: {
                                            change: function (field, value) {
                                                localStorage.setItem("storeMeetingDetailSubTopics", null);
                                                Ext.getCmp('mainIssueDiscussedMentorReviewForm').setValue("");
                                            }
                                        }
                                    }
                                ]
                            }
                            //{
                            //    xtype: 'textfield',
                            //    name: 'meetingSubject',
                            //    id: 'meetingSubjectMentorReviewForm',
                            //    disabled: false,
                            //    cls: 'textFieldDisableCLS',
                            //    clearIcon: false,
                            //    inputCls: 'inputCLS',
                            //    flex: 3,
                            //    listeners: {
                            //        element: 'element',
                            //        tap: function () {
                            //            Mentor.app.application.getController('AppDetail').changeMenteeDetail();
                            //        }
                            //    }
                            //},
                        ]
                    },
                    {
                        xtype: 'panel',
                        id: 'mentorMeetingSubTopicPanel',
                        margin: '20 0 0 0 ',
                        layout: {
                            type: 'vbox'
                        },
                        items: [
                            {
                                xtype: 'label',
                                html: 'Subtopics Discussed',
                               /* cls: 'reviewFormLabelCLS',
                                flex: 2*/
                                style: 'font-weight: normal;color:#161718',
                                cls : 'appLabelCls',
                            },
                            {
                                xtype: 'textareafield',
                                name: 'mainIssueDiscussed',
                                id: 'mainIssueDiscussedMentorReviewForm',
                                disabled: false,
                                cls: 'textFieldDisableCLS',
                                clearIcon: false,
                                inputCls: 'inputCLS',
                                maxRows: 2,
                                //flex: 3,
                                listeners: {
                                    element: 'element',
                                    tap: function () {
                                        Mentor.app.application.getController('AppDetail').changeMeetingDetail(Ext.getCmp('meetingSubjectMentorReviewForm').getValue());
                                    }
                                }
                            }
                        ]
                    },
                    {
                        xtype: 'panel',
                        id: 'mentorMeetingFollowUpMeetingPanel',
                        margin: '20 0 0 0 ',
                        layout: {
                            type: 'vbox'
                        },
                        items: [
                            {
                                xtype: 'label',
                                html: 'FollowUp Meeting With',
                               /* cls: 'reviewFormLabelCLS',
                                flex: 2*/
                                style: 'font-weight: normal;color:#161718',
                                cls : 'appLabelCls',
                            },
                            {
                                xtype: 'textfield',
                                id: 'meetingFollowUpMeetingMentorReviewForm',
                                disabled: true,
                                cls: 'textFieldDisableCLS',
                                clearIcon: false,
                                //flex: 3,
                                //listeners: {
                                //    element: 'element',
                                //    tap: function () {
                                //        Mentor.app.application.getController('AppDetail').changeMenteeDetail();
                                //    }
                                //}
                            }
                        ]
                    },
                    {
                        xtype: 'panel',
                        layout: {
                            type: 'hbox'
                        },
                        margin: '20 0 0 0 ',
                        items : [
                            {
                                xtype: 'panel',
                                id: 'mentorMeetingStartTimePanel',
                                margin: '0 5 0 0 ',
                                layout: {
                                    type: 'vbox'
                                },
                                flex: 1,
                                items: [
                                    {
                                        xtype: 'label',
                                        html: 'Meeting Start Time',
                                        /*cls: 'reviewFormLabelCLS',
                                        flex: 2*/
                                        style: 'font-weight: normal;color:#161718',
                                        cls : 'appLabelCls',
                                    },
                                    {
                                        xtype: 'textfield',
                                        name: 'meetingStartTime',
                                        id: 'meetingStartTimeMentorReviewForm',
                                        disabled: false,
                                        cls: 'textFieldDisableCLS',
                                        clearIcon: false,
                                        inputCls: 'inputCLS',
                                        style:'font-size: 17px;',
                                        //flex: 3,
                                        listeners: {
                                            keyup: function (form, e) {
                                                Mentor.app.application.getController('Main').calculateElapsedTimeEditForm();
                                            }
                                        }
                                        //listeners: {
                                        //    element: 'element',
                                        //    tap: function () {
                                        //        Mentor.app.application.getController('AppDetail').changeMenteeDetail();
                                        //    }
                                        //}
                                    }
                                ]
                            },
                            {
                                xtype: 'panel',
                                id: 'mentorMeetingEndTimePanel',
                                layout: {
                                    type: 'vbox'
                                },
                                margin: '0 0 0 5',
                                flex: 1,
                                items: [
                                    {
                                        xtype: 'label',
                                        html: 'Meeting End Time',
                                        /*cls: 'reviewFormLabelCLS',
                                        flex: 2*/
                                        style: 'font-weight: normal;color:#161718',
                                        cls : 'appLabelCls',
                                    },
                                    {
                                        xtype: 'textfield',
                                        name: 'meetingEndTime',
                                        id: 'meetingEndTimeReviewForm',
                                        disabled: false,
                                        cls: 'textFieldDisableCLS',
                                        clearIcon: false,
                                        inputCls: 'inputCLS',
                                         style:'font-size: 17px;',
                                        //flex: 3,
                                        listeners: {
                                            keyup: function (form, e) {
                                                Mentor.app.application.getController('Main').calculateElapsedTimeEditForm();
                                            }
                                        }
                                        //listeners: {
                                        //    element: 'element',
                                        //    tap: function () {
                                        //        Mentor.app.application.getController('AppDetail').changeMenteeDetail();
                                        //    }
                                        //}
                                    }
                                ]
                            },
                        ]
                    },
                    {
                        xtype: 'panel',
                        id: 'mentorMeetingElapsedPanel',
                        margin: '20 0 0 0 ',
                        layout: {
                            type: 'vbox'
                        },
                        items: [
                            {
                                xtype: 'label',
                                html: 'Elapsed Time (Minutes)',
                                /*cls: 'reviewFormLabelCLS',
                                flex: 2*/
                                style: 'font-weight: normal;color:#161718',
                                cls : 'appLabelCls',
                            },
                            {
                                xtype: 'textfield',
                                name: 'meetingElapsedTime',
                                id: 'meetingElapsedTimeMentorReviewForm',
                                disabled: false,
                                cls: 'textFieldDisableCLS',
                                clearIcon: false,
                                inputCls: 'inputCLS',
                                //flex: 3,
                                listeners: {
                                    keyup: function (form, e) {
                                        Mentor.app.application.getController('Main').calculateEndTimeEditForm();
                                    },
                                    blur: function () {
                                        var thisValue = Ext.getCmp('meetingElapsedTimeMentorReviewForm').getValue();
                                        if (thisValue == '' || thisValue == 0) {
                                            Ext.getCmp('meetingElapsedTimeMentorReviewForm').setValue(1);
                                        }
                                    }
                                }
                                //listeners: {
                                //    element: 'element',
                                //    tap: function () {
                                //        Mentor.app.application.getController('AppDetail').changeMenteeDetail();
                                //    }
                                //}
                            }
                        ]
                    },
                    {
                        xtype: 'panel',
                        id: 'mentorMeetingMentorActionItemPanel',
                        margin: '20 0 0 0 ',
                        layout: {
                            type: 'vbox'
                        },
                        items: [
                            {
                                xtype: 'label',
                                html: 'ToDo Item By Mentor',
                                id: 'idLabelActionItemByMentor',
                                /*cls: 'reviewFormLabelCLS',
                                flex: 2*/
                                style: 'font-weight: normal;color:#161718',
                                cls : 'appLabelCls',
                            },
                            {
                                xtype: 'textareafield',
                                name: 'actionItemsByMentor',
                                id: 'actionItemsByMentorMentorReviewForm',
                                disabled: false,
                                cls: 'textFieldDisableCLS',
                                clearIcon: false,
                                maxRows: 2,
                                inputCls: 'inputCLS',
                                //flex: 3,
                                listeners: {
                                    element: 'element',
                                    tap: function () {
                                        Mentor.app.application.getController('AppDetail').changeMentorActionItemsDetail();
                                    }
                                }
                            }
                        ]
                    },
                    {
                        xtype: 'panel',
                        id: 'mentorMeetingMenteeActionItemPanel',
                        margin: '20 0 0 0 ',
                        layout: {
                            type: 'vbox'
                        },
                        items: [
                            {
                                xtype: 'label',
                                html: 'ToDo Items By Mentee',
                                id: 'idLabelActionItemByMentee',
                                /*cls: 'reviewFormLabelCLS',
                                flex: 2*/
                                style: 'font-weight: normal;color:#161718',
                                cls : 'appLabelCls',
                            },
                            {
                                /*listeners : {
                                 element : 'element',
                                 tap : function() {
                                 Mentor.app.application.getController('AppDetail').changeMenteeActionItemsDetail();
                                 }
                                 }*/
                                xtype: 'textareafield',
                                name: 'actionItemsByMentee',
                                id: 'actionItemsByMenteeMentorReviewForm',
                                disabled: true,
                                disabledCls: 'textFieldDisableCLS',
                                inputCls: 'inputDisableCLS',
                                maxRows: 2,
                                //flex: 3
                            }
                        ]
                    },
                    {
                        xtype: 'panel',
                        margin: '20 0 0 0 ',
                        layout: {
                            type: 'vbox'
                        },
                        items: [
                            {
                                xtype: 'label',
                                html: 'Mentee Comment',
                                id: 'idLabelMenteeComment',
                                /*cls: 'reviewFormLabelCLS',
                                flex: 2*/
                                style: 'font-weight: normal;color:#161718',
                                cls : 'appLabelCls',
                            },
                            {
                                xtype: 'textareafield',
                                name: 'menteeComment',
                                id: 'menteeCommentMenteeReviewForm',
                                cls: 'textFieldDisableCLS',
                                maxRows: 2,
                                disabled: true,
                                inputCls: 'inputDisabledCLS',
                                clearIcon: false,
                                //flex: 3
                            }
                        ]
                    },
                    {
                        xtype: 'panel',
                        margin: '20 0 0 0 ',
                        layout: {
                            type: 'vbox'
                        },
                        items: [
                            {
                                xtype: 'label',
                                html: 'Mentor Comment',
                                id: 'idLabelMentorComment',
                               /* cls: 'reviewFormLabelCLS',
                                flex: 2*/
                                style: 'font-weight: normal;color:#161718',
                                cls : 'appLabelCls',
                            },
                            {
                                xtype: 'textareafield',
                                name: 'mentorComment',
                                id: 'mentorCommentMentorReviewForm',
                                cls: 'textFieldDisableCLS',
                                maxRows: 2,
                                disabled: false,
                                inputCls: 'inputCLS',
                                clearIcon: false,
                                //flex: 3
                            }
                        ]
                    },
                    {
                        xtype: 'panel',
                        id: 'mentorMeetingRatingPanel',
                        margin: '10 0 0 0 ',
                        style: 'background-color: #f2f2f2;',
                        items: [
                            {
                                xtype: 'label',
                                html: 'Rate your satisfaction with the meeting',
                                margin: '20 0 0 0',
                                cls: 'reviewFormLabelCLS',
                                style: 'text-align: -webkit-center;'
                            },
                            {
                                xtype: 'label',
                                id: 'satisfactionRatingText',
                                style: 'text-align: -webkit-center;font-weight: bold;font-size:14px;color: #808080;'
                            },
                            {
                                xtype: 'label',
                                html: 'From Very Unsatisfied to Very Satisfied',
                                style: 'text-align: -webkit-center;font-weight: bold;font-size:14px;color: #808080;'
                            },
                            {
                                //style: 'background:red',
                                xtype: 'ratingfield',
                                name: 'usefull',
                                label: ' ',
                                labelWidth: '0%',
                                value: 0,
                                id: 'idSatisfactionWithMeeting',
                                labelCls: 'background:red',
                                style: 'background-color: #f2f2f2;'
                            }
                        ]
                    }
                ]
            },
            {
                xtype: 'container',
                layout: {
                    type: 'hbox',
                    pack: 'center'
                },
                margin: '20 10',
                items: [
                    {
                        //cls : 'tick',
                        xtype: 'button',
                        text: 'UPDATE',
                        cls: 'signin-btn-cls',
                        pressedCls : 'press-btn-cls',
                        id: 'idBtnSubmitMentorReviewForm',
                        action: "doSubmitMentorReviewForm",
                        flex: 1,
                        
                    },
                    {
                        xtype: 'button',
                        text: 'CANCEL',
                        pressedCls : 'press-grey_btn-cls',
                        cls: 'grey-btn-cls',
                        action: 'btnBackMentorReviewForm',
                        id: 'idBtnBackMentorReviewForm',
                        flex: 1,
                        
                    }
                ]
            }
        ],
        listeners: [
            {
                event: "activate",
                fn: "onPageActivate"
            },
            {
                event: "painted",
                fn: "onPagePainted"
            }
        ]
    },
    onPageActivate: function () {
        localStorage.setItem('CacheEditMentorActionItems', null);
        localStorage.setItem('CacheMentorSubtopicDiscussed', null);

        Ext.getStore('MeetingType').load();
        Ext.getStore('MeetingPlace').load();
        Ext.getStore('Topics').load();
        //Change the lable of Mentor and Mentor where Name Exits according to Application Configuration
        Ext.getCmp('idLabelActionItemByMentor').setHtml('ToDo Item By ' + Mentor.Global.MENTOR_NAME);
        Ext.getCmp('idLabelActionItemByMentee').setHtml('ToDo Items By ' + Mentor.Global.MENTEE_NAME);
        Ext.getCmp('idLabelMentorComment').setHtml(Mentor.Global.MENTOR_NAME + ' Comment');
        Ext.getCmp('idLabelMenteeComment').setHtml(Mentor.Global.MENTEE_NAME + ' Comment');
    },
    onPagePainted: function () {
        clearInterval(Mentor.Global.SET_INTERVAL_ID);
        Mentor.Global.SET_INTERVAL_ID = 0;
        Ext.getCmp('satisfactionRatingText').setHtml('(This rating will not be seen by the ' + Mentor.Global.MEETING_DETAIL.MenteeName + ')');
    }
});
