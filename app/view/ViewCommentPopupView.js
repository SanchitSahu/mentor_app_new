Ext.define('Mentor.view.ViewCommentPopupView', {
    extend: 'Ext.Panel',
    xtype: 'viewcommentpopview',
    requires: [],
    config: {
        cls: 'viewCommentPopupScreen',
        defaults: {
        },
        items: [
            {
                xtype: 'textfield',
                id: 'idViewCommentID',
                hidden: true
            },
            {
                xtype: 'textfield',
                id: 'idViewCommentMeetingID',
                hidden: true
            },
            {
                xtype: 'label',
                html: 'Comment Title',
                style: 'font-weight: bold;',
                padding: '35 10 3 10',
                hidden: true
            },
            {
                xtype: 'label',
                id: 'idViewCommentTitle',
                //cls: 'textFieldDisableCLS',
                inputCls: 'inputCLS',
                margin: '0 10 0 10',
                clearIcon: false,
                hidden: true
            },
            {
                xtype: 'label',
                html: "Comment Description",
                style: 'font-weight: bold;',
                padding: '20 10 3 10'
            },
            {
                xtype: 'textareafield',
                height: '250px',
                style: "background-color: #AFEEEE;",
                id: 'idViewCommentDescription',
                margin: '0 10 0 10',
                disabled: false,
                clearIcon: false
            },
            {
                xtype: 'container',
                layout: {
                    type: 'hbox',
                    pack: 'center'
                },
                margin: '5 0 10 0',
                items: [
                    {
                        xtype: 'button',
                        text: 'Edit',
                        id: 'idEditBtnViewCommentPopup',
                        //cls : 'tick',
                        cls: 'signin-btn-cls-comment',
                        //action: "doSubmitAddEditComment",
                        flex: 1,
                        margin: '10 5 0 5',
                        listeners: {
                            tap: function () {
                                var CommentID = Ext.getCmp('idViewCommentID').getValue();
                                var popupObject = Ext.getCmp('viewCommentPopUp');
                                popupObject.hide();

                                var editCommentPopup = Mentor.app.application.getController('AppDetail').editCommentPopup('', '', CommentID);
                                Ext.Viewport.add(editCommentPopup);
                                editCommentPopup.show();
                            }
                        }
                    },
                    {
                        xtype: 'button',
                        text: 'Delete',
                        id: 'idDeleteBtnViewCommentPopup',
                        //cls : 'cancel',
                        cls: 'decline-btn-cls-comment',
                        flex: 1,
                        margin: '10 5 0 5',
                        listeners: {
                            tap: function () {
                                Ext.Msg.confirm("MELS", "Are you sure you want to delete this comment?", function (btn) {
                                    if (btn == 'yes') {
                                        var CommentID = Ext.getCmp('idViewCommentID').getValue();
                                        var popupObject = Ext.getCmp('viewCommentPopUp');
                                        popupObject.hide();
                                        Mentor.app.application.getController('AppDetail').deleteCommentPopup('', '', CommentID);
                                    }
                                });
                            }
                        }
                    }
                ]
            }
        ]
    },
    listeners: [
        {
            event: "activate",
            fn: "onPageActivate"
        },
        {
            event: "painted",
            fn: "onPagePainted"
        }
    ],
    onPagePainted: function () {}
});