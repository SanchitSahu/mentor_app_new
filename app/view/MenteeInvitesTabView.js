Ext.define('Mentor.view.MenteeInvitesTabView', {
    extend: 'Ext.TabPanel',
    xtype: 'menteeInvitesTabView',
    id: 'idMenteeInvitesTabView',
    config: {
        tabBar: {
            docked: 'top',
            layout: {
                pack: 'center'
            },
            defaults: {
                flex: 1
            }
        },
        items: [
            {
                docked: 'top',
                xtype: 'toolbar',
                title : Mentor.Global.MENTEE_INVITES_TITLE,//title: 'Invites',
                id: 'invitesTab',
                cls: 'mettingHistoryToolBar',
                defaults: {
                    iconMask: true
                },
                items: [
                    {
                        xtype: 'button',
                        id: 'listButton',
                        iconCls: 'leftMenuIconCLS',
                         iconMask: true,
                        docked: 'left',
                        ui: 'plain',
                        handler: function(){
                            if(Ext.Viewport.getMenus().left.isHidden()){
                                Ext.Viewport.showMenu('left');
                            }
                            else
                            {
                                Ext.Viewport.hideMenu('left');
                            }
                        }
                    },
                    {
                        iconCls: 'goToNotesIcon',
                        docked: 'left',
                        //style: 'visibility:hidden'
                        id: 'addnotes',
                        action: 'addnotes'
                    },
                   /* {
                        iconCls: 'refreshIcon',
                        docked: 'left',
                        style: 'visibility:hidden'
                    },*/
                    {
                        iconCls: 'helpIcon',
                        docked: 'right',
                        listeners: {
                            element: 'element',
                            tap: function () {
                                var TabBar = Ext.getCmp('idPendingInvitesTabInvitesMenteeBottomTabView').getTabBar();
                                var TabBarPos = TabBar.items.indexOf(TabBar.getActiveTab());
                                if (TabBarPos == 3) {
                                    var popup = Mentor.app.application.getController('AppDetail').addHelpPopup('issue_invite');
                                } else {
                                    var popup = Mentor.app.application.getController('AppDetail').addHelpPopup('invite_screen');
                                }

                                Ext.Viewport.add(popup);
                                popup.show();
                            }
                        }
                    },
                    {
                        iconCls: 'refreshIcon',
                        action: '',
                        docked: 'right',
                        id: 'refreshscreen',
                        handler: function () {
                            var TabBar = Ext.getCmp('idPendingInvitesTabInvitesMenteeBottomTabView').getTabBar();
                            var TabBarPos = TabBar.items.indexOf(TabBar.getActiveTab());
                            if (TabBarPos == 0) {
                                Mentor.app.application.getController('AppDetail').getMentorWaitScreenAccepted();
                            } else if (TabBarPos == 1) {
                                Mentor.app.application.getController('AppDetail').getMentorWaitScreenPending();
                            } else if (TabBarPos == 2) {
                                Mentor.app.application.getController('AppDetail').getMentorWaitScreenRejected();
                            } else if (TabBarPos == 3) {
                                Mentor.app.application.getController('AppDetail').btnInviteForMeeting();
                            }
                        }
                    }
                    //{
                    //    iconCls: 'arrow_left',
                    //    iconCls: 'addMettingMentorMettingHistory',
                    //    action: 'btnBackMenteeReviewForm',
                    //    docked: 'right',
                    //    style: 'visibility: hidden;',
                    //},
                ]
            },
            {
                title: Mentor.Global.MENTEE_ACCEPTED_TITLE,
                xtype: 'inviteStatusAccepted'
            },
            {
                title: Mentor.Global.MENTEE_PENDING_TITLE,
                xtype: 'inviteStatusPending'
            },
            {
                title: Mentor.Global.MENTEE_DECLINED_TITLE,
                xtype: 'inviteStatusRejected'
            },
            {
                xtype: 'invitementor',
                title: Mentor.Global.MENTEE_ISSUE_INVITE_TITLE
            }
        ],
        listeners: [
            {
                event: "activate",
                fn: "onPageActivate"
            },
            {
                event: "painted",
                fn: "onPagePainted"
            },
            {
                event: "activeitemchange",
                fn: function(value, oldValue, eOpts ){
                    try{
                        var TabBar = Ext.getCmp('idPendingInvitesTabInvitesMenteeBottomTabView').getTabBar();
                        var TabBarPos = TabBar.items.indexOf(TabBar.getActiveTab());
                        TabBar.getAt(0).setStyle('color: #a399af;');
                        TabBar.getAt(1).setStyle('color: #a399af;');
                        TabBar.getAt(2).setStyle('color: #a399af;');
                        if (TabBarPos == 0) {
                           TabBar.getAt(0).setStyle('color: #96ff96;');
                        } else if (TabBarPos == 1) {
                            TabBar.getAt(1).setStyle('color: #fffe86;');
                        } else if (TabBarPos == 2) {
                            TabBar.getAt(2).setStyle('color: #ffa49d;');
                        } 
                    }catch(e){
                        console.log(e);
                    }
                }
            }
        ]
    },
    onPageActivate: function () {
         var TabBar = Ext.getCmp('idPendingInvitesTabInvitesMenteeBottomTabView').getTabBar();
        TabBar.getAt(0).setStyle('color: #96ff96;');

    },
    onPagePainted: function () {
        var TabBar = Ext.getCmp('idPendingInvitesTabInvitesMenteeBottomTabView').getTabBar();
       /* TabBar.getAt(0).setStyle('background-color: #96ff96;');
        TabBar.getAt(1).setStyle('background-color: #fffe86;');
        TabBar.getAt(2).setStyle('background-color: #ffa49d;');*/
        //TabBar.getAt(0).setStyle('color: #96ff96;');

        clearInterval(Mentor.Global.SET_INTERVAL_ID);
        Mentor.Global.SET_INTERVAL_ID = 0;
        Mentor.Global.SET_INTERVAL_ID = setInterval(function () {
            var TabBar = Ext.getCmp('idPendingInvitesTabInvitesMenteeBottomTabView').getTabBar();
            var TabBarPos = TabBar.items.indexOf(TabBar.getActiveTab());
            if (TabBarPos == 0) {
                Ext.getCmp('refreshscreen').fireAction('tap', '', Mentor.app.application.getController('AppDetail').getMentorWaitScreenAccepted(true));
            } else if (TabBarPos == 1) {
                Ext.getCmp('refreshscreen').fireAction('tap', '', Mentor.app.application.getController('AppDetail').getMentorWaitScreenPending(true));
            } else if (TabBarPos == 2) {
                Ext.getCmp('refreshscreen').fireAction('tap', '', Mentor.app.application.getController('AppDetail').getMentorWaitScreenRejected(true));
            } else if (TabBarPos == 3) {
                Ext.getCmp('refreshscreen').fireAction('tap', '', Mentor.app.application.getController('AppDetail').btnInviteForMeeting(true));
            }
        }, Mentor.Global.SET_INTERVAL_TIME);
    },

   
});