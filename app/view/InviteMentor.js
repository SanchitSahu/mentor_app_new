Ext.define('Mentor.view.InviteMentor', {
    extend: 'Ext.Panel',
    xtype: 'invitementor',
    requires: [],
    config: {
        cls: 'meetinghistory',
        //scrollable: "vertical",
        scrollable: {
            direction: 'vertical',
            indicators: {
                y: {
                    autoHide: false
                }
            }
        },
        defaults: {
            xtype: 'container'
        },
        items: [
            {
                docked: 'top',
                xtype: 'toolbar',
                title: 'Invite Screen',
                cls: 'my-titlebar',
                hidden: true,
                defaults: {
                    iconMask: true
                },
                items: [
                    {
                        iconCls: 'arrow_left',
                        action: 'btnBackInviteBack',
                        docked: 'left'
                    },
                    {
                        iconCls: 'arrow_right',
                        action: 'btnGotoWaitScreenInviteMentorScreen',
                        docked: 'right'
                    }
                ]
            },
            {
                xtype: 'label',
                id: 'idUserName',
                style: 'text-align: right;padding-right: 10px;padding: 10px;background-color: #C2C2C2;',
                docked: 'top',
                hidden: true
            },
            {
                xtype: 'label',
                id: 'idVersionInviteMentor',
                style: 'text-align: right;padding-right: 10px;padding: 10px;',
                docked: 'bottom',
                hidden: true
            },
            {
                xtype: 'label',
                html: 'All mentors assigned to you are unavailable at this time. Please try again later.',
                id: 'idNoMentorAvailable',
                style: 'text-align: center;font-size: 17px;padding-right: 10px;padding: 10px;',
                //docked: 'bottom',
                hidden: true
            },
           {
                xtype: 'fieldset',
                //title: 'Select Available Mentor',
                id: 'idLabelSelectMentorInvireMentor',
                margin : '20',
                items: [
                    {
                        xtype: 'selectfield',
                        id: 'idMentorSelectFieldInviteMentorScreen',
                        store: 'InviteMentor',
                        displayField: 'MentorName',
                        placeHolder: '-- Select Mentor --',
                        valueField: 'MentorID',

                        listeners: {
                            change: function (field, value) {
                                Mentor.app.getController('AppDetail').getMeetingHistory(true);
                                var MeetingHistory = Ext.getStore('MeetingHistory');

                                Ext.getCmp('idIsFollowUpMeetingInviteMentorNo').setChecked(true);
                                MeetingHistory.clearFilter();
                                if (value instanceof Ext.data.Model) {
                                    value = value.get(field.getValueField());
                                }
                                console.log(value);
                                var Store = Ext.getStore('InviteMentor');
                                var record = Store.findRecord('MentorID', value);
                                if (record != null) {
                                    if (record.data.rStatus == "TRUE") {
                                        Ext.getCmp('idSendBtnInviteMentorScreen').setDisabled(true);
                                        Ext.getCmp('idSendBtnInviteMentorScreen').setText("Invitation Still Pending");
                                        Ext.getCmp('idTopicsSelectFieldInviteMentorScreen').setDisabled(true);
                                        Ext.getCmp('menteeCommentInviteMentorScreen').setDisabled(true);
                                    } else {
                                        Ext.getCmp('idSendBtnInviteMentorScreen').setDisabled(false);
                                        //Ext.getCmp('idSendBtnInviteMentorScreen').setText("SEND INVITATION");
                                        Ext.getCmp('idSendBtnInviteMentorScreen').setText("SEND INVITE");
                                        Ext.getCmp('idTopicsSelectFieldInviteMentorScreen').setDisabled(false);
                                        Ext.getCmp('menteeCommentInviteMentorScreen').setDisabled(false);
                                    }
                                }
                            }
                        }
                    }
                ]
            },
           
            {
                xtype: 'fieldset',
                //title: 'Select Main Topic <span class="spanAsterisk">*</span>',
                margin: '0 20 0 20',
                items: [
                    {
                        // autoSelect: false,
                        // placeHolder: '-- Select Main Topic --',
                        xtype: 'selectfield',
                        id: 'idTopicsSelectFieldInviteMentorScreen',
                        store: 'Topics',
                        displayField: 'TopicDescription',
                        valueField: 'TopicID'
                    }
                ]
            },
            {
                xtype: 'label',
                html: Mentor.Global.MENTEE_NAME + " Email",
                style: 'font-weight: bold;',
                margin: '20 10 0 20',
                hidden: true
            },
            {
                xtype: 'textfield',
                id: 'idMeenteeEmailInviteMentorScreen',
                margin: '0 10 0 10',
                cls: 'textFieldDisableCLS',
                inputCls: 'inputCLS',
                clearIcon: false,
                hidden: true
            },
            {
                xtype: 'label',
                html: Mentor.Global.MENTEE_NAME + " Phone No",
                style: 'font-weight: bold;',
                margin: '20 10 0 20',
                hidden: true
            },
            {
                xtype: 'textfield',
                id: 'idMeenteePhoneNoInviteMentorScreen',
                margin: '0 10 0 10',
                cls: 'textFieldDisableCLS',
                inputCls: 'inputCLS',
                clearIcon: false,
                hidden: true
            },
            {
                xtype: 'panel',
                margin: '10 0 10 10',
                layout: {
                    type: 'hbox'
                },
                items: [
                    {
                        xtype: 'checkboxfield',
                        id: 'idIsPhoneSMSCapableInviteMentorScreen',
                        label: "Is above number Text Message(aka SMS) capable ?",
                        labelCls: 'radio-btn-cls',
                        labelWidth: '90%',
                        labelAlign: 'right',
                        hidden: true
                    }
                ]
            },
            {
                xtype: 'label',
                html: "Is this a follow up meeting?",
                style: 'font-weight: bold;',
                margin: '0 10 0 20',
                cls: 'InviteMentorLabelCLS',
                style : 'background:transparent',
            },
            {
                xtype: 'panel',
                margin: '0 20 0 20',
                layout: {
                    type: 'hbox'
                },
                style : 'background:transparent',
                items: [
                    {
                        xtype: 'radiofield',
                        name: 'color',
                        value: '0',
                        label: 'Yes',
                        //labelWidth: 100,
                        //flex: 1,
                        labelCls: 'radio-btn-cls',
                        labelAlign: 'right',
                        labelWidth: '80%',
                        style : 'background:transparent',
                        id: 'idIsFollowUpMeetingInviteMentorYes',
                        listeners: {
                            'check': function (radio, e, eOpts) {
                                Ext.getCmp('idLabelFollowUpListInviteMentor').setHidden(false)
                                Ext.getCmp('idFieldsetFollowUpListInviteMentor').setHidden(false);
                            }
                        }
                    },
                    {
                        xtype: 'radiofield',
                        id: 'idIsFollowUpMeetingInviteMentorNo',
                        name: 'color',
                        value: '1',
                        labelWidth: '80%',
                        checked: true,
                        //flex: 1,
                        label: 'No',
                        labelCls: 'radio-btn-cls',
                        labelAlign: 'right',
                        style : 'background:transparent',
                        listeners: {
                            'check': function (radio, e, eOpts) {
                                Ext.getCmp('idLabelFollowUpListInviteMentor').setHidden(true);
                                Ext.getCmp('idFieldsetFollowUpListInviteMentor').setHidden(true);
                            }
                        }
                    }
                ]
//                items: [
//                    {
//                        xtype: 'checkboxfield',
//                        id: 'idIsFollowUpMeetingInviteMentor',
//                        label: "Is this a follow up meeting?",
//                        labelCls: 'radio-btn-cls',
//                        labelWidth: '90%',
//                        labelAlign: 'right',
//                        cls: 'wordWrapSpan',
//                        listeners: {
//                            'check': function (radio, e, eOpts) {
//                                Ext.getCmp('idLabelFollowUpListInviteMentor').setHidden(false)
//                                Ext.getCmp('idFieldsetFollowUpListInviteMentor').setHidden(false);
//                            },
//                            'uncheck': function (radio, e, eOpts) {
//                                Ext.getCmp('idLabelFollowUpListInviteMentor').setHidden(true);
//                                Ext.getCmp('idFieldsetFollowUpListInviteMentor').setHidden(true);
//                            }
//                        }
//                    }
//                ]
            },
            {
                xtype: 'label',
                id: "idLabelFollowUpListInviteMentor",
                html: 'FollowUp Meeting for:',
                //style: 'font-weight: bold;',
                margin: '10 20 0 20',
                cls : 'appLabelCls',
                hidden: true
            },
            {
                xtype: 'fieldset',
                id: "idFieldsetFollowUpListInviteMentor",
                title: "",
                //style: 'margin-top:-20px;',
                margin: '-20 20 0 20',
                hidden: true,
                items: [
                    {
                        xtype: 'selectfield',
                        id: 'idFollowUpListInviteMentor',
                        displayField: 'MeetingDetail',
                        valueField: 'MeetingID'
                    }
                ]
            },
            {
                xtype: 'label',
                html: 'Reason for Meeting Request <span class="spanAsterisk">*</span>',
                margin: '20 0 0 15',
                style: 'font-weight: bold;',
                hidden : true,

            },
            {
                xtype: 'textareafield',
                name: 'mentorComment',
                id: 'menteeCommentInviteMentorScreen',
                cls: 'textFieldDisableCLS',
                maxRows: 2,
                disabled: false,
                margin: '20 20 0 20',
                inputCls: 'inputCLS',
                clearIcon: false,
                placeHolder : 'Reason for Meeting Request'
            },
            {
                xtype: 'container',
                layout: {
                    type: 'hbox'
                },
                margin: '0 10',
                items: [
                    {
                        xtype: 'button',
                        text: 'SEND INVITE',
                        cls: 'signin-btn-cls',
                        pressedCls : 'press-btn-cls',
                        action: "btnSendInviteMentorScreen",
                        id: 'idSendBtnInviteMentorScreen',
                        flex: 1,
                        style : 'font-size: 17px;'
                    },
                    {
                        xtype: 'button',
                        text: 'CANCEL',
                        pressedCls : 'press-grey_btn-cls',
                        cls: 'grey-btn-cls',
                        action: "btnCancelInviteMentorScreen",
                        id: 'idCancelBtnInviteMentorScreen',
                        flex: 1,
                        style : 'font-size: 17px;',
                        listeners: {
                            tap: function () {
                               /* var TabBar = Ext.getCmp('idMenteeInvitesTabView').getTabBar();
                                TabBar.setActiveTab(1);*/
                                Ext.getCmp('idPendingInvitesTabInvitesMenteeBottomTabView').getTabBar().setActiveTab(0);
                            }
                        }
                    }
                ]
            }
        ],
        listeners: [
            {
                event: "activate",
                fn: "onPageActivate"
            },
            {
                event: "painted",
                fn: "onPagePainted"
            },
            {
                event: "deactivate",
                fn: "onPageDeactivate"
            },
            {
                fn: 'onRadioChange',
                event: 'check',
                delegate: '#idIsFollowUpMeetingInviteMentorYes'
            }
        ]
    },
    onRadioChange: function () {
        var selectedMentor = Ext.getCmp('idMentorSelectFieldInviteMentorScreen').getValue();

        if (selectedMentor == -1) {
            Ext.Msg.alert('MELS', "Please select a " + Mentor.Global.MENTOR_NAME + " prior to selecting the Meeting");
            //Ext.getCmp('idIsFollowUpMeetingInviteMentor').setChecked(false);
            Ext.getCmp('idIsFollowUpMeetingInviteMentorNo').setChecked(true);
            return false;
        } else {
            var FollowUpDropdown = Ext.getCmp('idFollowUpListInviteMentor');

            var MeetingHistory = Ext.getStore('MeetingHistory');
            MeetingHistory.clearFilter();

            MeetingHistory.filterBy(function (rec) {
                return (rec.get('MentorID') == Ext.getCmp('idMentorSelectFieldInviteMentorScreen').getValue()) && (rec.get('isCancelled') == 0);
            });
            console.log(MeetingHistory);
            FollowUpDropdown.setStore(MeetingHistory);
        }
    },
    onPageActivate: function () {
        var TopicsStore = Ext.getStore('Topics').load();
        Ext.getStore('InviteMentor').load();
        Ext.getCmp('idVersionInviteMentor').setHtml(Mentor.Global.VERSION_NAME);
        Ext.getCmp('menteeCommentInviteMentorScreen').reset();

        //Change the lable of Mentor and Mentor where Name Exits according to Application Configuration
        //Ext.getCmp('idLabelSelectMentorInvireMentor').setTitle('Select Available ' + Mentor.Global.MENTOR_NAME + " <span class='spanAsterisk'>*</span>");
    },
    onPagePainted: function () {
        Ext.getCmp('idIsFollowUpMeetingInviteMentorNo').setChecked(false);
        Ext.getStore('InviteMentor').load();
        Ext.getCmp('idVersionInviteMentor').setHtml(Mentor.Global.VERSION_NAME);

        //Change the lable of Mentor and Mentor where Name Exits according to Application Configuration
        //Ext.getCmp('idLabelSelectMentorInvireMentor').setTitle('Select Available ' + Mentor.Global.MENTOR_NAME + " <span class='spanAsterisk'>*</span>");
        Mentor.app.application.getController('AppDetail').btnInviteForMeeting();

        var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));

        Ext.getCmp('idMeenteeEmailInviteMentorScreen').setValue(MenteeLoginUser.MenteeEmail);
        Ext.getCmp('idMeenteePhoneNoInviteMentorScreen').setValue(MenteeLoginUser.MenteePhone);
        Ext.getCmp('idIsPhoneSMSCapableInviteMentorScreen').setChecked(!parseInt(MenteeLoginUser.DisableSMS));
        Ext.getCmp('idTopicsSelectFieldInviteMentorScreen').reset();
    },
    onPageDeactivate: function () {
        var MeetingHistory = Ext.getStore('MeetingHistory');
        MeetingHistory.clearFilter();
    }
});