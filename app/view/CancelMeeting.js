Ext.define('Mentor.view.CancelMeeting', {
    extend: 'Ext.Panel',
    xtype: 'mentorCancelMeeting',
    id: 'idMentorCancelMeeting',
    requires: [],
    config: {
        cls: 'mentor_cancel_meeting',
        //scrollable: "vertical",
        scrollable: {
            direction: 'vertical',
            indicators: {
                y: {
                    autoHide: true
                }
            }
        },
        defaults: {
            xtype: 'container'
        },
        items: [
            {
                docked: 'top',
                xtype: 'toolbar',
                id: 'cancelMeetingTitle',
                title: 'Cancel Meeting',
                cls: 'my-titlebar',
                defaults: {
                    iconMask: true
                },
                items: [
                    {
                        iconCls: 'btnBackCLS',
                        action: 'btnBackMentorCancelMeeting',
                        docked: 'left'
                    },
//                    {
//                        iconCls: 'addIcon',
//                        docked: 'left',
//                        listeners: {
//                            element: 'element',
//                            tap: function () {
//                                var popup = Mentor.app.application.getController('AppDetail').addNotePopup();
//                                Ext.Viewport.add(popup);
//                                popup.show();
//                            }
//                        }
//                    },
                    {
                        iconCls: 'goToNotesIcon',
                        docked: 'left',
                        //style: 'visibility:hidden'
                        id: 'addnotes',
                        action: 'addnotes'
                    },
                    {
                        iconCls: 'helpIcon',
                        docked: 'right',
                        listeners: {
                            element: 'element',
                            tap: function () {
                                if (Ext.getCmp('cancelMeetingTitle').getTitle().getHtml().toLowerCase() == "cancel meeting") {
                                    var popup = Mentor.app.application.getController('AppDetail').addHelpPopup('cancel_meeting');
                                } else if (Ext.getCmp('cancelMeetingTitle').getTitle().getHtml().toLowerCase() == "cancel invite") {
                                    var popup = Mentor.app.application.getController('AppDetail').addHelpPopup('cancel_invite');
                                } else {
                                    var popup = Mentor.app.application.getController('AppDetail').addHelpPopup('no_help');
                                }
                                Ext.Viewport.add(popup);
                                popup.show();
                            }
                        }
                    },
                    {
                        iconCls: 'btnBackCLS',
                        action: 'btnBackMentorCancelMeeting',
                        docked: 'right',
                        style: 'visibility:hidden'
                    },
                ]
            },
            {
                xtype: 'panel',
                layout: {
                    type: 'vbox'
                },
                items: [
                    {
                        xtype: 'label',
                        id: 'idInfoMenteeNameMentorCancelMeetingDetail',
                        style: 'text-align: center;padding: 15px;border-bottom:1px solid #ccd4ee;color:#181717;font-size: 17px;',
                        html : 'Meeting Request from Mentee1',

                    },
                    {
                        xtype: 'label',
                        //html : 'Mentee Name',
                        style: 'font-weight: normal;',
                        margin: '20 20 0 20',
                        id: 'idLabelMenteeNameMentorCancelMeetingDetail',
                        cls : 'appLabelCls',
                        hidden : true,
                    },
                    {
                        xtype: 'textfield',
                        //name: 'waitScreenMeetingStatus',
                        id: 'idMenteeNameMentorCancelMeetingDetail',
                        disabled: true,
                        hidden : true,
                        disabledCls: 'textFieldDisableCLS',
                        inputCls: 'inputDisableCLS',
                        margin: '0 20 0 20'
                                /*disabled :false,
                                 listeners : {
                                 element : 'element',
                                 tap : function() {
                                 Mentor.app.application.getController('AppDetail').changeMenteeDetail();
                                 }
                                 }*/


                    },
                    {
                        xtype: 'label',
                        html: 'Main Topics',
                        style: 'font-weight: normal;',
                        margin: '20 20 0 20',
                        cls : 'appLabelCls'
                    },
                    {
                        xtype: 'textfield',
                        //name: 'waitScreenMeetingStatus',
                        id: 'idMainTopicsMentorCancelMeetingDetail',
                        disabled: true,
                        disabledCls: 'textFieldDisableCLS',
                        inputCls: 'inputDisableCLS',
                        margin: '0 20 0 20'
                    },
                    /*{
                        xtype: 'label',
                        html: 'Meeting Date Time',
                        id: 'idLabelMeetingDateTimeMentorCancelMeetingDetail',
                        style: 'font-weight: normal;',
                        margin: '20 20 0 20',
                        hidden: true,
                        cls : 'appLabelCls'
                    },*/
                    {
                        xtype: 'container',
                        layout: {
                            type: 'hbox',
                            pack: 'center'
                        },
                        margin: '20 20 0 20',
                        items : [
                            {
                                xtype: 'container',
                                layout: {type: 'vbox'},
                                flex : 1,
                                margin : '0 10 0 0',
                                items : [
                                    {
                                        xtype: 'label',
                                        html: 'Meeting Date',
                                        id: 'idLabelMeetingDateTimeMentorCancelMeetingDetail',
                                        style: 'font-weight: normal;',
                                        hidden: true,
                                        cls : 'appLabelCls'
                                    },
                                    {
                                        xtype: 'textfield',
                                        //name: 'waitScreenMeetingStatus',
                                        id: 'idMeetingDateTimeMentorCancelMeetingDetail',
                                        disabled: true,
                                        disabledCls: 'textFieldDisableCLS',
                                        inputCls: 'inputDisableCLS',
                                        hidden: true
                                    },
                                ]
                            },
                            {
                                xtype: 'container',
                                layout: {type: 'vbox'},
                                flex : 1,
                                margin : '0 0 0 10',
                                items : [
                                    {
                                        xtype: 'label',
                                        html: 'Meeting Time',
                                        id: 'idLabelMeetingTimeMentorCancelMeetingDetail',
                                        style: 'font-weight: normal;',
                                        hidden: true,
                                        cls : 'appLabelCls'
                                    },
                                    {
                                        xtype: 'textfield',
                                        //name: 'waitScreenMeetingStatus',
                                        id: 'idMeetingTimeMentorCancelMeetingDetail',
                                        disabled: true,
                                        disabledCls: 'textFieldDisableCLS',
                                        inputCls: 'inputDisableCLS',
                                        hidden: true
                                    },
                                ]
                            }
                        ]
                    },
                    {
                        xtype: 'label',
                        //html : Mentor.Global.MENTEE_NAME+' Comment',
                        margin: '20 20 0 20',
                        style: 'font-weight: normal;',
                        id: 'idLabelMenteeCommentCancelMeetingDetail',
                        cls : 'appLabelCls'
                    },
                    {
                        xtype: 'textareafield',
//                        name: 'mentorComment',
                        id: 'menteeCancellationCommentCancelMeetingScreen',
                        cls: 'textFieldDisableCLS',
                        maxRows: 2,
                        disabled: false,
                        margin: '0 20 10 20',
                        inputCls: 'inputCLS',
                        clearIcon: false,
                        placeHolder : 'Comments'
                    },
//                    {
//                        xtype: 'button',
//                        text: 'Cancel Meeting',
//                        cls: 'signin-btn-cls',
//                        action: "btnCancelPendingMeeting",
//                        id: 'idbtnCancelPendingMeeting',
//                        //flex : 1,
//                        margin: '0 10 10 10',
//                    },
                    {
                        xtype: 'button',
                        text: 'CONFIRM CANCELLATION',
                        cls: 'signin-btn-cls',
                        pressedCls : 'press-btn-cls',
                        action: "btnCancelPendingMeeting",
                        id: 'idbtnCancelPendingMeeting',
//                        flex: 1,
                        margin: '40 20 0 20'
                    },
                    {
                        xtype: 'button',
                        text: 'DO NOT CANCEL MEETING',
                        pressedCls : 'press-grey_btn-cls',
                        cls: 'grey-btn-cls',
                        action: "btnBackMentorCancelMeeting",
                        id: 'idbtnBackCancelPendingMeeting',
//                        flex: 1,
                        margin: '10 20 10 20'
                    }
//                    {
//                        xtype: 'container',
//                        layout: {
//                            type: 'hbox',
//                            pack: 'center',
//                        },
//                        margin: '0 10 10 10',
//                        items: [
//                            {
//                                xtype: 'button',
//                                text: 'Submit',
//                                cls: 'signin-btn-cls',
//                                action: "btnCancelPendingMeeting",
//                                id: 'idbtnCancelPendingMeeting',
//                                flex : 1,
//                                margin: '0 5 0 0',
//                            },
//                            {
//                                xtype: 'button',
//                                text: 'Cancel',
//                                cls: 'decline-btn-cls',
//                                action: "btnBackMentorCancelMeeting",
//                                id: 'idbtnBackCancelPendingMeeting',
//                                flex : 1,
//                                margin: '0 5 0 0',
//                            },
//                        ]
//                    }
                ]
            }
        ],
        listeners: []
    },
    onPageActivate: function () {
        //Check The Login User and display the name on the top header
        var MentorLoginDetail = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
        var MenteeLoginDetail = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));

        if (MentorLoginDetail != null) {
            Ext.getCmp('idUserNameInviteStatusList').setHtml(MentorLoginDetail.MentorName);
        } else if (MenteeLoginDetail != null) {
            Ext.getCmp('idUserNameInviteStatusList').setHtml(MenteeLoginDetail.MenteeName);
        }
    },
    onPagePainted: function () {}
});